#!/usr/bin/env python3

'''
Copyright 2023 INRAE, Université de Tours
requests at: sebastien.leclercq@inrae.fr

This file is part of GenoFig.

GenoFig is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Genofig is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

A copy of the GNU General Public License is available in the COPYING file. If not, see <https://www.gnu.org/licenses/>

This script may use some code adapted from Easyfig v2.1.1 written by Mitchell Sullivan and licensed under GNU GPLv3 (http://mjsull.github.io/Easyfig/files.html - Legacy versions)

The GenoFig logo is trademark of INRAE, UNiversité de Tours
'''


VERSION = "1.1.1"

# Standard Python modules
import threading
import time
import os
import sys
import ast
import json
import subprocess


#print(sys.path)

# Interface GTk3+
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk, Gdk, GObject, GdkPixbuf
import cairo

# Local modules
import Genomodules.ClassesGenofig as CGeno
import Genomodules.Drawing as DGeno
from Bio import SeqRecord

# Blast Execution module
from Bio.Blast.Applications import NcbiblastnCommandline, NcbitblastxCommandline


# File Extension types
EXT_CONFIG = "genocfg"
FTYPE_CONFIG = "Config file (*."+EXT_CONFIG+")"
EXT_ALL = "genofig"
FTYPE_ALL = "GenoFig file (*."+EXT_ALL+")"


# give the version and exit, necessary to enter Bioconda repository
###################################################################
if len(sys.argv) > 1:
    print("GenoFig version", VERSION)
    exit(0)


# Main frame
################################################################
class AppWindow(Gtk.ApplicationWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
  
        self.set_default_size(1500, 500)
        self.startingdir=os.path.dirname(os.path.abspath(__file__))
        self.blastpath=self.startingdir
        if self.startingdir != "":
            if sys.platform.startswith('win'):
                self.set_default_icon_from_file(f"{self.startingdir}\extras\Genofig.ico")
            elif sys.platform.startswith('darwin'):
                self.set_default_icon_from_file(f"{self.startingdir}/extras/GenoFig_128.png")
            else:
                self.set_default_icon_from_file(f"{self.startingdir}/extras/Genofig.svg")
        else:
            self.set_default_icon_from_file(f"extras/Genofig.svg")

        # Frame including: 1- general pictures settings
        #                  2- buttons to choose the sequence/feature/blast/legend/decoration sub-windows
        #                  3- the sub-windows
        self.boxMain = Gtk.Box(orientation=1, spacing=15)
        self.add(self.boxMain)

# set up initial values
#-------------------------------------------------------------------------
        self.outputfile = ""
        self.theminblast = None
        self.init_pictwidth = "3000"
        self.init_pictmarginTB = "500"
        self.init_pictmarginLR = "200"
        self.back_color = Gdk.RGBA()
        self.back_color.parse('#ffeedd')

        self.blastncol_color = Gdk.RGBA()
        self.blastncol_color.parse('#ababab')

        self.blastxcol_color = Gdk.RGBA()
        self.blastxcol_color.parse('#eeabee')

        self.black_color = Gdk.RGBA()
        self.black_color.parse('black')

        # Window Name, default save files and saving status
        self.set_title('GenoFig '+VERSION+ ' New - unsaved')
        self.current_working_filename = "new.genofig"
        self.current_working_configname = "new.genocfg"
        self.isCurrentSavePath = 0
        self.saveDirectoryPath = ''
        self.figDirectoryPath = ''
        self.configDirectoryPath = ''
        self.seqDirectoryPath = ''

# Setting up CSS styles
#-------------------------------------------------------------------------
        css_provider = Gtk.CssProvider()
        if self.startingdir != "":        
          if sys.platform.startswith('win'):
            css_provider.load_from_file(Gio.File.new_for_path(f"{self.startingdir}\extras\style.css"))
          else:
            css_provider.load_from_file(Gio.File.new_for_path(f"{self.startingdir}/extras/style.css"))
        else:
          css_provider.load_from_file(Gio.File.new_for_path("extras/style.css"))
        context = Gtk.StyleContext()
        screen = Gdk.Screen.get_default()
        context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

                
# General picture parameters
#-------------------------------------------------------------------------

        # Grid container to place  picture parameters
        self.boxPictParams = Gtk.Box()
        self.boxPictParams.set_border_width(10)

        # Grid container to place size and save 
        self.gridPictParams = Gtk.Grid()
        self.gridPictParams.set_property("row-spacing", 1)

        self.labelPWidth = Gtk.Label(label="Image width: ")
        self.entryPWidth = CGeno.EntrySmall(5, self.init_pictwidth, 100, 90000)
        self.entryPWidth.set_width_chars(1)

        self.labelPWidthpx = Gtk.Label(label=" px")
        self.labelPWidthpx.set_xalign(0)

        self.labelPName = Gtk.Label(label="Draw figure in: ")
        self.labelPName.set_xalign(1)
        self.entryPName = Gtk.Entry()
        self.entryPName.set_editable(True)
        self.entryPName.set_has_frame(False)
        self.entryPName.set_width_chars(40)
        self.entryPName.set_icon_from_icon_name(Gtk.EntryIconPosition.PRIMARY, "document-open")
        self.entryPName.connect("icon-press", self.on_picture_name)


        self.gridPictParams.attach(self.labelPWidth, 0, 0, 1, 1)
        self.gridPictParams.attach(self.entryPWidth, 1, 0, 1, 1)
        self.gridPictParams.attach(self.labelPWidthpx, 2, 0, 1, 1)
        self.gridPictParams.attach(self.labelPName, 0, 1, 1, 1)
        self.gridPictParams.attach(self.entryPName, 1, 1, 6, 1)
        
        

        # Grid container to place margins 
        self.gridPictMargins = Gtk.Grid()
        self.gridPictMargins.set_property("row-spacing", 5)
        self.gridPictMargins.set_property("column-spacing", 5)

        self.labelPMargin=Gtk.Label(label="margins")
        self.entryPMtop = CGeno.EntrySmall(5, self.init_pictmarginTB, 0, 10000)
        self.entryPMbottom = CGeno.EntrySmall(5, self.init_pictmarginTB, 0, 10000)
        self.entryPMleft = CGeno.EntrySmall(5, self.init_pictmarginLR, 0, 10000)
        self.entryPMright = CGeno.EntrySmall(5, self.init_pictmarginLR, 0, 10000)

        self.gridPictMargins.attach(self.entryPMleft, 0, 1, 1, 1)
        self.gridPictMargins.attach(self.labelPMargin, 1, 1, 1, 1)
        self.gridPictMargins.attach(self.entryPMright, 2, 1, 1, 1)
        self.gridPictMargins.attach(self.entryPMtop, 1, 0, 1, 1)
        self.gridPictMargins.attach(self.entryPMbottom, 1, 2, 1, 1)


        # Grid container to place margins 
        self.gridPictColor = Gtk.Grid()
        self.gridPictColor.set_property("row-spacing", 15)
        self.gridPictColor.set_property("column-spacing", 5)

        self.labelPColor=Gtk.Label(label="background color", xalign=1)

        self.buttonPColor=Gtk.ColorButton.new_with_rgba(self.back_color)
        self.buttonPColor.set_relief(Gtk.ReliefStyle.NONE)

        self.checkPColor=Gtk.CheckButton()
        self.checkPColor.set_active(True)
        self.checkPColor.set_property("margin-bottom", 4)
        self.checkPColor.connect("toggled", self.on_toggledColor, self.buttonPColor, self.buttonPColor)


        self.labelPEnhance=Gtk.Label(label="enhance graph", xalign=1)
        self.switchPEnhance=Gtk.Switch()
        self.switchPEnhance.set_active(False)

        self.gridPictColor.attach(self.labelPColor, 0, 1, 1, 1)
        self.gridPictColor.attach(self.checkPColor, 1, 1, 1, 1)
        self.gridPictColor.attach(self.buttonPColor, 2, 1, 1, 1)
        self.gridPictColor.attach(self.labelPEnhance, 0, 2, 1, 1)
        self.gridPictColor.attach(self.switchPEnhance, 1, 2, 2, 1)


        self.butRUN = Gtk.Button(label="  CREATE FIGURE  ", name="generate-button")
        self.butRUN.set_property("margin-bottom", 40)
        self.butRUN.connect("clicked", self.createcommand)

        self.boxPictParams.pack_start(self.gridPictParams, False, False, 0)
        self.boxPictParams.pack_start(self.gridPictMargins, False, False, 30)
        self.boxPictParams.pack_start(self.gridPictColor, False, False, 15)
        self.boxPictParams.pack_start(self.butRUN, False, False, 15)


# Combo lists definition
#-------------------------------------------------------------------------

        self.listSeqPos= Gtk.ListStore(str)
        for item in ["-", "left", "right", "center", "best blast", "custom"]:
            self.listSeqPos.append([item])

        self.listSeqLabType= Gtk.ListStore(str)
        for item in ["-", 'locus', 'accession', 'organism','strain','organism+strain', 'description', 'locus+organism', 'locus+organism+strain', 'locus+size', 'locus+description', 'accession+organism', 'accession+organism+strain','accession+description']:
            self.listSeqLabType.append([item])

        self.listSeqLabPos= Gtk.ListStore(str)
        for item in ["-", 'left', 'right', 'top', 'bottom', 'top/left', 'top/right', 'bottom/left', 'bottom/right']:
            self.listSeqLabPos.append([item])

        self.listSeqFeatLabType= Gtk.ListStore(str)
        for item in ["-", 'gene', 'product', 'note', 'locus_tag', 'mobile_element_type','operon']:
            self.listSeqFeatLabType.append([item])

        self.listSeqFeatLabPos= Gtk.ListStore(str)
        for item in ["-", 'Top', 'Middle', 'Bottom']:
            self.listSeqFeatLabPos.append([item])

        self.listFeatLabPos= Gtk.ListStore(str)
        for item in ["-", 'Opposite', 'Top', 'Middle', 'Bottom']:
            self.listFeatLabPos.append([item])


        # Defining featTypeList is necessary to update the list when sequences with new features are loaded
        self.featTypeList = ["CDS", "gene", "mobile_element","tRNA","rRNA","tmRNA","misc_feature","regulatory","repeat_region","operon"]
        self.listFeatType = Gtk.ListStore(str)
        for item in self.featTypeList:
            self.listFeatType.append([item])
        
        self.listFeatStrand = Gtk.ListStore(str)
        for item in ["-", 'none', 'lead up', 'lag up']:
            self.listFeatStrand.append([item])

        self.listFeatShape = Gtk.ListStore(str)
        for item in ["-", 'arrow', 'rectangle', 'frame', 'signal', 'range', 'rangeL']:
            self.listFeatShape.append([item])

        self.listFeatHatch = Gtk.ListStore(str)
        for item in ["-", 'none', 'hbar', 'hbar2', 'circle', 'crossed', 'crossed2', 'rain', 'diagonal', 'Rdiagonal', 'waved', 'dotted', 'crosses']:
            self.listFeatHatch.append([item])

        self.listFeatFilterF = Gtk.ListStore(str)
        for item in ['any', 'gene', 'product', 'note', 'mobile_element_type', 'operon','regulatory_class','rpt_type']:
            self.listFeatFilterF.append([item])

        # Used to select the sequence with the 'All vs seq' Blast selection option 
        # No sequence when created, then modified each time the listSeq object is modified
        self.listSeqChoice = Gtk.ListStore(str)
        for item in ['No Sequence', 'Other']:
            self.listSeqChoice.append([item])

        ## Decoration lists as types and positions
        self.decoTypeList = ["GC%", "GC%_fill", "GCskew", "GCskew_fill", "Scale"]
        self.listDecoType = Gtk.ListStore(str)
        for item in self.decoTypeList:
            self.listDecoType.append([item])
        
        self.DecoPositions = ["-","above", "on sequence", "below"]
        self.listDecoPosition = Gtk.ListStore(str)
        for item in self.DecoPositions:
            self.listDecoPosition.append([item])
        
        self.DecoPositionsLabel = ["-","left", "right", "middle"]
        self.listDecoPositionLabel = Gtk.ListStore(str)
        for item in self.DecoPositionsLabel:
            self.listDecoPositionLabel.append([item])
    ## Define dictionnary for decoration sequence selection
        self.DecorationSequences = dict()

# Sequence parameters page
#-------------------------------------------------------------------------
        self.listSeq = CGeno.ListStoreDnd(GObject.TYPE_BOOLEAN,    #  0 = tag to say that the row is not the general (used for many things)
                                          GObject.TYPE_STRING,     #  1 = add/remove icon name
                                          GObject.TYPE_PYOBJECT,   #  2 = add/remove action
                                          GObject.TYPE_STRING,     #  3 = sequence name
                                          GObject.TYPE_PYOBJECT,   #  4 = sequence information action
                                          GObject.TYPE_OBJECT,     #  5 = the Bioseq record of the sequence
                                          GObject.TYPE_BOOLEAN,    #  6 = select/unselect the sequence

                                          GObject.TYPE_STRING,     #  7 = sequence height
                                          GObject.TYPE_STRING,     #  8 = line width
                                          GObject.TYPE_STRING,     #  9 = space after the sequence
                                          GObject.TYPE_STRING,     # 10 = color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 11 = x position when 'custom' is selected in 13
                                          GObject.TYPE_STRING,     # 12 = line color

                                          GObject.TYPE_STRING,     # 13 = position of the sequence in the image
                                          GObject.TYPE_STRING,     # 14 = min position of the sequence
                                          GObject.TYPE_STRING,     # 15 = max position of the sequence
                                          GObject.TYPE_BOOLEAN,    # 16 = reverse complement the sequence

                                          GObject.TYPE_BOOLEAN,    # 17 = print sequence label
                                          GObject.TYPE_STRING,     # 18 = sequence label type
                                          GObject.TYPE_STRING,     # 19 = sequence label position according to sequence
                                          GObject.TYPE_STRING,     # 20 = sequence label color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 21 = sequence label color
                                          GObject.TYPE_STRING,     # 22 = sequence label font size

                                          GObject.TYPE_BOOLEAN,    # 23 = print features label
                                          GObject.TYPE_STRING,     # 24 = features label type
                                          GObject.TYPE_STRING,     # 25 = features label position according to sequence
                                          GObject.TYPE_STRING,     # 26 = features label color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 27 = features label color
                                          GObject.TYPE_STRING,     # 28 = features label font size
                                          GObject.TYPE_STRING,     # 29 = features label rotation
                                          GObject.TYPE_STRING,     # 30 = sequence label offset to the sequence cartoon
                                          
                                          GObject.TYPE_INT,        # 31 = sequence uniq ID
                                          GObject.TYPE_BOOLEAN,    # 32 = sequence label bold
                                          GObject.TYPE_BOOLEAN     # 33 = sequence label italic
                                         
                                          )

        # construct the list of columns numbers necessary for batch insertion of column information when sequences are created
        self.colsSeqID = [0]
        self.seqID = 0
        while len(self.colsSeqID) <= 31:
          self.colsSeqID.append(self.colsSeqID[-1]+1)
        #---------------------------------------------
        self.tableSeq = Gtk.TreeView(model=self.listSeq)
        self.tableSeq.set_reorderable(True)
        self.tableSeq.set_property("hover-selection", True)

        renderer_Sbut = CGeno.CellRendererButton()
        self.colSAddRem = Gtk.TreeViewColumn('      ', renderer_Sbut, icon_name=1, callable=2)
        self.tableSeq.append_column(self.colSAddRem)
        
        renderer_text = Gtk.CellRendererText()
        self.colSName = Gtk.TreeViewColumn("sequence name", renderer_text, text=3)
        self.colSName.set_resizable(True)
        self.colSName.set_alignment(0.5)
        self.tableSeq.append_column(self.colSName)
        
        renderer_butInfo = CGeno.CellRendererButton()
        renderer_butInfo.set_property("icon_name", "dialog-information-symbolic")
        self.colSInfo = Gtk.TreeViewColumn("infos", renderer_butInfo, callable=4, visible=0)
        self.colSInfo.set_resizable(True)
        self.tableSeq.append_column(self.colSInfo)

        renderer_togS = Gtk.CellRendererToggle()
        renderer_togS.connect("toggled", self.on_toggleSeq_changed, 6)
        self.colSsel = Gtk.TreeViewColumn("active", renderer_togS, active=6, visible=0)
        self.colSsel.set_resizable(True)
        self.tableSeq.append_column(self.colSsel)

        renderer_space = Gtk.CellRendererText()
        self.colSpace1 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableSeq.append_column(self.colSpace1)

        renderer_combopos = CGeno.CellRendererComboNoEntry(self.listSeqPos)
        renderer_combopos.connect("edited", self.on_comboSeqPos_changed)
        self.colSPosition = Gtk.TreeViewColumn("position", renderer_combopos, text=13)
        self.colSPosition.set_property("min-width", self.max_colsize(self.listSeqPos))
        self.colSPosition.set_resizable(True)
        self.tableSeq.append_column(self.colSPosition)


        renderer_spinMi = Gtk.CellRendererText()
        renderer_spinMi.set_property("editable", True)
        renderer_spinMi.connect("edited", self.change_Smin)
        self.colSMin = Gtk.TreeViewColumn("min", renderer_spinMi, text=14, visible=0)
        self.colSMin.set_resizable(True)
        self.tableSeq.append_column(self.colSMin)

        renderer_spinMa = Gtk.CellRendererText()
        renderer_spinMa.set_property("editable", True)
        renderer_spinMa.connect("edited", self.change_Smax)
        self.colSMax = Gtk.TreeViewColumn("max", renderer_spinMa, text=15, visible=0)
        self.colSMax.set_resizable(True)
        self.tableSeq.append_column(self.colSMax)

        renderer_spinR = Gtk.CellRendererToggle()
        renderer_spinR.connect("toggled", self.on_toggleSeq_changed, 16)
        self.colSRev = Gtk.TreeViewColumn("reverse", renderer_spinR, active=16, visible=0)
        self.colSRev.set_resizable(True)
        self.tableSeq.append_column(self.colSRev)

        renderer_spinH = Gtk.CellRendererText()
        renderer_spinH.set_property("editable", True)
        renderer_spinH.connect("edited", self.on_int_changed, self.listSeq, 7, 0, 1000)
        self.colSHeight = Gtk.TreeViewColumn("height", renderer_spinH, text=7)
        self.colSHeight.set_resizable(True)
        self.tableSeq.append_column(self.colSHeight)

        renderer_spinS = Gtk.CellRendererText()
        renderer_spinS.set_property("editable", True)
        renderer_spinS.connect("edited", self.on_int_changed, self.listSeq, 9, -10000, 10000)
        self.colSSpace = Gtk.TreeViewColumn("space\nbelow", renderer_spinS, text=9)
        self.colSSpace.set_resizable(True)
        self.tableSeq.append_column(self.colSSpace)

        self.colSpace2 = Gtk.TreeViewColumn(" ", renderer_space)
        self.tableSeq.append_column(self.colSpace2)

        self.colSpaceSline = Gtk.TreeViewColumn("line", renderer_space)
        self.tableSeq.append_column(self.colSpaceSline)

        renderer_spinW = Gtk.CellRendererText()
        renderer_spinW.set_property("editable", True)
        renderer_spinW.connect("edited", self.on_int_changed, self.listSeq, 8, 0, 1000)
        self.colSLineW = Gtk.TreeViewColumn("\nwidth", renderer_spinW, text=8)
        self.colSLineW.set_resizable(True)
        self.tableSeq.append_column(self.colSLineW)

        renderer_butCol = Gtk.CellRendererText()
        renderer_butCol.set_property("editable", True)
        renderer_butCol.connect("editing-started", self.on_change_colCell, self.listSeq, 10, 12, "Sequence")
        self.colSColor = Gtk.TreeViewColumn('\ncolor', renderer_butCol, text=10)
        self.colSColor.set_property("fixed-width", 45)
        self.colSColor.add_attribute(renderer_butCol, "cell-background", 12)
        self.colSColor.set_resizable(True)
        self.tableSeq.append_column(self.colSColor)

        self.colSpace3 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableSeq.append_column(self.colSpace3)


        renderer_spinSLPr = Gtk.CellRendererToggle()
        renderer_spinSLPr.connect("toggled", self.on_toggleSeq_changed, 17)
        self.colSLPrint = Gtk.TreeViewColumn("seq.\nlabel", renderer_spinSLPr, active=17)
        self.colSLPrint.set_resizable(True)
        self.tableSeq.append_column(self.colSLPrint)


        renderer_comboSLType = CGeno.CellRendererComboNoEntry(self.listSeqLabType)
        renderer_comboSLType.connect("edited", self.on_comboSeq_changed, 18)
        self.colSLType = Gtk.TreeViewColumn("\ntype", renderer_comboSLType, text=18)
#        self.colSLType.set_property("min-width", self.max_colsize(self.listSeqLabType))
        self.colSLType.set_property("fixed-width", 90)
        self.colSLType.set_resizable(True)
        self.tableSeq.append_column(self.colSLType)

        renderer_comboSLPos = CGeno.CellRendererComboNoEntry(self.listSeqLabPos)
        renderer_comboSLPos.connect("edited", self.on_comboSeq_changed, 19)
        self.colSLPos = Gtk.TreeViewColumn("\nposition", renderer_comboSLPos, text=19)
        self.colSLPos.set_property("min-width", self.max_colsize(self.listSeqLabPos))
        self.colSLPos.set_resizable(True)
        self.tableSeq.append_column(self.colSLPos)

        renderer_spinSLOff = Gtk.CellRendererText()
        renderer_spinSLOff.set_property("editable", True)
        renderer_spinSLOff.connect("edited", self.on_int_changed, self.listSeq, 30, -300, 300)
        self.colSLabOff = Gtk.TreeViewColumn("\noffset", renderer_spinSLOff, text=30)
        self.colSLabOff.set_resizable(True)
        self.tableSeq.append_column(self.colSLabOff)

        renderer_butCol = Gtk.CellRendererText()
        renderer_butCol.set_property("editable", True)
        renderer_butCol.connect("editing-started", self.on_change_colCell, self.listSeq, 20, 21, "Sequence label")
        self.colSColor = Gtk.TreeViewColumn('\ncolor', renderer_butCol, text=20)
        self.colSColor.set_property("fixed-width", 45)
        self.colSColor.add_attribute(renderer_butCol, "cell-background", 21)
        self.colSColor.set_resizable(True)
        self.tableSeq.append_column(self.colSColor)
 
        renderer_spinSLS = Gtk.CellRendererText()
        renderer_spinSLS.set_property("editable", True)
        renderer_spinSLS.connect("edited", self.on_int_changed, self.listSeq, 22, 1, 300)
        self.colSLabSize = Gtk.TreeViewColumn("\nsize", renderer_spinSLS, text=22)
        self.colSLabSize.set_resizable(True)
        self.tableSeq.append_column(self.colSLabSize)
 
        renderer_spinSLBr = Gtk.CellRendererToggle()
        renderer_spinSLBr.connect("toggled", self.on_toggleSeq_changed, 32)
        self.colSLBold = Gtk.TreeViewColumn("\nB", renderer_spinSLBr, active=32)
        self.colSLBold.set_resizable(False)
        self.tableSeq.append_column(self.colSLBold)

        renderer_spinSLIr = Gtk.CellRendererToggle()
        renderer_spinSLIr.connect("toggled", self.on_toggleSeq_changed, 33)
        self.colSLItalic = Gtk.TreeViewColumn("\nI", renderer_spinSLIr, active=33)
        self.colSLItalic.set_resizable(False)
        self.tableSeq.append_column(self.colSLItalic)
 
        self.colSpace3 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableSeq.append_column(self.colSpace3)



 
        renderer_spinSFLPr = Gtk.CellRendererToggle()
        renderer_spinSFLPr.connect("toggled", self.on_toggleSeq_changed, 23)
        self.colSFLPrint = Gtk.TreeViewColumn("feat.\nlabel", renderer_spinSFLPr, active=23)
        self.colSFLPrint.set_resizable(True)
        self.tableSeq.append_column(self.colSFLPrint)


        renderer_comboSFLType = CGeno.CellRendererComboNoEntry(self.listSeqFeatLabType)
        renderer_comboSFLType.connect("edited", self.on_comboSeq_changed, 24)
        self.colSFLType = Gtk.TreeViewColumn("\ntype", renderer_comboSFLType, text=24)
        self.colSFLType.set_resizable(True)
        self.tableSeq.append_column(self.colSFLType)

        renderer_comboSFLPos = CGeno.CellRendererComboNoEntry(self.listSeqFeatLabPos)
        renderer_comboSFLPos.connect("edited", self.on_comboSeq_changed, 25)
        self.colSFLPos = Gtk.TreeViewColumn("\nposition", renderer_comboSFLPos, text=25)
        self.colSFLPos.set_property("min-width", self.max_colsize(self.listSeqFeatLabPos))
        self.colSFLPos.set_resizable(True)
        self.tableSeq.append_column(self.colSFLPos)

        renderer_butSFCol = Gtk.CellRendererText()
        renderer_butSFCol.set_property("editable", True)
        renderer_butSFCol.connect("editing-started", self.on_change_colCell, self.listSeq, 26, 27, "Feature label")
        self.colSFColor = Gtk.TreeViewColumn('\ncolor', renderer_butSFCol, text=26)
        self.colSFColor.set_property("fixed-width", 45)
        self.colSFColor.add_attribute(renderer_butSFCol, "cell-background", 27)
        self.colSFColor.set_resizable(True)
        self.tableSeq.append_column(self.colSFColor)
 
        renderer_spinSFLS = Gtk.CellRendererText()
        renderer_spinSFLS.set_property("editable", True)
        renderer_spinSFLS.connect("edited", self.on_int_changed, self.listSeq, 28, 1, 300)
        self.colSFLabSize = Gtk.TreeViewColumn("\nsize", renderer_spinSFLS, text=28)
        self.colSFLabSize.set_resizable(True)
        self.tableSeq.append_column(self.colSFLabSize)

        renderer_spinSFLR = Gtk.CellRendererText()
        renderer_spinSFLR.set_property("editable", True)
        renderer_spinSFLR.connect("edited", self.on_int_changed, self.listSeq, 29, -360, 360)
        self.colSFLabRot = Gtk.TreeViewColumn("\nrot.", renderer_spinSFLR, text=29)
        self.colSFLabRot.set_resizable(True)
        self.tableSeq.append_column(self.colSFLabRot)

        #Populate the Sequence list (Treeview) with general and 'add' button
        self.addSeq(False, "General", "", "")

        self.listSeq.connect("row-deleted", self.on_Seqnbchanged)
        self.listSeq.connect("rows-reordered", self.on_Seqnbchanged)


# Feature parameters page
#-------------------------------------------------------------------------

        self.listFeat = CGeno.ListStoreDnd(GObject.TYPE_BOOLEAN,   #  0 = tag to say that the row is not the general (used for many things)
                                          GObject.TYPE_STRING,     #  1 = add/remove icon name
                                          GObject.TYPE_PYOBJECT,   #  2 = add/remove action
                                          GObject.TYPE_STRING,     #  3 = feature id
                                          GObject.TYPE_BOOLEAN,    #  4 = select/unselect the sequence

                                          GObject.TYPE_STRING,     #  5 = feature type
                                          GObject.TYPE_STRING,     #  6 = feature strand position
                                          GObject.TYPE_STRING,     #  7 = feature shape
                                          GObject.TYPE_STRING,     #  8 = feature hatching

                                          GObject.TYPE_STRING,     #  9 = feature height ratio
                                          GObject.TYPE_STRING,     # 10 = feature color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 11 = feature fill color
                                          GObject.TYPE_BOOLEAN,    # 12 = fill the feature

                                          GObject.TYPE_STRING,     # 13 = feature line witdh
                                          GObject.TYPE_STRING,     # 14 = feature line color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 15 = feature line color

                                          GObject.TYPE_STRING,     # 16 = filter feature
                                          GObject.TYPE_STRING,     # 17 = field onto witch to filter

                                          GObject.TYPE_PYOBJECT,    # 18 = copy action

                                          GObject.TYPE_BOOLEAN,    # 19 = print features label
                                          GObject.TYPE_STRING,     # 20 = features label type
                                          GObject.TYPE_STRING,     # 21 = features label position according to sequence
                                          GObject.TYPE_STRING,     # 22 = features label color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 23 = features label color
                                          GObject.TYPE_STRING,     # 24 = features label font size
                                          GObject.TYPE_STRING,     # 25 = features label rotation
                                          GObject.TYPE_BOOLEAN,    # 26 = show feature in legend
                                          
                                          GObject.TYPE_BOOLEAN,    # 27 = features label bold
                                          GObject.TYPE_BOOLEAN     # 28 = features label italic

                                           )     

        self.colsFeatID = [0]
        while len(self.colsFeatID) <= 26:
          self.colsFeatID.append(self.colsFeatID[-1]+1)


        self.tableFeat = Gtk.TreeView(model=self.listFeat)
        self.tableFeat.set_reorderable(True)
        self.tableFeat.set_property("hover-selection", True)


        renderer_butFAR = CGeno.CellRendererButton()
        self.colFAddRem = Gtk.TreeViewColumn('      ', renderer_butFAR, icon_name=1, callable=2)
        self.colFAddRem.set_resizable(True)
        self.tableFeat.append_column(self.colFAddRem)

        renderer_butFC = CGeno.CellRendererButton()
        renderer_butFC.set_property("icon_name", "edit-copy")
        self.colFCopy = Gtk.TreeViewColumn("    ", renderer_butFC, callable=18, visible=0)
        self.colFCopy.set_resizable(True)
        self.tableFeat.append_column(self.colFCopy)


        renderer_Ftext = Gtk.CellRendererText()
        renderer_Ftext.connect("edited", self.on_comboFeat_changed, 3)
        self.colFId = Gtk.TreeViewColumn("", renderer_Ftext, text=3, editable=0)
        self.colFId.set_resizable(True)
        self.tableFeat.append_column(self.colFId)
        
        renderer_togFS = Gtk.CellRendererToggle()
        renderer_togFS.connect("toggled", self.on_toggleFeat_changed, 4)
        self.colFsel = Gtk.TreeViewColumn("selected", renderer_togFS, active=4, visible=0)
        self.colFsel.set_resizable(True)
        self.tableFeat.append_column(self.colFsel)
        
        renderer_togFLS = Gtk.CellRendererToggle()
        renderer_togFLS.connect("toggled", self.on_toggleFeat_changed, 26)
        self.colFLsel = Gtk.TreeViewColumn("in\nlegend", renderer_togFLS, active=26, visible=0)
        self.colFLsel.set_resizable(True)
        self.tableFeat.append_column(self.colFLsel)

        self.colSpace6 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableFeat.append_column(self.colSpace6)


        renderer_comboFType = CGeno.CellRendererComboNoEntry(self.listFeatType)
        renderer_comboFType.connect("edited", self.on_comboFeat_changed, 5)
        self.colFType = Gtk.TreeViewColumn("type", renderer_comboFType, text=5, visible=0)
        self.colFType.set_property("min-width", self.max_colsize(self.listFeatType))
        self.colFType.set_resizable(True)
        self.tableFeat.append_column(self.colFType)

        renderer_spinFFi = Gtk.CellRendererText()
        renderer_spinFFi.set_property("editable", True)
        renderer_spinFFi.connect("edited", self.on_comboFeat_changed, 16)
        self.colFFilter = Gtk.TreeViewColumn("filter", renderer_spinFFi, text=16, visible=0)
        self.colFFilter.set_property("fixed-width", 125)
        self.colFFilter.set_resizable(True)
        self.tableFeat.append_column(self.colFFilter)

        renderer_comboFFilterF = CGeno.CellRendererComboNoEntry(self.listFeatFilterF)
        renderer_comboFFilterF.connect("edited", self.on_comboFeat_changed, 17)
        self.colFFilterField = Gtk.TreeViewColumn("in field", renderer_comboFFilterF, text=17, visible=0)
#        self.colFFilterField.set_property("min-width", self.max_colsize(self.listFeatFilterF))
        self.colFFilterField.set_property("fixed-width", 60)
        self.colFFilterField.set_resizable(True)
        self.tableFeat.append_column(self.colFFilterField)

        self.colSpace7 = Gtk.TreeViewColumn(" ", renderer_space)
        self.tableFeat.append_column(self.colSpace7)

        renderer_comboFStrand = CGeno.CellRendererComboNoEntry(self.listFeatStrand)
        renderer_comboFStrand.connect("edited", self.on_comboFeat_changed, 6)
        self.colFStrand = Gtk.TreeViewColumn("strand", renderer_comboFStrand, text=6)
        self.colFStrand.set_property("min-width", self.max_colsize(self.listFeatStrand))
        self.colFStrand.set_resizable(True)
        self.tableFeat.append_column(self.colFStrand)

        renderer_comboFShape = CGeno.CellRendererComboNoEntry(self.listFeatShape)
        renderer_comboFShape.connect("edited", self.on_comboFeat_changed, 7)
        self.colFShape = Gtk.TreeViewColumn("shape", renderer_comboFShape, text=7)
        self.colFShape.set_property("min-width", self.max_colsize(self.listFeatShape))
        self.colFShape.set_resizable(True)
        self.tableFeat.append_column(self.colFShape)

        # Caution, the feature heigh is a ratio of the sequence height !!
        renderer_spinFH = Gtk.CellRendererText()
        renderer_spinFH.set_property("editable", True)
        renderer_spinFH.connect("edited", self.on_float_changed, self.listFeat, 9, -50, 50)
        self.colFHeight = Gtk.TreeViewColumn("height\nratio", renderer_spinFH, text=9)
        self.colFHeight.set_resizable(True)
        self.tableFeat.append_column(self.colFHeight)


        self.colSpace8 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableFeat.append_column(self.colSpace8)


        renderer_togFF = Gtk.CellRendererToggle()
        renderer_togFF.connect("toggled", self.on_toggleFeat_changed, 12)
        self.colFFill = Gtk.TreeViewColumn("fill", renderer_togFF, active=12, visible=0)
        self.colFFill.set_resizable(True)
        self.tableFeat.append_column(self.colFFill)

        renderer_butColFF = Gtk.CellRendererText()
        renderer_butColFF.set_property("editable", True)
        renderer_butColFF.connect("editing-started", self.on_change_colCell, self.listFeat, 10, 11, "Feature fill")
        self.colFFillCol = Gtk.TreeViewColumn('\ncolor', renderer_butColFF, text=10)
        self.colFFillCol.set_property("fixed-width", 45)
        self.colFFillCol.add_attribute(renderer_butColFF, "cell-background", 11)
        self.colFFillCol.set_resizable(True)
        self.tableFeat.append_column(self.colFFillCol)

        self.colSpace8 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableFeat.append_column(self.colSpace8)

        self.colSpaceline = Gtk.TreeViewColumn("line", renderer_space)
        self.tableFeat.append_column(self.colSpaceline)

        renderer_spinFLW = Gtk.CellRendererText()
        renderer_spinFLW.set_property("editable", True)
        renderer_spinFLW.connect("edited", self.on_int_changed, self.listFeat, 13, 0, 50)
        self.colFLWidth = Gtk.TreeViewColumn("\nwidth", renderer_spinFLW, text=13)
        self.colFLWidth.set_resizable(True)
        self.tableFeat.append_column(self.colFLWidth)

        renderer_butColFL = Gtk.CellRendererText()
        renderer_butColFL.set_property("editable", True)
        renderer_butColFL.connect("editing-started", self.on_change_colCell, self.listFeat, 14, 15, "Feature line")
        self.colFLineCol = Gtk.TreeViewColumn('\ncolor', renderer_butColFL, text=14)
        self.colFLineCol.set_property("fixed-width", 45)
        self.colFLineCol.add_attribute(renderer_butColFL, "cell-background", 15)
        self.colFLineCol.set_resizable(True)
        self.tableFeat.append_column(self.colFLineCol)

        renderer_comboFHatch = CGeno.CellRendererComboNoEntry(self.listFeatHatch)
        renderer_comboFHatch.connect("edited", self.on_comboFeat_changed, 8)
        self.colFHatch = Gtk.TreeViewColumn("\nhatching", renderer_comboFHatch, text=8)
        self.colFHatch.set_property("min-width", self.max_colsize(self.listFeatHatch))
        self.colFHatch.set_resizable(True)
        self.tableFeat.append_column(self.colFHatch)

        self.colSpace8 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableFeat.append_column(self.colSpace8)


        renderer_spinFLPr = Gtk.CellRendererToggle()
        renderer_spinFLPr.connect("toggled", self.on_toggleFeat_changed, 19)
        self.colFLPrint = Gtk.TreeViewColumn("label", renderer_spinFLPr, active=19, visible=0)
        self.colFLPrint.set_resizable(True)
        self.tableFeat.append_column(self.colFLPrint)

        renderer_comboFLType = CGeno.CellRendererComboNoEntry(self.listSeqFeatLabType)
        renderer_comboFLType.connect("edited", self.on_comboFeat_changed, 20)
        self.colFLType = Gtk.TreeViewColumn("\ntype", renderer_comboFLType, text=20, visible=0)
        self.colFLType.set_resizable(True)
        self.tableFeat.append_column(self.colFLType)

        renderer_comboFLPos = CGeno.CellRendererComboNoEntry(self.listFeatLabPos)
        renderer_comboFLPos.connect("edited", self.on_comboFeat_changed, 21)
        self.colFLPos = Gtk.TreeViewColumn("\nposition", renderer_comboFLPos, text=21, visible=0)
        self.colFLPos.set_property("min-width", self.max_colsize(self.listFeatLabPos))
        self.colFLPos.set_resizable(True)
        self.tableFeat.append_column(self.colFLPos)

        renderer_spinFLBr = Gtk.CellRendererToggle()
        renderer_spinFLBr.connect("toggled", self.on_toggleFeat_changed, 27)
        self.colFLBold = Gtk.TreeViewColumn("\nB", renderer_spinFLBr, active=27)
        self.colFLBold.set_resizable(False)
        self.tableFeat.append_column(self.colFLBold)

        renderer_spinFLIr = Gtk.CellRendererToggle()
        renderer_spinFLIr.connect("toggled", self.on_toggleFeat_changed, 28)
        self.colFLItal = Gtk.TreeViewColumn("\nI", renderer_spinFLIr, active=28)
        self.colFLItal.set_resizable(False)
        self.tableFeat.append_column(self.colFLItal)


        renderer_butFLCol = Gtk.CellRendererText()
        renderer_butFLCol.set_property("editable", True)
        renderer_butFLCol.connect("editing-started", self.on_change_colCell, self.listFeat, 22, 23, "Feature label")
        self.colFLColor = Gtk.TreeViewColumn('\ncolor', renderer_butFLCol, text=22)
        self.colFLColor.set_property("fixed-width", 45)
        self.colFLColor.add_attribute(renderer_butFLCol, "cell-background", 23)
        self.colFLColor.set_resizable(True)
        self.tableFeat.append_column(self.colFLColor)

 
        renderer_spinFLS = Gtk.CellRendererText()
        renderer_spinFLS.set_property("editable", True)
        renderer_spinFLS.connect("edited", self.on_int_changed, self.listFeat, 24, 1, 300)
        self.colFLabSize = Gtk.TreeViewColumn("\nsize", renderer_spinFLS, text=24, visible=0)
        self.colFLabSize.set_resizable(True)
        self.tableFeat.append_column(self.colFLabSize)

        renderer_spinFLR = Gtk.CellRendererText()
        renderer_spinFLR.set_property("editable", True)
        renderer_spinFLR.connect("edited", self.on_int_changed, self.listFeat, 25, -360, 360)
        self.colFLabRot = Gtk.TreeViewColumn("\nrot", renderer_spinFLR, text=25, visible=0)
        self.colFLabRot.set_resizable(True)
        self.tableFeat.append_column(self.colFLabRot)


        #Populate the Feature list with 'general'
        self.addFeat(False, "General")
        self.featInc = 1


# BlastN parameters page
#-------------------------------------------------------------------------

        # File selection part
        self.labelBlastN = Gtk.Label(label="BLASTN", name="label-blast")
        self.labelBlastN.set_property("margin", 0)
        self.butMinMaxBN = Gtk.Button.new_from_icon_name("window-minimize-symbolic", 1)
        self.butMinMaxBN.set_property("margin-start", 5)
        self.butMinMaxBN.set_relief(Gtk.ReliefStyle.NONE) 
        self.boxLabelBN = Gtk.Box()
        self.boxLabelBN.add(self.labelBlastN)
        self.boxLabelBN.add(self.butMinMaxBN)

        self.buttonBNrun  = Gtk.Button(label="Run BlastN", name="blast-button")
        self.buttonBNrun.connect("clicked", self.on_BlastRun_clicked, "blastn")
        self.buttonBNload = Gtk.Button(label="Load file")
        self.buttonBNload.connect("clicked", self.on_BlastLoad_clicked, "blastn")

        # structure for file display
        self.listBlastN = Gtk.ListStore(GObject.TYPE_PYOBJECT,   #  0 = remove action
                                        GObject.TYPE_STRING)     #  1 = blastN filename
        self.treeviewBN    = Gtk.TreeView(model=self.listBlastN)
        self.treeviewBN.set_headers_visible(False)
        sel = self.treeviewBN.get_selection()
        sel.set_mode(Gtk.SelectionMode.NONE)

        self.scrollBN = Gtk.ScrolledWindow()
        self.scrollBN.set_size_request(300, 80)
        self.scrollBN.add(self.treeviewBN)

        renderer_butBN = CGeno.CellRendererButton()
        renderer_butBN.set_property("icon_name", "window-close")
        renderer_butBN.set_property("width", 15)
        self.colbutBN = Gtk.TreeViewColumn("", renderer_butBN, callable=0)
        self.treeviewBN.append_column(self.colbutBN)

        renderer_textBN = Gtk.CellRendererText()
        self.colnameBN = Gtk.TreeViewColumn("", renderer_textBN, text=1)
        self.treeviewBN.append_column(self.colnameBN)



        self.gridBNRun = Gtk.Grid()
        self.gridBNRun.attach(self.buttonBNrun, 0, 0, 1, 1)
        self.gridBNRun.attach(self.buttonBNload, 1, 0, 1, 1)
        self.gridBNRun.attach(self.scrollBN, 0, 1, 2, 1)

        # Display options part
        self.labelBND = Gtk.Label(label="Matches display options", name="label-blast", xalign=0, margin_bottom=10)

        self.labelBNDlength = Gtk.Label(label="minimum length:", xalign=0)
        self.labelBNDsim = Gtk.Label(label="minimum similarity:", xalign=0)
        self.labelBNDeval = Gtk.Label(label="minimum e-value:", xalign=0)
        self.labelBNDdist = Gtk.Label(label="distance to sequence:", xalign=0)
        self.entryBNDlength = CGeno.EntrySmall(6, "100", 0, 1000000000)
        self.entryBNDsim = CGeno.EntrySmall(6, "75", 0, 100)
        self.entryBNDeval = CGeno.EntrySmallFloat(6, "0.001", 0, 10)
        self.entryBNDdist = CGeno.EntrySmallSameAs(6, "-", 0, 1000)


        self.labelBNDcol = Gtk.Label(label="color (min/max):", xalign=0, margin_start=20)
        self.labelBNDrevcol = Gtk.Label(label="reversed color:", xalign=0, margin_start=20)
        self.labelBNDopa = Gtk.Label(label="opacity:", xalign=0, margin_start=20)
        self.labelBNDoutline = Gtk.Label(label="outline matches:", xalign=0, margin_start=20)

        self.buttonBNDcolmin=Gtk.ColorButton.new_with_rgba(self.blastncol_color)
        self.buttonBNDcolmax=Gtk.ColorButton.new_with_rgba(self.blastncol_color)
        self.buttonBNDrevcolmin=Gtk.ColorButton.new_with_rgba(self.blastncol_color)
        self.buttonBNDrevcolmax=Gtk.ColorButton.new_with_rgba(self.blastncol_color)
        self.entryBNDopa = CGeno.EntrySmallFloat(3, "0.7", 0, 1)

        self.buttonBNDcolmin.set_relief(Gtk.ReliefStyle.NONE)
        self.buttonBNDcolmax.set_relief(Gtk.ReliefStyle.NONE)
        self.buttonBNDrevcolmin.set_relief(Gtk.ReliefStyle.NONE)
        self.buttonBNDrevcolmax.set_relief(Gtk.ReliefStyle.NONE)

        self.checkBNDrevcol=Gtk.CheckButton()
        self.checkBNDrevcol.connect("toggled", self.on_toggledColor, self.buttonBNDrevcolmin, self.buttonBNDrevcolmax)
        self.checkBNDoutline=Gtk.CheckButton()
        self.checkBNDoutline.set_active(True)

        self.gridBNDisplay = Gtk.Grid()
        self.gridBNDisplay.set_property("column-spacing", 5)
        self.gridBNDisplay.set_property("margin-bottom", 5)

        self.gridBNDisplay.attach(self.labelBND, 0, 0, 2, 1)
        self.gridBNDisplay.attach(self.labelBNDlength, 0, 1, 1, 1)
        self.gridBNDisplay.attach(self.labelBNDsim, 0, 2, 1, 1)
        self.gridBNDisplay.attach(self.labelBNDeval, 0, 3, 1, 1)
        self.gridBNDisplay.attach(self.labelBNDdist, 0, 4, 1, 1)
        self.gridBNDisplay.attach(self.entryBNDlength, 1, 1, 1, 1)
        self.gridBNDisplay.attach(self.entryBNDsim, 1, 2, 1, 1)
        self.gridBNDisplay.attach(self.entryBNDeval, 1, 3, 1, 1)
        self.gridBNDisplay.attach(self.entryBNDdist, 1, 4, 1, 1)

        self.gridBNDisplay.attach(self.labelBNDcol, 2, 1, 2, 1)
        self.gridBNDisplay.attach(self.labelBNDrevcol, 2, 2, 1, 1)
        self.gridBNDisplay.attach(self.labelBNDopa, 2, 3, 2, 1)
        self.gridBNDisplay.attach(self.labelBNDoutline, 2, 4, 2, 1)
        self.gridBNDisplay.attach(self.checkBNDrevcol, 3, 2, 1, 1)
        self.gridBNDisplay.attach(self.buttonBNDcolmin, 4, 1, 1, 1)
        self.gridBNDisplay.attach(self.buttonBNDcolmax, 5, 1, 1, 1)
        self.gridBNDisplay.attach(self.buttonBNDrevcolmin, 4, 2, 1, 1)
        self.gridBNDisplay.attach(self.buttonBNDrevcolmax, 5, 2, 1, 1)
        self.gridBNDisplay.attach(self.entryBNDopa, 4, 3, 1, 1)
        self.gridBNDisplay.attach(self.checkBNDoutline, 4, 4, 1, 1)

        # Label options part
        self.labelBNL = Gtk.Label(label="Label options", name="label-blast", xalign=0, margin_bottom=10)
        self.labelBNLshow = Gtk.Label(label="show labels:", xalign=0)
        self.labelBNLmin = Gtk.Label(label="min. match size:", xalign=0)
        self.labelBNLcol = Gtk.Label(label="label color:", xalign=0)
        self.labelBNLsize = Gtk.Label(label="label size:", xalign=0)
        self.labelBNLdec = Gtk.Label(label="decimals:", xalign=0)
        self.checkBNLshow=Gtk.CheckButton(margin_bottom=5)
        self.entryBNLmin = CGeno.EntrySmall(5, "1000", 0, 1000000000)
        self.buttonBNLcol=Gtk.ColorButton.new_with_rgba(self.black_color)
        self.buttonBNLcol.set_relief(Gtk.ReliefStyle.NONE)
        self.entryBNLsize = CGeno.EntrySmall(3, "20", 1, 200)
        self.entryBNLdec = CGeno.EntrySmall(2, "0", 0, 5)


        self.gridBNLabel = Gtk.Grid()
        self.gridBNLabel.set_property("column-spacing", 5)

        self.gridBNLabel.attach(self.labelBNL, 0, 0, 2, 1)
        self.gridBNLabel.attach(self.labelBNLshow, 0, 1, 1, 1)
        self.gridBNLabel.attach(self.labelBNLmin, 0, 2, 1, 1)
        self.gridBNLabel.attach(self.labelBNLcol, 0, 3, 1, 1)
        self.gridBNLabel.attach(self.labelBNLsize, 0, 4, 1, 1)
        self.gridBNLabel.attach(self.labelBNLdec, 0, 5, 1, 1)
        self.gridBNLabel.attach(self.checkBNLshow, 1, 1, 1, 1)
        self.gridBNLabel.attach(self.entryBNLmin, 1, 2, 1, 1)
        self.gridBNLabel.attach(self.buttonBNLcol, 1, 3, 1, 1)
        self.gridBNLabel.attach(self.entryBNLsize, 1, 4, 1, 1)
        self.gridBNLabel.attach(self.entryBNLdec, 1, 5, 1, 1)


        # Selection options part
        self.labelBNS = Gtk.Label(label="Matches selection", name="label-blast", xalign=0, margin_bottom=10)
        self.radioBNNone = Gtk.RadioButton.new_with_label_from_widget(None, "None")
        self.radioBNAdj = Gtk.RadioButton.new_with_label_from_widget(self.radioBNNone, "Adjacent")
        self.radioBNAll = Gtk.RadioButton.new_with_label_from_widget(self.radioBNNone, "All vs. All")
        self.radioBNOne = Gtk.RadioButton.new_with_label_from_widget(self.radioBNNone, "All vs. ")
        self.radioBNCustom = Gtk.RadioButton.new_with_label_from_widget(self.radioBNNone, "Custom")

        self.comboBNOne = Gtk.ComboBoxText()
        self.comboBNOne.append('1', "No Sequence")
        self.comboBNOne.set_active_id('1')
        self.comboBNOne.set_state_flags(Gtk.StateFlags.INSENSITIVE, False )
        self.radioBNOne.connect("clicked", self.on_radioB_changed, self.comboBNOne)

        self.buttonBNCustom = Gtk.Button(label="show")
        self.buttonBNCustom.set_state_flags(Gtk.StateFlags.INSENSITIVE, False )
        self.radioBNCustom.connect("clicked", self.on_radioB_changed, self.buttonBNCustom)
        self.buttonBNCustom.connect("clicked", self.customB_window, "BLASTN")
        self.BNDialog = None
        self.BNDchecked = None

        self.gridBNChoice = Gtk.Grid()
        self.gridBNChoice.set_property("column-spacing", 5)
        self.gridBNChoice.set_property("row-spacing", 4)
        self.gridBNChoice.attach(self.labelBNS, 0, 0, 2, 1)
        self.gridBNChoice.attach(self.radioBNNone, 0, 1, 1, 1)
        self.gridBNChoice.attach(self.radioBNAdj, 0, 2, 1, 1)
        self.gridBNChoice.attach(self.radioBNAll, 0, 3, 1, 1)
        self.gridBNChoice.attach(self.radioBNOne, 0, 4, 1, 1)
        self.gridBNChoice.attach(self.comboBNOne, 1, 4, 1, 1)
        self.gridBNChoice.attach(self.radioBNCustom, 0, 5, 1, 1)
        self.gridBNChoice.attach(self.buttonBNCustom, 1, 5, 1, 1)
        
        self.boxBN = Gtk.Box()
        self.boxBN.pack_start(self.gridBNRun, False, False, 10)
        self.boxBN.pack_start(self.gridBNDisplay, False, False, 10)
        self.boxBN.pack_start(self.gridBNLabel, False, False, 40)
        self.boxBN.pack_start(self.gridBNChoice, False, False, 40)
        self.boxBN.set_property("margin-top", 5)
        self.boxBN.set_property("margin-bottom", 2)
        self.butMinMaxBN.connect("clicked", self.on_minmaxBlastframe, self.boxBN)

        self.frameBN = Gtk.Frame()
        self.frameBN.set_label_widget(self.boxLabelBN)
        self.frameBN.set_property("margin-end", 10)
        self.frameBN.set_property("margin-start", 10)
        self.frameBN.set_property("margin-bottom", 3)
        self.frameBN.add_child(Gtk.Builder.new(), self.boxBN, None)


# TBlastX parameters page
#-------------------------------------------------------------------------

        # File selection part
        self.labelBlastX = Gtk.Label(label="TBLASTX", name="label-blast")
        self.labelBlastX.set_property("margin", 0)
        self.butMinMaxBX = Gtk.Button.new_from_icon_name("window-maximize-symbolic", 1)
        self.butMinMaxBX.set_property("margin-start", 5)
        self.butMinMaxBX.set_relief(Gtk.ReliefStyle.NONE) 
        self.boxLabelBX = Gtk.Box()
        self.boxLabelBX.add(self.labelBlastX)
        self.boxLabelBX.add(self.butMinMaxBX)


        self.buttonBXrun  = Gtk.Button(label="Run TBlastX", name="blast-button")
        self.buttonBXrun.connect("clicked", self.on_BlastRun_clicked, "tblastx")
        self.buttonBXload = Gtk.Button(label="Load file")
        self.buttonBXload.connect("clicked", self.on_BlastLoad_clicked, "tblastx")

        # structure for file display
        self.listBlastX = Gtk.ListStore(GObject.TYPE_PYOBJECT,   #  0 = remove action
                                        GObject.TYPE_STRING)     #  1 = blastN filename
        self.treeviewBX    = Gtk.TreeView(model=self.listBlastX)
        self.treeviewBX.set_headers_visible(False)
        sel = self.treeviewBX.get_selection()
        sel.set_mode(Gtk.SelectionMode.NONE)

        self.scrollBX = Gtk.ScrolledWindow()
        self.scrollBX.set_size_request(300, 80)
        self.scrollBX.add(self.treeviewBX)

        renderer_butBX = CGeno.CellRendererButton()
        renderer_butBX.set_property("icon_name", "window-close")
        renderer_butBX.set_property("width", 15)
        self.colbutBX = Gtk.TreeViewColumn("", renderer_butBX, callable=0)
        self.treeviewBX.append_column(self.colbutBX)

        renderer_textBX = Gtk.CellRendererText()
        self.colnameBX = Gtk.TreeViewColumn("", renderer_textBX, text=1)
        self.treeviewBX.append_column(self.colnameBX)

        self.gridBXRun = Gtk.Grid()
        self.gridBXRun.attach(self.buttonBXrun, 0, 0, 1, 1)
        self.gridBXRun.attach(self.buttonBXload, 1, 0, 1, 1)
        self.gridBXRun.attach(self.scrollBX, 0, 1, 2, 1)


        # Display options part
        self.labelBXD = Gtk.Label(label="Matches display options", name="label-blast", xalign=0, margin_bottom=10)

        self.labelBXDlength = Gtk.Label(label="minimum length:", xalign=0)
        self.labelBXDsim = Gtk.Label(label="minimum similarity:", xalign=0)
        self.labelBXDeval = Gtk.Label(label="minimum e-value:", xalign=0)
        self.labelBXDdist = Gtk.Label(label="distance to sequence:", xalign=0)
        self.entryBXDlength = CGeno.EntrySmall(6, "40", 0, 1000000000)
        self.entryBXDsim = CGeno.EntrySmall(6, "30", 0, 100)
        self.entryBXDeval = CGeno.EntrySmallFloat(6, "0.001", 0, 10)
        self.entryBXDdist = CGeno.EntrySmallSameAs(6, "-", 0, 1000)


        self.labelBXDcol = Gtk.Label(label="color (min/max):", xalign=0, margin_start=20)
        self.labelBXDrevcol = Gtk.Label(label="reversed color:", xalign=0, margin_start=20)
        self.labelBXDopa = Gtk.Label(label="opacity:", xalign=0, margin_start=20)
        self.labelBXDoutline = Gtk.Label(label="outline matches:", xalign=0, margin_start=20)
        self.labelBXDmerge = Gtk.Label(label="merge matches:", xalign=0, margin_start=20)

        self.buttonBXDcolmin=Gtk.ColorButton.new_with_rgba(self.blastxcol_color)
        self.buttonBXDcolmax=Gtk.ColorButton.new_with_rgba(self.blastxcol_color)
        self.buttonBXDrevcolmin=Gtk.ColorButton.new_with_rgba(self.blastxcol_color)
        self.buttonBXDrevcolmax=Gtk.ColorButton.new_with_rgba(self.blastxcol_color)
        self.entryBXDopa = CGeno.EntrySmallFloat(3, "0.7", 0, 1)

        self.buttonBXDcolmin.set_relief(Gtk.ReliefStyle.NONE)
        self.buttonBXDcolmax.set_relief(Gtk.ReliefStyle.NONE)
        self.buttonBXDrevcolmin.set_relief(Gtk.ReliefStyle.NONE)
        self.buttonBXDrevcolmax.set_relief(Gtk.ReliefStyle.NONE)

        self.checkBXDrevcol=Gtk.CheckButton()
        self.checkBXDrevcol.connect("toggled", self.on_toggledColor, self.buttonBXDrevcolmin, self.buttonBXDrevcolmax)
        self.checkBXDoutline=Gtk.CheckButton()
        self.checkBXDoutline.set_active(True)
        self.checkBXDmerge=Gtk.CheckButton()
        self.checkBXDmerge.set_active(True)

        self.gridBXDisplay = Gtk.Grid()
        self.gridBXDisplay.set_property("column-spacing", 5)
        self.gridBXDisplay.set_property("margin-bottom", 5)

        self.gridBXDisplay.attach(self.labelBXD, 0, 0, 2, 1)
        self.gridBXDisplay.attach(self.labelBXDlength, 0, 1, 1, 1)
        self.gridBXDisplay.attach(self.labelBXDsim, 0, 2, 1, 1)
        self.gridBXDisplay.attach(self.labelBXDeval, 0, 3, 1, 1)
        self.gridBXDisplay.attach(self.labelBXDdist, 0, 4, 1, 1)
        self.gridBXDisplay.attach(self.entryBXDlength, 1, 1, 1, 1)
        self.gridBXDisplay.attach(self.entryBXDsim, 1, 2, 1, 1)
        self.gridBXDisplay.attach(self.entryBXDeval, 1, 3, 1, 1)
        self.gridBXDisplay.attach(self.entryBXDdist, 1, 4, 1, 1)

        self.gridBXDisplay.attach(self.labelBXDcol, 2, 1, 2, 1)
        self.gridBXDisplay.attach(self.labelBXDrevcol, 2, 2, 1, 1)
        self.gridBXDisplay.attach(self.labelBXDopa, 2, 3, 2, 1)
        self.gridBXDisplay.attach(self.labelBXDoutline, 2, 4, 2, 1)
        self.gridBXDisplay.attach(self.labelBXDmerge, 2, 5, 2, 1)
        self.gridBXDisplay.attach(self.checkBXDrevcol, 3, 2, 1, 1)
        self.gridBXDisplay.attach(self.buttonBXDcolmin, 4, 1, 1, 1)
        self.gridBXDisplay.attach(self.buttonBXDcolmax, 5, 1, 1, 1)
        self.gridBXDisplay.attach(self.buttonBXDrevcolmin, 4, 2, 1, 1)
        self.gridBXDisplay.attach(self.buttonBXDrevcolmax, 5, 2, 1, 1)
        self.gridBXDisplay.attach(self.entryBXDopa, 4, 3, 1, 1)
        self.gridBXDisplay.attach(self.checkBXDoutline, 4, 4, 1, 1)
        self.gridBXDisplay.attach(self.checkBXDmerge, 4, 5, 1, 1)

        # Label options part
        self.labelBXL = Gtk.Label(label="Label options", name="label-blast", xalign=0, margin_bottom=10)
        self.labelBXLshow = Gtk.Label(label="show labels:", xalign=0)
        self.labelBXLmin = Gtk.Label(label="min. match size:", xalign=0)
        self.labelBXLcol = Gtk.Label(label="label color:", xalign=0)
        self.labelBXLsize = Gtk.Label(label="label size:", xalign=0)
        self.labelBXLdec = Gtk.Label(label="decimals:", xalign=0)
        self.checkBXLshow=Gtk.CheckButton(margin_bottom=5)
        self.entryBXLmin = CGeno.EntrySmall(5, "100", 0, 1000000000)
        self.buttonBXLcol=Gtk.ColorButton.new_with_rgba(self.black_color)
        self.buttonBXLcol.set_relief(Gtk.ReliefStyle.NONE)
        self.entryBXLsize = CGeno.EntrySmall(3, "20", 1, 200)
        self.entryBXLdec = CGeno.EntrySmall(2, "0", 0, 5)

        self.gridBXLabel = Gtk.Grid()
        self.gridBXLabel.set_property("column-spacing", 5)

        self.gridBXLabel.attach(self.labelBXL, 0, 0, 2, 1)
        self.gridBXLabel.attach(self.labelBXLshow, 0, 1, 1, 1)
        self.gridBXLabel.attach(self.labelBXLmin, 0, 2, 1, 1)
        self.gridBXLabel.attach(self.labelBXLcol, 0, 3, 1, 1)
        self.gridBXLabel.attach(self.labelBXLsize, 0, 4, 1, 1)
        self.gridBXLabel.attach(self.labelBXLdec, 0, 5, 1, 1)
        self.gridBXLabel.attach(self.checkBXLshow, 1, 1, 1, 1)
        self.gridBXLabel.attach(self.entryBXLmin, 1, 2, 1, 1)
        self.gridBXLabel.attach(self.buttonBXLcol, 1, 3, 1, 1)
        self.gridBXLabel.attach(self.entryBXLsize, 1, 4, 1, 1)
        self.gridBXLabel.attach(self.entryBXLdec, 1, 5, 1, 1)

        # Selection options part
        self.labelBXS = Gtk.Label(label="Matches selection", name="label-blast", xalign=0, margin_bottom=10)
        self.radioBXNone = Gtk.RadioButton.new_with_label_from_widget(None, "None")
        self.radioBXAdj = Gtk.RadioButton.new_with_label_from_widget(self.radioBXNone, "Adjacent")
        self.radioBXAll = Gtk.RadioButton.new_with_label_from_widget(self.radioBXNone, "All vs. All")
        self.radioBXOne = Gtk.RadioButton.new_with_label_from_widget(self.radioBXNone, "All vs. ")
        self.radioBXCustom = Gtk.RadioButton.new_with_label_from_widget(self.radioBXNone, "Custom")

        self.comboBXOne = Gtk.ComboBoxText()
        self.comboBXOne.append('1', "No Sequence")
        self.comboBXOne.set_active_id('1')
        self.comboBXOne.set_state_flags(Gtk.StateFlags.INSENSITIVE, False )
        self.radioBXOne.connect("clicked", self.on_radioB_changed, self.comboBXOne)

        self.buttonBXCustom = Gtk.Button(label="show")
        self.buttonBXCustom.set_state_flags(Gtk.StateFlags.INSENSITIVE, False )
        self.radioBXCustom.connect("clicked", self.on_radioB_changed, self.buttonBXCustom)
        self.buttonBXCustom.connect("clicked", self.customB_window, "TBLASTX")
        self.BXDialog = None
        self.BXDchecked = None

        self.gridBXChoice = Gtk.Grid()
        self.gridBXChoice.set_property("column-spacing", 5)
        self.gridBXChoice.set_property("row-spacing", 4)
        self.gridBXChoice.attach(self.labelBXS, 0, 0, 2, 1)
        self.gridBXChoice.attach(self.radioBXNone, 0, 1, 1, 1)
        self.gridBXChoice.attach(self.radioBXAdj, 0, 2, 1, 1)
        self.gridBXChoice.attach(self.radioBXAll, 0, 3, 1, 1)
        self.gridBXChoice.attach(self.radioBXOne, 0, 4, 1, 1)
        self.gridBXChoice.attach(self.comboBXOne, 1, 4, 1, 1)
        self.gridBXChoice.attach(self.radioBXCustom, 0, 5, 1, 1)
        self.gridBXChoice.attach(self.buttonBXCustom, 1, 5, 1, 1)

        self.boxBX = Gtk.Box()
        self.boxBX.pack_start(self.gridBXRun, False, False, 10)
        self.boxBX.pack_start(self.gridBXDisplay, False, False, 10)
        self.boxBX.pack_start(self.gridBXLabel, False, False, 40)
        self.boxBX.pack_start(self.gridBXChoice, False, False, 40)
        self.boxBX.set_property("margin-top", 5)
        self.boxBX.set_property("margin-bottom", 2)
        self.butMinMaxBX.connect("clicked", self.on_minmaxBlastframe, self.boxBX)

        self.frameBX = Gtk.Frame()
        self.frameBX.set_label_widget(self.boxLabelBX)
        self.frameBX.set_property("margin-end", 10)
        self.frameBX.set_property("margin-start", 10)
        self.frameBX.set_property("margin-bottom", 15)
        self.frameBX.add_child(Gtk.Builder.new(), self.boxBX, None)

        self.boxBlasts = Gtk.Box.new(Gtk.Orientation.VERTICAL, 5)
        self.boxBlasts.pack_start(self.frameBN, False, False, 0)
        self.boxBlasts.pack_start(self.frameBX, False, False, 0)

# Legend parameters page
#-----------------------------------------------------------------

        # Creating Main Legends Box
        self.boxLegends = Gtk.Box(spacing=100)
        self.boxLegends.set_margin_top(50)

        # Creating Legends Grid included in Box
        self.gridLegends = Gtk.Grid()
        self.gridLegends.set_column_spacing(10)
        self.gridLegends.set_property("column-spacing", 30)
        self.gridLegends.set_property("row-spacing", 30)

        # First row
        self.displaysText = Gtk.Label()
        self.displaysText.set_markup("<b>General display</b>")
        self.gridLegends.attach(self.displaysText, 0, 0, 2, 1)

        # General displays
        self.dispScale_label = Gtk.Label(label="Display scale")
        self.dispScale_button = Gtk.Switch()
        self.dispScale_button.props.halign = Gtk.Align.START
        
        self.dispFeat_label = Gtk.Label(label="Display features")
        self.dispFeat_button = Gtk.Switch()
        self.dispFeat_button.props.halign = Gtk.Align.START
        self.dispBlast_label = Gtk.Label(label="Display BLAST")
        self.dispBlast_button = Gtk.Switch()
        self.dispBlast_button.props.halign = Gtk.Align.START
        self.dispBlastX_label = Gtk.Label(label="Display TBLASTX")
        self.dispBlastX_button = Gtk.Switch()
        self.dispBlastX_button.props.halign = Gtk.Align.START

        # Attach to grid general displays
        self.gridLegends.attach(self.dispScale_label, 0, 2, 1, 1)
        self.gridLegends.attach(self.dispScale_button, 1, 2, 1, 1)
        self.gridLegends.attach(self.dispFeat_label, 0, 3, 1, 1)
        self.gridLegends.attach(self.dispFeat_button, 1, 3, 1, 1)
        self.gridLegends.attach(self.dispBlast_label, 0, 4, 1, 1)
        self.gridLegends.attach(self.dispBlast_button, 1, 4, 1, 1)
        self.gridLegends.attach(self.dispBlastX_label, 0, 5, 1, 1)
        self.gridLegends.attach(self.dispBlastX_button, 1, 5, 1, 1)


        # Horizontal Positions
        self.hpositionstext = Gtk.Label()
        self.hpositionstext.set_markup("<b>Horizontal position</b>")

        self.hposFeat_combo = Gtk.ComboBoxText()
        self.hposFeat_combo.append('1', "Left")
        self.hposFeat_combo.append('2', "Middle")
        self.hposFeat_combo.append('3', "Right")

        self.hposBlast_combo = Gtk.ComboBoxText()
        self.hposBlast_combo.append('1', "Left")
        self.hposBlast_combo.append('2', "Middle")
        self.hposBlast_combo.append('3', "Right")
        
        self.hposBlastX_combo = Gtk.ComboBoxText()
        self.hposBlastX_combo.append('1', "Left")
        self.hposBlastX_combo.append('2', "Middle")
        self.hposBlastX_combo.append('3', "Right")

        self.hposScale_combo = Gtk.ComboBoxText()
        self.hposScale_combo.append('1', "Left")
        self.hposScale_combo.append('2', "Middle")
        self.hposScale_combo.append('3', "Right")

        # Vertical Positions
        self.vpositionstext = Gtk.Label()
        self.vpositionstext.set_markup("<b>Vertical position</b>")

        self.vposFeat_combo = Gtk.ComboBoxText()
        self.vposFeat_combo.append('1', "Top")
        self.vposFeat_combo.append('2', "Middle")
        self.vposFeat_combo.append('3', "Bottom")

        self.vposBlast_combo = Gtk.ComboBoxText()
        self.vposBlast_combo.append('1', "Top")
        self.vposBlast_combo.append('2', "Middle")
        self.vposBlast_combo.append('3', "Bottom")

        self.vposBlastX_combo = Gtk.ComboBoxText()
        self.vposBlastX_combo.append('1', "Top")
        self.vposBlastX_combo.append('2', "Middle")
        self.vposBlastX_combo.append('3', "Bottom")

        self.vposScale_combo = Gtk.ComboBoxText()
        self.vposScale_combo.append('1', "Top")
        self.vposScale_combo.append('2', "Middle")
        self.vposScale_combo.append('3', "Bottom")

        # Attach to grid positions
        self.gridLegends.attach(self.hpositionstext, 2, 0, 1, 1)
        self.gridLegends.attach(self.hposFeat_combo, 2, 3, 1, 1)
        self.gridLegends.attach(self.hposBlast_combo, 2, 4, 1, 1)
        self.gridLegends.attach(self.hposBlastX_combo, 2, 5, 1, 1)
        self.gridLegends.attach(self.hposScale_combo, 2, 2, 1, 1)
        self.gridLegends.attach(self.vpositionstext, 3, 0, 1, 1)
        self.gridLegends.attach(self.vposFeat_combo, 3, 3, 1, 1)
        self.gridLegends.attach(self.vposBlast_combo, 3, 4, 1, 1)
        self.gridLegends.attach(self.vposBlastX_combo, 3, 5, 1, 1)
        self.gridLegends.attach(self.vposScale_combo, 3, 2, 1, 1)

        # Scaling
        self.scalingtext = Gtk.Label()
        self.scalingtext.set_markup("<b>Scaling</b>")
        self.scaleFeat = Gtk.Entry(text=0.3)
        self.scaleFeat.props.xalign = 0.5
        self.scaleBlast = Gtk.Entry(text=0.3)
        self.scaleBlast.props.xalign = 0.5
        self.scaleBlastX = Gtk.Entry(text=0.3)
        self.scaleBlastX.props.xalign = 0.5
        self.scaleScale = Gtk.Entry(text=0.3)
        self.scaleScale.props.xalign = 0.5

        # Attach Scalling
        self.gridLegends.attach(self.scalingtext, 4, 0, 1, 1)
        self.gridLegends.attach(self.scaleFeat, 4, 3, 1, 1)
        self.gridLegends.attach(self.scaleBlast, 4, 4, 1, 1)
        self.gridLegends.attach(self.scaleBlastX, 4, 5, 1, 1)
        self.gridLegends.attach(self.scaleScale, 4, 2, 1, 1)


        # Font Size
        self.fontsizetext = Gtk.Label()
        self.fontsizetext.set_markup("<b>Font Size</b>")
        self.fsizeFeat = Gtk.Entry(text=25)
        self.fsizeFeat.props.xalign = 0.5
        self.fsizeBlast = Gtk.Entry(text=25)
        self.fsizeBlast.props.xalign = 0.5
        self.fsizeBlastX = Gtk.Entry(text=25)
        self.fsizeBlastX.props.xalign = 0.5
        self.fsizeScale = Gtk.Entry(text=25)
        self.fsizeScale.props.xalign = 0.5

        # Attach font size
        self.gridLegends.attach(self.fontsizetext, 5, 0, 1, 1)
        self.gridLegends.attach(self.fsizeFeat, 5, 3, 1, 1)
        self.gridLegends.attach(self.fsizeBlast, 5, 4, 1, 1)
        self.gridLegends.attach(self.fsizeBlastX, 5, 5, 1, 1)
        self.gridLegends.attach(self.fsizeScale, 5, 2, 1, 1)

        # Font color
        self.fontcolortext = Gtk.Label()
        self.fontcolortext.set_markup("<b>Font color</b>")
        self.fcolorFeat_setup = Gdk.RGBA()
        self.fcolorBlast_setup = Gdk.RGBA()
        self.fcolorBlastX_setup = Gdk.RGBA()
        self.fcolorScale_setup = Gdk.RGBA()

        self.fcolorFeat = Gtk.ColorButton.new_with_rgba(self.fcolorFeat_setup)
        self.fcolorBlast = Gtk.ColorButton().new_with_rgba(self.fcolorBlast_setup)
        self.fcolorBlastX = Gtk.ColorButton().new_with_rgba(self.fcolorBlastX_setup)
        self.fcolorScale = Gtk.ColorButton().new_with_rgba(self.fcolorScale_setup)

        # Attach font color
        self.gridLegends.attach(self.fontcolortext, 6, 0, 1, 1)
        self.gridLegends.attach(self.fcolorFeat, 6, 3, 1, 1)
        self.gridLegends.attach(self.fcolorBlast, 6, 4, 1, 1)
        self.gridLegends.attach(self.fcolorBlastX, 6, 5, 1, 1)
        self.gridLegends.attach(self.fcolorScale, 6, 2, 1, 1)

        
        # Max Cols Number
        self.NbColstext = Gtk.Label()
        self.NbColstext.set_markup("<b>Cols number</b>")
        
        self.nbcolsFeat = Gtk.Entry(text=1)
        self.nbcolsFeat.props.xalign = 0.5

        self.gridLegends.attach(self.NbColstext, 7, 0, 1, 1)
        self.gridLegends.attach(self.nbcolsFeat, 7, 3, 1, 1)

        # Packing grid into main Box
        self.boxLegends.pack_start(self.gridLegends, True, True, 10)

        self.set_legends_default()

# Decoration parameters page
#-------------------------------------------------------------------------

        self.listDeco = CGeno.ListStoreDnd(GObject.TYPE_BOOLEAN,   #  0 = tag to say that the row is not the general (used for many things)
                                          GObject.TYPE_STRING,     #  1 = add/remove icon name
                                          GObject.TYPE_PYOBJECT,   #  2 = add/remove action
                                          GObject.TYPE_PYOBJECT,   #  3 = copy action
                                          GObject.TYPE_STRING,     #  4 = decoration id
                                          GObject.TYPE_BOOLEAN,    #  5 = select/unselect the decoration
                                          GObject.TYPE_STRING,     #  6 = decoration type
                                          GObject.TYPE_STRING,     #  7 = decoration step size
                                          GObject.TYPE_STRING,     #  8 = decoration window size
                                          GObject.TYPE_STRING,     #  9 = decoration position
                                          GObject.TYPE_STRING,     # 10 = decoration height ratio
                                          GObject.TYPE_STRING,     # 11 = decoration line witdh
                                          GObject.TYPE_STRING,     # 12 = decoration line color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 13 = decoration line color
                                          GObject.TYPE_BOOLEAN,    # 14 = reverse decoration
                                          GObject.TYPE_BOOLEAN,    # 15 = print features label
                                          GObject.TYPE_STRING,     # 16 = decoration label color text field ('' or '-')
                                          GObject.TYPE_STRING,     # 17 = decoration label color
                                          GObject.TYPE_STRING,     # 18 = decoration label font size
                                          GObject.TYPE_STRING,     # 19 = decoration label position
                                          GObject.TYPE_PYOBJECT,   # 20 = sequence custom selection
                                          GObject.TYPE_BOOLEAN,    # 21 = use original position from the genbank file and not local position
                                          GObject.TYPE_INT         # 22 = show/hide the toogle for gbkposition
                                            )

        self.colsDecoID = [0]
        while len(self.colsDecoID) <= 20:
          self.colsDecoID.append(self.colsDecoID[-1]+1)


        self.tableDeco = Gtk.TreeView(model=self.listDeco)
        self.tableDeco.set_reorderable(True)
        self.tableDeco.set_property("hover-selection", True)
        
        renderer_butDAR = CGeno.CellRendererButton()
        self.colDAddRem = Gtk.TreeViewColumn('      ', renderer_butDAR, icon_name=1, callable=2)
        self.colDAddRem.set_resizable(True)
        self.tableDeco.append_column(self.colDAddRem)

        renderer_butDC = CGeno.CellRendererButton()
        renderer_butDC.set_property("icon_name", "edit-copy")
        self.colDCopy = Gtk.TreeViewColumn("    ", renderer_butDC, callable=3, visible=0)
        self.colDCopy.set_resizable(True)
        self.tableDeco.append_column(self.colDCopy)


        renderer_Dtext = Gtk.CellRendererText()
        renderer_Dtext.connect("edited", self.on_comboDeco_changed, 4)
        self.colDId = Gtk.TreeViewColumn("", renderer_Dtext, text=4, editable=0)
        self.colDId.set_resizable(True)
        self.tableDeco.append_column(self.colDId)
        
        renderer_togDS = Gtk.CellRendererToggle()
        renderer_togDS.connect("toggled", self.on_toggleDeco_changed, 5)
        self.colDsel = Gtk.TreeViewColumn("selected", renderer_togDS, active=5, visible=0)
        self.colDsel.set_resizable(True)
        self.tableDeco.append_column(self.colDsel)

        renderer_spinDcustomseq = CGeno.CellRendererButton()
        renderer_spinDcustomseq.set_property("icon_name", "accessories-text-editor")
        self.colDcustom = Gtk.TreeViewColumn("show on\nsequences", renderer_spinDcustomseq, callable=20, visible=0)
        self.colDcustom.set_resizable(True)
        self.tableDeco.append_column(self.colDcustom)
        # Initialize Dialog for custon decoration sequences

        renderer_space = Gtk.CellRendererText()
        self.colSpace6 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableDeco.append_column(self.colSpace6)


        renderer_comboDType = CGeno.CellRendererComboNoEntry(self.listDecoType)
#        renderer_comboDType.connect("edited", self.on_comboDeco_changed, 6)
        renderer_comboDType.connect("edited", self.on_decotype_changed, 6, 22)
        self.colDType = Gtk.TreeViewColumn("type", renderer_comboDType, text=6, visible=0)
        self.colDType.set_property("min-width", self.max_colsize(self.listDecoType))
        self.colDType.set_resizable(True)
        self.tableDeco.append_column(self.colDType)
        
        
        renderer_spinDDi = Gtk.CellRendererText()
        renderer_spinDDi.set_property("editable", True)
        renderer_spinDDi.connect("edited", self.on_comboDeco_changed, 7)
        self.colDDilter = Gtk.TreeViewColumn("step /\nminor ticks", renderer_spinDDi, text=7, visible=0)
        self.colDDilter.set_resizable(True)
        self.colDDilter.set_clickable(True)
        self.tableDeco.append_column(self.colDDilter)
        
        renderer_spinDDwin = Gtk.CellRendererText()
        renderer_spinDDwin.set_property("editable", True)
        renderer_spinDDwin.connect("edited", self.on_comboDeco_changed, 8)
        self.colDDwin = Gtk.TreeViewColumn("window /\nmajor ticks", renderer_spinDDwin, text=8, visible=0)
        self.colDDwin.set_resizable(True)
        self.colDDwin.set_clickable(True)
        self.tableDeco.append_column(self.colDDwin)
        
        renderer_comboDType = CGeno.CellRendererComboNoEntry(self.listDecoPosition)
        renderer_comboDType.connect("edited", self.on_comboDeco_changed, 9)
        self.colDType = Gtk.TreeViewColumn("position", renderer_comboDType, text=9)
        self.colDType.set_property("min-width", self.max_colsize(self.listDecoPosition))
        self.colDType.set_resizable(True)
        self.tableDeco.append_column(self.colDType)
        

        renderer_space = Gtk.CellRendererText()
        self.colSpace7 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableDeco.append_column(self.colSpace7)
        
        # Caution, the feature heigh is a ratio of the sequence height !!
        renderer_spinDH = Gtk.CellRendererText()
        renderer_spinDH.set_property("editable", True)
        renderer_spinDH.connect("edited", self.on_float_changed, self.listDeco, 10, -50, 50)
        self.colDHeight = Gtk.TreeViewColumn("height\nratio", renderer_spinDH, text=10)
        self.colDHeight.set_resizable(True)
        self.tableDeco.append_column(self.colDHeight)
        
        renderer_spinDLW = Gtk.CellRendererText()
        renderer_spinDLW.set_property("editable", True)
        renderer_spinDLW.connect("edited", self.on_int_changed, self.listDeco, 11, 0, 50)
        self.colDLWidth = Gtk.TreeViewColumn("line\nwidth", renderer_spinDLW, text=11)
        self.colDLWidth.set_resizable(True)
        self.tableDeco.append_column(self.colDLWidth)

        renderer_butColDL = Gtk.CellRendererText()
        renderer_butColDL.set_property("editable", True)
        renderer_butColDL.connect("editing-started", self.on_change_colCell, self.listDeco, 12, 13, "Feature line")
        self.colDLineCol = Gtk.TreeViewColumn('line\ncolor', renderer_butColDL, text=12)
        self.colDLineCol.set_property("fixed-width", 45)
        self.colDLineCol.add_attribute(renderer_butColDL, "cell-background", 13)
        self.colDLineCol.set_resizable(True)
        self.tableDeco.append_column(self.colDLineCol)
        
        renderer_spinDrev = Gtk.CellRendererToggle()
        renderer_spinDrev.connect("toggled", self.on_toggleDeco_changed, 14)
        self.colDrev = Gtk.TreeViewColumn("reverse", renderer_spinDrev, active=14, visible=0)
        self.colDrev.set_resizable(True)
        self.tableDeco.append_column(self.colDrev)
        
        self.colSpace9 = Gtk.TreeViewColumn("     ", renderer_space)
        self.tableDeco.append_column(self.colSpace9)


        renderer_spinDLPr = Gtk.CellRendererToggle()
        renderer_spinDLPr.connect("toggled", self.on_toggleDeco_changed, 15)
        self.colDLPrint = Gtk.TreeViewColumn("print\nlabel", renderer_spinDLPr, active=15, visible=0)
        self.colDLPrint.set_resizable(True)
        self.tableDeco.append_column(self.colDLPrint)
        
        renderer_butDLCol = Gtk.CellRendererText()
        renderer_butDLCol.set_property("editable", True)
        renderer_butDLCol.connect("editing-started", self.on_change_colCell, self.listDeco, 16, 17, "Decoration label")
        self.colDLColor = Gtk.TreeViewColumn('label\ncolor', renderer_butDLCol, text=16)
        self.colDLColor.set_property("fixed-width", 45)
        self.colDLColor.add_attribute(renderer_butDLCol, "cell-background", 17)
        self.colDLColor.set_resizable(True)
        self.tableDeco.append_column(self.colDLColor)
 
        renderer_spinDLS = Gtk.CellRendererText()
        renderer_spinDLS.set_property("editable", True)
        renderer_spinDLS.connect("edited", self.on_int_changed, self.listDeco, 18, 1, 300)
        self.colDLabSize = Gtk.TreeViewColumn("label\nsize", renderer_spinDLS, text=18)
        self.colDLabSize.set_resizable(True)
        self.tableDeco.append_column(self.colDLabSize)
        
        renderer_comboDType = CGeno.CellRendererComboNoEntry(self.listDecoPositionLabel)
        renderer_comboDType.connect("edited", self.on_comboDeco_changed, 19)
        self.colDType = Gtk.TreeViewColumn("label\nposition", renderer_comboDType, text=19)
        self.colDType.set_property("min-width", self.max_colsize(self.listDecoPositionLabel))
        self.colDType.set_resizable(True)
        self.tableDeco.append_column(self.colDType)

        renderer_spinDgbkpos = Gtk.CellRendererToggle()
        renderer_spinDgbkpos.connect("toggled", self.on_toggleDeco_changed, 21)
        self.colDgbkpos = Gtk.TreeViewColumn("use original\nGenBank position", renderer_spinDgbkpos, active=21)
        self.colDgbkpos.set_resizable(True)
        self.colDgbkpos.add_attribute(renderer_spinDgbkpos,'visible',22)
        self.tableDeco.append_column(self.colDgbkpos)

        self.colSpace10 = Gtk.TreeViewColumn(" ", renderer_space)
        self.tableDeco.append_column(self.colSpace10)

        self.addDeco(False, "General")
        self.decoInc = 1

# Packaging everything
#-------------------------------------------------------------------------
 
        # Creation of the notebook to encapsulate the sequence/feature/blast parameter pages
        self.Pwindows = Gtk.Notebook()

        self.scrollSeq = Gtk.ScrolledWindow()
#        self.scrollSeq.add(self.tableSeq)


        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # This part is to create an Overlay to display the 'Add sequence' button on top 
        # of the Sequence list panel, in order to avoid Segmentation fault or unexpected crash when the FileChooser closes.
        # This solves a bug linked to an internal GTK3 library disfunction in memory allocation making 
        # with no clear reasons.
        
        # For Features and Decoration panels, the trick is not necessary but is still implemented as a matter of graphical consistency.
        
        self.SeqOverlay = Gtk.Overlay()
        self.SeqOverlay.add(self.tableSeq)

        self.addSeqButton = Gtk.Button.new_from_icon_name("list-add",1)
        self.addSeqButton.connect("clicked", self.on_addNewSeq_clicked)
        self.addSeqButton.set_name("AddSeq")
        self.addSeqButton.set_valign(Gtk.Align.START)
        self.addSeqButton.set_halign(Gtk.Align.START)
        self.addSeqButton.set_relief(Gtk.ReliefStyle.NONE)
        self.addSeqButton.set_focus_on_click(False)
        self.addSeqButton.set_property("margin-top", 0)
        self.addSeqButton.set_property("margin-left", 0)

        self.SeqOverlay.add_overlay(self.addSeqButton)
        self.SeqOverlay.show_all()
        
#        self.scrollSeq.add(self.SeqOverlay)
        # using a Viewport container solves the problem of scrollbar position reset when clicking on a text or combo box        
        self.SeqViewport = Gtk.Viewport()        
        self.SeqViewport.add(self.SeqOverlay)        
        self.scrollSeq.add(self.SeqViewport)

        #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



        self.scrollFeat = Gtk.ScrolledWindow()
#        self.scrollFeat.add(self.tableFeat)

        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        self.FeatOverlay = Gtk.Overlay()
        self.FeatOverlay.add(self.tableFeat)

        self.addFeatButton = Gtk.Button.new_from_icon_name("list-add",1)
        self.addFeatButton.connect("clicked", self.on_addNewFeat_clicked)
        self.addFeatButton.set_name("AddSeq")
        self.addFeatButton.set_valign(Gtk.Align.START)
        self.addFeatButton.set_halign(Gtk.Align.START)
        self.addFeatButton.set_relief(Gtk.ReliefStyle.NONE)
        self.addFeatButton.set_focus_on_click(False)
        self.addFeatButton.set_property("margin-top", 0)
        self.addFeatButton.set_property("margin-left", 0)

        self.FeatOverlay.add_overlay(self.addFeatButton)
        self.FeatOverlay.show_all()
        
#        self.scrollFeat.add(self.FeatOverlay)
        # using a Viewport container solves the problem of scrollbar position reset when clicking on a text or combo box        
        self.FeatViewport = Gtk.Viewport()        
        self.FeatViewport.add(self.FeatOverlay)        
        self.scrollFeat.add(self.FeatViewport)        
        #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        
        self.scrollBlasts = Gtk.ScrolledWindow()
        self.scrollBlasts.add(self.boxBlasts)

        self.scrollLegends = Gtk.ScrolledWindow()
        self.scrollLegends.add(self.boxLegends)

        self.scrollDeco = Gtk.ScrolledWindow()
#        self.scrollDeco.add(self.tableDeco)

        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        self.DecoOverlay = Gtk.Overlay()
        self.DecoOverlay.add(self.tableDeco)

        self.addDecoButton = Gtk.Button.new_from_icon_name("list-add",1)
        self.addDecoButton.connect("clicked", self.on_addNewDeco_clicked)
        self.addDecoButton.set_name("AddSeq")
        self.addDecoButton.set_valign(Gtk.Align.START)
        self.addDecoButton.set_halign(Gtk.Align.START)
        self.addDecoButton.set_relief(Gtk.ReliefStyle.NONE)
        self.addDecoButton.set_focus_on_click(False)
        self.addDecoButton.set_property("margin-top", 0)
        self.addDecoButton.set_property("margin-left", 0)

        self.DecoOverlay.add_overlay(self.addDecoButton)
        self.DecoOverlay.show_all()
        
#        self.scrollDeco.add(self.DecoOverlay)
        # using a Viewport container solves the problem of scrollbar position reset when clicking on a text or combo box        
        self.DecoViewport = Gtk.Viewport()        
        self.DecoViewport.add(self.DecoOverlay)        
        self.scrollDeco.add(self.DecoViewport)
        #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        self.Pwindows.append_page(self.scrollSeq, Gtk.Label(label='Sequences'))
        self.Pwindows.append_page(self.scrollFeat, Gtk.Label(label='Features'))
        self.Pwindows.append_page(self.scrollBlasts, Gtk.Label(label='Homologies'))
        self.Pwindows.append_page(self.scrollLegends, Gtk.Label(label='Legends'))
        self.Pwindows.append_page(self.scrollDeco, Gtk.Label(label='Decoration'))

        self.boxMain.pack_start(self.boxPictParams, False, False, 0)
        self.boxMain.pack_start(self.Pwindows, True, True, 0)
        
        # Define Grid for display status
        self.status = Gtk.Grid()
        
        # Grid to display blast information on the left
        self.statusLabelBlastn = Gtk.Label(label="")
        statusLabelBlastn_context = self.statusLabelBlastn.get_style_context()
        statusLabelBlastn_context.add_class("status_blastn")
        self.statusLabelBlastx = Gtk.Label(label="")
        statusLabelBlastx_context = self.statusLabelBlastx.get_style_context()
        statusLabelBlastx_context.add_class("status_blastx")
        
        self.status.attach(self.statusLabelBlastn, 1, 1, 1, 1)
        self.status.attach(self.statusLabelBlastx, 2, 1, 1, 1)

        self.statusLabel = Gtk.Label(label="test")
        self.status.attach(self.statusLabel, 3, 1, 1, 1)
        self.statusLabel.set_label("Status OK - Waiting for action")
        statusCurrent_context = self.statusLabel.get_style_context()
        statusCurrent_context.add_class("status")

        self.boxMain.pack_start(self.status, False, True, 0)

        self.createDecoSeq()



# Create the menu
#-------------------------------------------------------------------------

        action = Gio.SimpleAction.new("new", None)
        action.connect("activate", self.on_New, "new")
        self.add_action(action)

        action = Gio.SimpleAction.new("open", None)
        action.connect("activate", self.on_Load, "open")
        self.add_action(action)

        action = Gio.SimpleAction.new("saveA", None)
        action.connect("activate", self.on_Save, "saveA")
        self.add_action(action)

        action = Gio.SimpleAction.new("saveAs", None)
        action.connect("activate", self.on_SaveAs, "saveA")
        self.add_action(action)

        action = Gio.SimpleAction.new("loadC", None)
        action.connect("activate", self.on_Load, "loadC")
        self.add_action(action)

        action = Gio.SimpleAction.new("saveC", None)
        action.connect("activate", self.on_Save, "saveC")
        self.add_action(action)

        if sys.platform.startswith('linux'):
          action = Gio.SimpleAction.new("saveDC", None)
          action.connect("activate", self.Save_default_configuration)
          self.add_action(action)

        
        action = Gio.SimpleAction.new("credits", None)
        action.connect("activate", self.on_Credits)
        self.add_action(action)
        
        action = Gio.SimpleAction.new("readme", None)
        action.connect("activate", self.on_ReadMe)
        self.add_action(action)


# checking if user default config exists on home directory, then load it : // Linux only
#-------------------------------------------------------------------------
        self.Load_default_configuration()
        
        
        
        
#==========================================================================================================================
#
# Interface functions +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
#==========================================================================================================================
    ##############################################################################################
    # Change combo list value in Sequence, Feature, and Decoration panels
    ##################################################################
    def on_comboSeq_changed(self, widget, index, text, ncol):
       self.listSeq[index][ncol] = text
 
    def on_comboFeat_changed(self, widget, index, text, ncol):
        self.listFeat[index][ncol] = text

    def on_comboDeco_changed(self, widget, index, text, ncol):

        # If the name/id of the Decoration is changed, the DecorationSequences dictionary needs to be updated accordingly
        # Two Decoration cannot have the same name
        if ncol == 4:
            oldID = self.listDeco[index][ncol]
            # Check if new the name doesn't already exists
            decoNames = [i[4]  for i in self.listDeco]
            if text in decoNames:
                self.update_status('status_error', f"ERROR - Decoration '{text}' already exists.")
                print(f"Decoration '{text}' already exists.")
                return False
            else:
                self.DecorationSequences[text] = self.DecorationSequences[oldID]
                del self.DecorationSequences[oldID]
        
        self.listDeco[index][ncol] = text

    def on_comboSeqPos_changed(self, widget, index, text):
       if text != "custom":
        self.listSeq[index][13] = text

       else:
        self.Posdialog = CGeno.DialogXpos(self, self.listSeq[index][3], self.listSeq[index][11])

        response = self.Posdialog.run()

        if response == Gtk.ResponseType.OK:
         xval = self.Posdialog.entryX.get_text()

         try: int(xval)
         except: 
             self.update_status('status_error', f"ERROR - must be integer value")
             print("must be integer value")
         else:
           self.listSeq[index][11] = xval
           self.listSeq[index][13] = "x = "+xval

         self.Posdialog.destroy()


    ##############################################################################################
    # Change Sequence combo lists in Blast selection One vs All and Custom
    ##########################################################
    def on_Seqnbchanged(self, *wargs):

       previous_selBN = self.comboBNOne.get_active_text()
       previous_selBX = self.comboBXOne.get_active_text()
       self.comboBNOne.remove_all()
       self.comboBXOne.remove_all()

       if self.listSeq.iter_n_children(None) < 2:
         self.comboBNOne.append('1', 'No Sequence')
         self.comboBNOne.set_active_id('1')
         self.comboBXOne.append('1', 'No Sequence')
         self.comboBXOne.set_active_id('1')
         return

       for i in range(1, self.listSeq.iter_n_children(None)):
         self.comboBNOne.append(str(i), self.listSeq[i][3])
         self.comboBXOne.append(str(i), self.listSeq[i][3])

         if self.listSeq[i][3] == previous_selBN:
           self.comboBNOne.set_active_id(str(i))

         if self.listSeq[i][3] == previous_selBX:
           self.comboBXOne.set_active_id(str(i))

       if self.BNDialog != None:
         self.BNDchecked = self.BNDialog.get_checked()
         self.BNDialog.destroy()
         self.BNDialog = None

       if self.BXDialog != None:
         self.BXDchecked = self.BXDialog.get_checked()
         self.BXDialog.destroy()
         self.BXDialog = None
    ##############################################################################################
    # Activate / inactivate item in the blast selection part
    ##########################################################
    def on_radioB_changed(self, widget, target):
        if widget.get_active():
          target.set_state_flags(Gtk.StateType.NORMAL, True)
        else:
          target.set_state_flags(Gtk.StateFlags.INSENSITIVE, False)


    ##############################################################################################
    # Show/hide item in the decorum list
    ##########################################################
    def on_decotype_changed(self, widget, index, text, ncol, ncolinact):
        self.listDeco[index][ncol] = text

        if text == 'Scale':
          self.listDeco[index][ncolinact]=1
       
        else:
          self.listDeco[index][ncolinact]=0


    ##############################################################################################
    # Change toggle value in Sequence and Feature panels
    ##########################################################
    def on_toggleSeq_changed(self, widget, index, ncol):
        value = not(self.listSeq.get_value(self.listSeq.get_iter(index), ncol))
        self.listSeq.set_value(self.listSeq.get_iter(index), ncol, value)

    def on_toggleFeat_changed(self, widget, index, ncol):
        value = not(self.listFeat.get_value(self.listFeat.get_iter(index), ncol))
        self.listFeat.set_value(self.listFeat.get_iter(index), ncol, value)
    
    def on_toggleDeco_changed(self, widget, index, ncol):
        value = not(self.listDeco.get_value(self.listDeco.get_iter(index), ncol))
        self.listDeco.set_value(self.listDeco.get_iter(index), ncol, value)


    ##############################################################################################
    # Show/Hide color buttons for reverse blast matches
    ##########################################################
    def on_toggledColor(self, widget, colbut1, colbut2):
        if widget.get_active():
          colbut1.show()
          colbut2.show()
        else:
          colbut1.hide()
          colbut2.hide()


    ##############################################################################################
    # Change color of touched cells
    # A general function which is called for all cells
    #########################################################################
    def on_change_colCell(self, widget, entry, index, listX, coltext_ncol, color_ncol, label):

        # select the current color: select the general color if None 
        colorsel=Gdk.RGBA()
        colorval=listX.get_value(listX.get_iter(index),color_ncol)
        if colorval == None: colorsel.parse(listX.get_value(listX.get_iter(0),color_ncol))
        else:  colorsel.parse(colorval)
    
        # Open a filechooser dialog box
        self.Coldialog = Gtk.ColorChooserDialog("Select "+label+" Color", self)
        self.Coldialog.set_use_alpha(False)
        self.Coldialog.set_rgba(colorsel)
        if index != "0": self.Coldialog.add_button("As general",32) # add a button to propose default color
        response = self.Coldialog.run()

        # get selected color and store it in the ListStore
        if response == Gtk.ResponseType.OK:
          colorsel=self.Coldialog.get_rgba() 
          listX.set_value(listX.get_iter(index), color_ncol, colorsel.to_string())
          listX.set_value(listX.get_iter(index), coltext_ncol, "")

        if response == 32:
          listX.set_value(listX.get_iter(index), coltext_ncol, "-")
          listX.set_value(listX.get_iter(index), color_ncol, None)

        self.Coldialog.destroy()

        # tell the widget to not ask for editing the text (since it is changed automatically when the color is set)
        entry.editing_done()


    ##############################################################################################
    # Manage str values as it would be int values
    # and change the corresponding list cell
    ##############################################################################################
    def on_int_changed(self, widget, index, v, rowObj, colnb, min, max):
        self.on_val_changed(widget, index, v, rowObj, colnb, min, max, 1)

    def on_float_changed(self, widget, index, v, rowObj, colnb, min, max):
        self.on_val_changed(widget, index, v, rowObj, colnb, min, max, 0)


    def on_val_changed(self, widget, index, v, rowObj, colnb, min, max, isint):
      cval='-'

      # Special case of the '' value for specific sequences
      if int(index) > 0 and (v =='' or v==' ' or v=='-'):
        rowObj.set_value(rowObj.get_iter(index), colnb, cval)
        return 1

      # Otherwise, manage values as int between min and max
      try:
         vint=float(v) 
         if vint < min: cval=str(min)
         elif vint > max: cval=str(max)
         else: 
           if isint:  cval=str(int(vint))
           else:      cval=str(float(vint))

      except:
         self.update_status('status_error', f"ERROR - {v} is not a correct value")
         print("'"+v+"' is not a correct value")

      else:
         rowObj.set_value(rowObj.get_iter(index), colnb, cval)


    ##############################################################################################
    # Manage str values fo position in sequence as it would be int values
    # and change the corresponding list cell
    ##############################################################################################
    def change_str2intpos(self, ismin, rowObj, index, colnb, v, min, max):
      maxval='max'
      if ismin: maxval=str(max)

      # Skip the general value which is non sense
      if int(index) == 0:
        rowObj.set_value(rowObj.get_iter(index), colnb, '')
        return 1

      # Special case of the values given as 'min' and 'max'
      if v =='max' or v=='Max':
        rowObj.set_value(rowObj.get_iter(index), colnb, maxval)
        return 1
      if v =='min' or v=='Min':
        rowObj.set_value(rowObj.get_iter(index), colnb, str(min))
        return 1

      if str(max) == maxval: max = rowObj.get_value(rowObj.get_iter(index), 5).length()


      # Otherwise, manage values as int between min and max
      try:
         vint=float(v) 
         if vint < min: cval=str(min)
         elif vint > max: cval=maxval
         else: cval=str(int(vint))

      except:
         self.update_status('status_error', f"ERROR - {v} is not a correct value")
         print("'"+v+"' is not a correct value")

      else:
         rowObj.set_value(rowObj.get_iter(index), colnb, cval)


    ##############################################################################################
    # Change minimum and maximum positions in the edited sequence
    ##########################################################
    def change_Smin(self, widget, index, value):
        if value == '': return
        maxi = self.listSeq.get_value(self.listSeq.get_iter(index), 15)
        if maxi == "max": maxi = self.listSeq.get_value(self.listSeq.get_iter(index), 5).length()
        self.change_str2intpos(True, self.listSeq, index, 14, value, 1, int(maxi)-1)
    
    def change_Smax(self, widget, index, value):
        if value == '': return
        maxi = self.listSeq.get_value(self.listSeq.get_iter(index), 5).length()
        self.change_str2intpos(False, self.listSeq, index, 15, value, int(self.listSeq.get_value(self.listSeq.get_iter(index), 14))+1, maxi)



    ##############################################################################################
    # Show/hide BlastN/TBlastX frames
    ##########################################################
    def on_minmaxBlastframe(self, button, Bbox):

       imgbut = button.get_image()

       if Bbox.get_visible():
         Bbox.hide()
         imgbut.set_from_icon_name("window-maximize-symbolic", 1)

       else:
         Bbox.show()
         imgbut.set_from_icon_name("window-minimize-symbolic", 1)
 

    ##############################################################################################
    # Create the Custom Blast window
    ##########################################################
    def customB_window(self, item, blasttype):
        seqlist = []

        if blasttype == "BLASTN":
          if self.BNDialog == None:
 
            for i in range(1, self.listSeq.iter_n_children(None)):
             seqlist.append(self.listSeq[i][3])

            self.BNDialog = CGeno.DialogCustomBlast(self, blasttype, seqlist, self.BNDchecked)
            if item != "noshow": self.BNDialog.show_all() # Dont show the dialog when funtion not run through the button

          else:
            self.BNDialog.show()



        elif blasttype == "TBLASTX":
          if self.BXDialog == None:

            for i in range(1, self.listSeq.iter_n_children(None)):
             seqlist.append(self.listSeq[i][3])

            self.BXDialog = CGeno.DialogCustomBlast(self, blasttype, seqlist, self.BXDchecked)
            if item != "noshow": self.BXDialog.show_all()
          else:
            self.BXDialog.show()

#==========================================================================================================================
#
# Non interface functions +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
#==========================================================================================================================

    ##############################################################################################
    # Select the output file name (SVG format or PNG format)
    ####################################################################
    def on_picture_name(self, *arg):

        # Open a filechooser dialog box
        self.PictNamedialog = Gtk.FileChooserDialog(title="Please choose output image file", parent=self, action=Gtk.FileChooserAction.SAVE)
        self.PictNamedialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)

        filefilter1= Gtk.FileFilter()
        filefilter1.add_pattern("*.svg")
        filefilter1.set_name("SVG image")
        filefilter2= Gtk.FileFilter()
        filefilter2.add_pattern("*.png")
        filefilter2.set_name("PNG image")
        filefilter3= Gtk.FileFilter()
        filefilter3.add_pattern("*")
        filefilter3.set_name("All files")
        self.PictNamedialog.add_filter(filefilter1)
        self.PictNamedialog.add_filter(filefilter2)
        self.PictNamedialog.add_filter(filefilter3)
        self.PictNamedialog.set_current_name(os.path.basename(self.entryPName.get_text()))
        self.PictNamedialog.set_current_folder(self.figDirectoryPath)

        # Update the path according to the extension of the current figure file name
        if self.entryPName.get_text()[-4:].lower() == '.svg':
           self.PictNamedialog.set_filter(filefilter1) 
        if self.entryPName.get_text()[-4:].lower() == '.png':
           self.PictNamedialog.set_filter(filefilter2) 


        response = self.PictNamedialog.run()
        
        # store all the filenames selected and update directory path if not defined yet
        if response == Gtk.ResponseType.OK:
          self.update_status('status', f"SUCCESS - Output file update")
          self.outputfile = self.formatfile(self.PictNamedialog.get_filename())
          self.outputformat = self.PictNamedialog.get_filter().get_name()

          # Update extension according to the selected format if necessary 
          if self.outputfile[-4:].lower() == '.svg' or self.outputfile[-4:].lower() == '.png':
            self.outputfile = self.outputfile[0:-4]
          if 'SVG' in self.outputformat:
            self.outputfile = self.outputfile + '.svg'
          elif 'PNG' in self.outputformat:
            self.outputfile = self.outputfile + '.png'

          self.entryPName.set_text(self.outputfile)
          self.figDirectoryPath = os.path.dirname(self.PictNamedialog.get_filename())
          if self.saveDirectoryPath == '':				# update Save directory if not defined yet
               self.saveDirectoryPath = self.figDirectoryPath
          if self.seqDirectoryPath == '':				# update seq directory if not defined yet
               self.seqDirectoryPath = self.figDirectoryPath

        else:
          self.update_status('status_error', f"ERROR - You need to select an output file")

        self.PictNamedialog.destroy()


    ##############################################################################################
    # Format the output file for display convenience
    ####################################################################
    def formatfile(self, filename):
      if filename == '': return filename
      if sys.platform.startswith('win'):
       return filename.replace(os.getcwd().replace("\\", "/") + "/", "") # WINDOWS version
      elif sys.platform.startswith('linux'):
       return os.path.relpath(filename, os.getcwd())
      else:
       return os.path.relpath(filename, os.getcwd())

   
    ##############################################################################################
    # Add a new sequences to the sequence table, with all its parameters
    ####################################################################
    def addSeq(self, notgal, seq_name, record, afile):
        iter = self.listSeq.append()

        if notgal:
          addrem_icon = "window-close"
          addrem_func = self.on_removeSeq_clicked
          info_func = self.give_infos
          bioobj = CGeno.BioSeqobj(record, afile)
          height = lwidth = space = seqpos = '-'
          line_color = seqlabcol = featlabcol = None
          seqlabprint = featlabprint = False
          seqlabtype = seqlabpos = seqlabsize = seqlaboff = "-"
          featlabtype = featlabpos = featlabsize = featlabrot = featlabcoltext = coltext = seqlabcoltext = '-'

        else:
#          addrem_icon = "list-add"
#          addrem_func = self.on_addNewSeq_clicked
          addrem_icon = ""
          addrem_func = self.do_nothing
          info_func = self.do_nothing
          bioobj = None
          height = '60'
          lwidth = '5'
          space = '200'
          coltext = seqlabcoltext = featlabcoltext = None
          seqpos = 'left'
          line_color = seqlabcol = featlabcol = "rgb(0,0,0)"
          seqlabprint = True
          seqlabtype = "locus"
          seqlabpos = "top/left"
          seqlabsize = '30'
          seqlaboff = '40'
          featlabprint = True
          featlabtype = "product"
          featlabpos = "Top"
          featlabsize = '20'
          featlabrot = '90'

        self.listSeq.set(iter,
                         self.colsSeqID,
                         [notgal, addrem_icon, addrem_func, seq_name, info_func,
                          bioobj, True, height, lwidth, space, coltext, "0",
                          line_color, seqpos, "1", "max", False,
                          seqlabprint, seqlabtype, seqlabpos, seqlabcoltext, seqlabcol, seqlabsize,
                          featlabprint, featlabtype, featlabpos, featlabcoltext, featlabcol, featlabsize, featlabrot, seqlaboff, self.seqID]
                        )
        self.seqID+=1
        return iter

    ##############################################################################################
    # Add one or more new sequences from a file
    ##########################################################
    def on_addNewSeq_clicked(self, index):

        # Open a filechooser dialog box
        self.NewSeqdialog = Gtk.FileChooserDialog(title="Please choose a sequence file", parent=self, action=Gtk.FileChooserAction.OPEN)
 #       self.NewSeqdialog = CGeno.SeqFileChooserDialog(title="Please choose a sequence file", parent=self, action=Gtk.FileChooserAction.OPEN)
        self.NewSeqdialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)

        self.NewSeqdialog.set_select_multiple(True)

        filefilter1= Gtk.FileFilter()
        filefilter1.add_pattern("*.fna")
        filefilter1.add_pattern("*.fa")
        filefilter1.add_pattern("*.fasta")
        filefilter1.add_pattern("*.gb")
        filefilter1.add_pattern("*.gbk")
        filefilter1.add_pattern("*.gbff")
        filefilter1.add_pattern("*.flat")
        filefilter1.set_name("Sequence files (*.gb, *.gbff, *.gbk, *.fna, *.fa, *.fasta, *.flat)")
        filefilter2= Gtk.FileFilter()
        filefilter2.add_pattern("*")
        filefilter2.set_name("All files")
        self.NewSeqdialog.add_filter(filefilter1)
        self.NewSeqdialog.add_filter(filefilter2)
        self.NewSeqdialog.set_current_folder(self.seqDirectoryPath)

        response = self.NewSeqdialog.run()
        filelist=[]

        # store all the filenames selected
        if response == Gtk.ResponseType.OK:
            filelist=self.NewSeqdialog.get_filenames()
            self.NewSeqdialog.destroy()
        else:
            self.NewSeqdialog.destroy()
            return None
        
        # read each file, and get all sequences from each file
        for afile in filelist:
           for aseq in CGeno.parseSeqFile(afile, "L"):
            # add the sequence to the sequence list
            s=self.addSeq(True, aseq[0], aseq[1], self.formatfile(afile))
            self.updateDecoSeq('add', self.seqID-1)
            self.update_status('status', f"SUCCESS - Sequence added")

            # add the features found in this sequence to the features list
            # if they don't already exist
            for item in self.listSeq.get_value(s, 5).getFeattypes():
                if item not in self.featTypeList:
                    self.featTypeList.append(item)
                    self.listFeatType.append([item])
            self.on_Seqnbchanged()

        self.seqDirectoryPath = os.path.dirname(afile)
        if self.saveDirectoryPath == '':				# Update save directory if not defined yet
            self.saveDirectoryPath = self.seqDirectoryPath
        if self.figDirectoryPath == '':				# Update fig directory if not defined yet
            self.figDirectoryPath = self.seqDirectoryPath

    ##############################################################################################
    # Remove the selected sequence
    ##########################################################
    def on_removeSeq_clicked(self, index):
        seqID=self.listSeq.get_value(self.listSeq.get_iter(index), 31)
        self.listSeq.remove(self.listSeq.get_iter(index))
        try:
            self.updateDecoSeq('del',seqID)
        except: 
            pass
        else:
            self.update_status('status', f"SUCCESS- Sequence removed")


    ##############################################################################################
    # Remove the selected BlastN file
    ##########################################################
    def on_removeBlastN_clicked(self, index):
        self.listBlastN.remove(self.listBlastN.get_iter(index))
        self.update_status('status', f"SUCCESS- BlastN removed")

    ##############################################################################################
    # Remove the selected BlastX file
    ##########################################################
    def on_removeBlastX_clicked(self, index):
        self.listBlastX.remove(self.listBlastX.get_iter(index))
        self.update_status('status', f"SUCCESS- BlastX removed")


    ##############################################################################################
    # Open a Blast filechooser dialog box
    ##########################################################
    def create_blast_dialog(self, blast_type, actiontype):

        # Open a filechooser dialog box
        if actiontype == 'OPEN':
         self.BlastFiledialog = Gtk.FileChooserDialog(parent=self, title="Please choose a "+blast_type+" file", action=Gtk.FileChooserAction.OPEN)
         self.BlastFiledialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
         self.BlastFiledialog.set_select_multiple(True)
        else :
         self.BlastFiledialog = Gtk.FileChooserDialog(parent=self, title="Please enter an output file name", action=Gtk.FileChooserAction.SAVE)
         self.BlastFiledialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK)

        if blast_type == "blastn": 
          pattern = "*.blastout"
        else: pattern = "*.blastXout"

        filefilter1= Gtk.FileFilter()
        filefilter1.add_pattern(pattern)
        filefilter1.set_name("Blast file ("+pattern+")")
        filefilter2= Gtk.FileFilter()
        filefilter2.add_pattern("*")
        filefilter2.set_name("All files")
        self.BlastFiledialog.add_filter(filefilter1)
        self.BlastFiledialog.add_filter(filefilter2)
        self.BlastFiledialog.set_current_folder(self.saveDirectoryPath)

        return self.BlastFiledialog


    ##############################################################################################
    # Lock "Blast" and "Create" buttons and display blast processing information in the GUI 
    ##########################################################
    def runBlast_status(self, tmpfasta, btype, outblastname):
        if btype == 'blastn':
            GLib.idle_add(self.butRUN.set_sensitive,False) # the use of idle_add allow to be thread-safe: GUI interaction are carried out in the main GUI thread. 
            GLib.idle_add(self.buttonBNrun.set_sensitive,False)
            while self.threadblast.is_alive():
                time.sleep(1)
                print("\trunning blastn...")
                GLib.idle_add(self.update_status_blast,'blastn', 'Blastn is running...')
        else:
            GLib.idle_add(self.butRUN.set_sensitive,False)
            GLib.idle_add(self.buttonBXrun.set_sensitive,False)
            while self.threadblastX.is_alive():
                time.sleep(1)
                print("\trunning tblastx...")
                GLib.idle_add(self.update_status_blast,'tblastx', 'tBlastx is running...')

        os.remove(tmpfasta)
        # Process output blast file
        GLib.idle_add(self.blast_process_results,btype, outblastname)

    ##############################################################################################
    # Unlock "Blast" and "Create" buttons and process blast file integration in the GUI
    ##############################################################################################
    def blast_process_results(self, blast_type, outblastname):
        # format the outblast name as a relative path
        outblastname = self.formatfile(outblastname)

        # reactivate the RUN button and clean the Blast status area
        self.butRUN.set_sensitive(True)
        self.update_status_blast(blast_type, '')

        # Stop here if the blast didn't perform well (for any reason)
        if blast_type == "blastn" and not self.blastNOK:
         return
        if blast_type == "tblastx" and not self.blastXOK:
         return

        # Update status
        print(f"{blast_type} done")
        self.update_status('status', f"SUCCESS - {blast_type} done")


        if blast_type == "blastn":
            # automatically display blast homologies if not displayed
            if self.radioBNNone.get_active(): self.radioBNAdj.set_active(True)
            
            # insert the new filename into the corresponding list with the remove file function
            # only if it didn't already exist 
            for names in self.listBlastN:
               if outblastname == names[1]: return
            iter = self.listBlastN.append()
            self.listBlastN.set(iter, [0, 1], [self.on_removeBlastN_clicked, outblastname])

        # do the same thing if tblastx was run
        else:
            if self.radioBXNone.get_active(): self.radioBXAdj.set_active(True)
            for names in self.listBlastX:
               if outblastname == names[1]: return
            iter = self.listBlastX.append()
            self.listBlastX.set(iter, [0, 1], [self.on_removeBlastX_clicked, outblastname])

    ##############################################################################################
    # Various blast running functions
    ##########################################################
    # BlastN Linux & MAC
    def runBlastN(self, query, subject, out):
        print("Starting blastn")
        try:
            # if GenoFig is run as a bundled MacOS app, specifically target the blastn executable within the app
            print(self.blastpath)
            if '/var/folders/' in self.blastpath: 
             command=self.blastpath+'/bin/blastn'
            else:
             command='blastn'
            blastjob = NcbiblastnCommandline(cmd=command, task='blastn', query=query, out=out, subject=subject, outfmt=6)
            blastjob()
            self.blastNOK=1
            
        except:
            print("ERROR! you may have to install ncbi-blast+ first")
            self.update_status('status_error', f"ERROR - you may have to install ncbi-blast+ first. ")
            return

    # BlastN Windows
    def runBlastN_windows(self, query, subject, out):
        print("Starting blastn")
        try:
            print(self.blastpath)
            subprocess.call(["blastn.exe", "-query", query, "-subject", subject, "-out", out, "-outfmt", "6"]) 
            # for compiled version only
#            subprocess.call([f"{self.blastpath}\\blastn.exe", "-query", query, "-subject", subject, "-out", out, "-outfmt", "6"])
            self.blastNOK=1
        except:
            print("ERROR! you may have to install ncbi-blast+ first")
            self.update_status('status_error', f"ERROR - you may have to install ncbi-blast+ first")
            return
    
    # tBlastX Linux & MAC
    def runBlastX(self, query, subject, out):
        print("Starting tblastx")
        try:
            # if GenoFig is run as a bundled MacOS app, specifically target the tblastx executable within the app
            if '/var/folders/' in self.blastpath: 
             command=self.blastpath+'/bin/tblastx'
            else:
             command='tblastx'
            blastjob = NcbitblastxCommandline(cmd=command, query=query, out=out, subject=subject, outfmt=6, query_gencode=11, db_gencode=11, evalue=0.001)
            blastjob()
            self.blastXOK=1
        except:
            print("ERROR! you may have to install ncbi-blast+ first")
            self.update_status('status_error', f"ERROR - you may have to install ncbi-blast+ first.")
            return

    # tBlastX Windows
    def runBlastX_windows(self, query, subject, out):
        print("Starting tblastx")
        try:
            subprocess.call(["tblastx.exe", "-query", query, "-subject", subject, "-out", out, "-outfmt", "6", "-query_gencode", "11", "-db_gencode", "11", "-evalue", "0.001"])
            # for compiled version only
#            subprocess.call([f"{self.blastpath}\\tblastx.exe", "-query", query, "-subject", subject, "-out", out, "-outfmt", "6", "-query_gencode", "11", "-db_gencode", "11", "-evalue", "0.001"])
            self.blastXOK=1
        except:
            print("ERROR! you may have to install ncbi-blast+ first")
            self.update_status('status_error', f"ERROR - you may have to install ncbi-blast+ first")
            return

    ##############################################################################################
    # Choose blast file and run blast in alternative thread
    ##########################################################
    def on_BlastRun_clicked(self, widget, blast_type):

        # Open a filechooser dialog box
        self.BlastFiledialog = self.create_blast_dialog(blast_type, "SAVE")

        response = self.BlastFiledialog.run()

        # store the selected filename
        if response == Gtk.ResponseType.OK:
          outblastname=self.BlastFiledialog.get_filename()
          self.BlastFiledialog.destroy()

          # update save and figure paths
          outblastdir = os.path.dirname(outblastname)
          if self.saveDirectoryPath == '':				# Update save directory if not defined yet
           self.saveDirectoryPath = outblastdir
          if self.figDirectoryPath == '':				# Update fig directory if not defined yet
           self.figDirectoryPath = outblastdir

        else:
          self.BlastFiledialog.destroy()
          return 0

        # create a temporary fasta file with the stored sequences
        try:
          tmpfasta=self.listSeq.saveinfasta()
        except Exception as lackseq:
          print(f"ERROR ! {lackseq.args[0]}") 
          self.update_status('status_error', f"ERROR - {lackseq.args[0]}")
          return 0

        if(blast_type == 'blastn'):
            # add blast file extension if not provided
            if not(outblastname.endswith(".blastout")): outblastname=outblastname+".blastout" 
            # prepare the blast command, depending on the platform
            if sys.platform.startswith('win'):
                self.threadblast = threading.Thread(target=self.runBlastN_windows, args=(tmpfasta, tmpfasta, outblastname))
            else:
                self.threadblast = threading.Thread(target=self.runBlastN, args=(tmpfasta, tmpfasta, outblastname))

            # run the blast command
            self.blastNOK=0 # status variable to check whether the blast correctly finished
            self.threadblast.start()

            # manage GUI status during the blast run
            self.threadblaststatus = threading.Thread(target=self.runBlast_status, args=(tmpfasta, 'blastn', outblastname))
            self.threadblaststatus.start()


        elif(blast_type == 'tblastx'):
            if not(outblastname.endswith(".blastXout")): outblastname=outblastname+".blastXout"
            if sys.platform.startswith('win'):
                self.threadblastX = threading.Thread(target=self.runBlastX_windows, args=(tmpfasta, tmpfasta, outblastname))
            else:
                self.threadblastX = threading.Thread(target=self.runBlastX, args=(tmpfasta, tmpfasta, outblastname))
            
            self.blastXOK=0
            self.threadblastX.start()

            self.threadblaststatusX = threading.Thread(target=self.runBlast_status, args=(tmpfasta, 'tblastx', outblastname))
            self.threadblaststatusX.start()



    ##############################################################################################
    # Add a blast results from a file
    ##########################################################
    def on_BlastLoad_clicked(self, widget, blast_type):
        
        # Open a filechooser dialog box
        self.BlastFiledialog = self.create_blast_dialog(blast_type, "OPEN")
        response = self.BlastFiledialog.run()

        # store the selected filename
        if response == Gtk.ResponseType.OK:
          filelist=self.BlastFiledialog.get_filenames()
          self.BlastFiledialog.destroy()
        else:
          self.BlastFiledialog.destroy()
          return 0

        # insert the filename into the corresponding list
        # and with the remove file function
        for afile in filelist:

          # format the outblast name as a relative path
          outblastname = self.formatfile(afile)

          if blast_type == "blastn":
           for names in self.listBlastN: 
              if outblastname == names[1]: return ## to avoid putting twice the same file in the list
           iter = self.listBlastN.append()
           self.listBlastN.set(iter, [0, 1], [self.on_removeBlastN_clicked, outblastname])
           # automatically display blast homologies if not displayed
           if self.radioBNNone.get_active(): self.radioBNAdj.set_active(True)

          else:
           for names in self.listBlastX: 
              if outblastname == names[1]: return ## to avoid putting twice the same file in the list
           iter = self.listBlastX.append()
           self.listBlastX.set(iter, [0, 1], [self.on_removeBlastX_clicked, outblastname])
           # automatically display blast homologies if not displayed
           if self.radioBXNone.get_active(): self.radioBXAdj.set_active(True)

        self.update_status('status', f"SUCCESS - Blast loaded")


    ##############################################################################################
    # Give sequence information
    ##########################################################
    def give_infos(self, index):

        self.dialog = Gtk.MessageDialog(transient_for=self,
                                 destroy_with_parent=True,
                                 message_type=Gtk.MessageType.INFO,
                                 buttons=Gtk.ButtonsType.CLOSE,
                                 text=self.listSeq.get_value(self.listSeq.get_iter(index), 3)+" sequence information")
        self.dialog = Gtk.MessageDialog(transient_for=self,
                                 destroy_with_parent=True,
                                 message_type=Gtk.MessageType.INFO,
                                 buttons=Gtk.ButtonsType.CLOSE,
                                 text=self.listSeq.get_value(self.listSeq.get_iter(index), 3)+" sequence information")
        self.dialog.format_secondary_text(str(self.listSeq.get_value(self.listSeq.get_iter(index), 5).getinfo()))
        self.dialog.add_button("Reload", 44)
        self.dialog.connect("response", self.infodialog_response, index)
        self.dialog.show()


    # manage dialog response
    def infodialog_response(self, widget, response_id, index):

        # reload the sequence
        if response_id == 44:
         self.dialog_reload_warning()
         self.reload_annotations_from_file(index)
         widget.destroy()

        # do nothing
        if response_id == Gtk.ResponseType.CLOSE:
         widget.destroy()


    ##############################################################################################
    # Warning dialog
    ##########################################################
    def dialog_reload_warning(self):
        self.dialog = Gtk.MessageDialog(self,
                                 Gtk.DialogFlags.MODAL,
                                 Gtk.MessageType.INFO,
                                 Gtk.ButtonsType.CLOSE,
                                 "WARNING !")
        self.dialog.format_secondary_text("Please re-run blast files if the nucleotide sequence was modified in the updated sequence")
        self.dialog.connect("response", self.infodialog_response, None)
        self.dialog.show()


    ##############################################################################################
    # Reload given sequence from information dialog.
    ##########################################################
    def reload_annotations_from_file(self, index):

        iter= self.listSeq.get_iter(index)
        afile=self.listSeq.get_value(iter, 5).getfile()
        for aseq in CGeno.parseSeqFile(afile, "L"):
           # reload the sequence annotations only for the selected sequence
           if  aseq[0] == self.listSeq.get_value(iter, 3):
              bioobj = CGeno.BioSeqobj(aseq[1], afile)
              self.listSeq.set_value(iter, 3, str(aseq[0]))
              self.listSeq.set_value(iter, 5, bioobj)
        self.update_status('status', f"SUCCESS - Annotation reloaded. Please re-run blast files if the nucleotide sequence was modified in the updated sequence")


    ##############################################################################################
    # Add a new feature to the features table, with all its parameters
    ####################################################################
    def addFeat(self, notgal, feat_name,filt='',filtf='any',fillcol=None):
        iter = self.listFeat.append()

        if notgal:
          addrem_icon = "window-close"
          addrem_func = self.on_removeFeat_clicked
          heightR = strand = shape = hatch = linewidth = '-'
          linecol = featlabcol = None
          featlabtype = featlabpos = featlabrot = featlabsize = linecoltext = featlabcoltext = '-'

        else:
#          addrem_icon = "list-add"
#          addrem_func = self.on_addNewFeat_clicked
          addrem_icon = ""
          addrem_func = self.do_nothing 
          heightR = linewidth = '1'
          strand = 'none'
          shape = 'arrow'
          hatch = 'none'
          linecoltext = featlabcoltext = None
          fillcol = "rgb(171,171,171)"
          linecol = "rgb(0,0,0)"
          featlabcol = "rgb(255,255,255)" # This field is required to be set up as a color in the "General" feature line, in order to make the column functional for other features
          featlabtype = featlabpos = featlabrot = featlabsize = '-'

        # Update the cell printing if no color is prefefined
        if fillcol == None:
          fcoltext = '-'
        else:
          fcoltext = None

        self.listFeat.set(iter,
                         self.colsFeatID,
                         [notgal, addrem_icon, addrem_func, feat_name, True,
                          'CDS', strand, shape, hatch,
                          heightR, fcoltext, fillcol, True,
                          linewidth, linecoltext, linecol,
                          filt, filtf, self.on_copyFeat_clicked,
                          True, featlabtype, featlabpos, featlabcoltext, featlabcol, featlabsize, featlabrot, True]

                        )
        return iter

    ##############################################################################################
    # Add a new decoration to the decoration table, with all its parameters
    ####################################################################

    def addDeco(self, notgal, deco_name):
        iter = self.listDeco.append()

        step = "1000"
        window = "5000"
        decotype= "Scale"

        if notgal:
          addrem_icon = "window-close"
          addrem_func = self.on_removeDeco_clicked
          heightR = linewidth = '-'
          linecol = labelcol = None
          position = labelcoltext = linecoltext = labelsize = labelpos = "-"

        else:
#          addrem_icon = "list-add"
#          addrem_func = self.on_addNewDeco_clicked
          addrem_icon = ""
          addrem_func = self.do_nothing
          heightR = linewidth = '1'
          labelsize = '15'
          position = "above"
          labelpos = 'right'
          linecol = labelcol = "rgb(0,0,0)"
          labelcoltext = linecoltext = None
 
      
        # Create the Decoration
        self.listDeco.set(iter,
                         self.colsDecoID,
                         [notgal, addrem_icon, addrem_func, self.on_copyDeco_clicked, deco_name, True, 
                          decotype, step, window, position, heightR, linewidth, linecoltext, linecol, False, True, 
                          labelcoltext, labelcol, labelsize, labelpos, self.showDecoSeq]
                        )

        # To display the "genbank original position" toggle box only for Scale decorations
        if decotype == "Scale" and notgal:
          self.listDeco[iter][22]=True 
                       
        return iter

    ##############################################################################################
    # Add a new feature after clicking
    ##########################################################
    def on_addNewFeat_clicked(self, index):
        self.addFeat(True, "Feature "+str(self.featInc))
        self.featInc += 1
        self.update_status('status', f"SUCCESS - Add new feature")

    ##############################################################################################
    # Add a new Decoration after clicking
    ##########################################################
    def on_addNewDeco_clicked(self, index):

        deco_name="Deco "+str(self.decoInc)
        self.decoInc += 1

        # Check if new the value doesn't already exists, and increment until a new name is found
        # Can happen after loading a saved project where decoration names doesn't respect the order
        decoNames = [i[4]  for i in self.listDeco]
        while deco_name in decoNames:
          deco_name="Deco "+str(self.decoInc)
          self.decoInc += 1

        self.addDeco(True, deco_name)

        # Add the sequence-specific decoration selection to the DecorationSequences dictionary
        seqslist = self.get_sequence_list()
        self.DecorationSequences[deco_name] = {k[0]:1 for k in seqslist}

        self.update_status('status', f"SUCCESS - Add new decoration")

    ##############################################################################################
    # Copy feature after clicking
    ##########################################################
    def on_copyFeat_clicked(self, index):
        it = self.listFeat.get_iter(index)

        # Create the new Feature and copy values one by one
        itnew = self.listFeat.insert_after(it)
        for i in self.colsFeatID:
          self.listFeat.set_value(itnew, i, self.listFeat.get_value(it, i))

        self.listFeat.set_value(itnew, 3, "Feature "+str(self.featInc))
        self.featInc += 1
        self.update_status('status', f"SUCCESS - Copied feature")

    ##############################################################################################
    # Copy decoration after clicking
    ##########################################################
    def on_copyDeco_clicked(self, index):
        it = self.listDeco.get_iter(index)

        # Create the new Decoration and copy values one by one
        itnew = self.listDeco.insert_after(it)
        for i in self.colsDecoID:
            self.listDeco.set_value(itnew, i, self.listDeco.get_value(it, i))

        # create a new ID/name for the decoration
        newname="Deco "+str(self.decoInc)
        self.decoInc += 1
        # Check if new the value doesn't already exists, and increment until a new name is found
        # Can happen after loading a saved project where decoration names doesn't respect the order
        decoNames = [i[4]  for i in self.listDeco]
        while newname in decoNames:
          newname="Deco "+str(self.decoInc)
          self.decoInc += 1

        self.listDeco.set_value(itnew, 4, newname)

        # Update the dictionary for the sequence-specific selection display of decoration,
        # By copying the values from the original one
        copiedDecSequence=self.DecorationSequences[self.listDeco.get_value(it, 4)]
        self.DecorationSequences[newname]=copiedDecSequence

        self.update_status('status', f"SUCCESS - Copied decoration")

    ##############################################################################################
    # Remove the selected sequence
    ##########################################################
    def on_removeFeat_clicked(self, index):
        self.listFeat.remove(self.listFeat.get_iter(index))
        self.update_status('status', f"SUCCESS - Removed feature")

    def on_removeDeco_clicked(self, index):
        #        self.DecorationSequences.pop(self.listDeco[index][4]) # this line should be removed if things works well in Decoration manipulation since 08/2023
        self.listDeco.remove(self.listDeco.get_iter(index))
        self.update_status('status', f"SUCCESS - Removed decoration")


    ##############################################################################################
    # Return the longest item of a ListStore of strings
    ##########################################################
    def max_colsize(self, liststore):
      s = []
      it = liststore.get_iter_first()
      while it :
        s.append(len(liststore.get_value(it, 0)))
        it=liststore.iter_next(it) 

      return 6*(max(s)+2)
    

    ##############################################################################################
    def get_sequence_list(self):
        seqslist = []
        for i in range(1, self.listSeq.iter_n_children(None)):
            seqslist.append([self.listSeq[i][31], self.listSeq[i][3]])
        return seqslist

    # Return Decoration custom sequences selection 
    ##########################################################
    def initDecoSeq(self):
        self.DecorationSequences = dict()
        seqslist = self.get_sequence_list()
        for deco in self.listDeco:
            if deco[4] != "General":
                self.DecorationSequences[deco[4]] = {k[0]:1 for k in seqslist}

    def createDecoSeq(self):
        seqslist = self.get_sequence_list()
        self.dialogCustomDeco = CGeno.DialogCustomDecoration(self, seqslist)

    def showDecoSeq(self, index, action=True):
        seqslist = self.get_sequence_list()
        decoName = self.listDeco[index][4]
        self.dialogCustomDeco.update_seqslist(seqslist, decoName)
        if len(seqslist) > 1:
            self.dialogCustomDeco.update_check_from_values(self.DecorationSequences[decoName])
            if action == "noshow":
                return 'end'

            self.dialogCustomDeco.show()

        else:
            print("Please add at least 2 sequences to use custom filter.")
            self.update_status('status_error', f"Please add at least 2 sequences to use custom filter.")


    def updateDecoSeq(self, action, seqID):
        # For each Decoration
        for i in range(1, self.listDeco.iter_n_children(None)):
            # Get the actual Decoration ID (name)
            decoID = self.listDeco[i][4]

            # For now, the default Decoration names (Decoration 1, Decoration 2, etc.) still exist in the DecorationSequences dictionary
            # They need to not be updated
            if decoID in self.DecorationSequences.keys():
                if action == "add":
                    self.DecorationSequences[decoID][seqID] = 1
                # remove
                else:
                    self.DecorationSequences[decoID].pop(seqID)



    def DecoSeq_onsave(self, decoName):
        # We need to convert sequences IDs to sequence index in seqList
        seqslist = self.get_sequence_list()

        # When is this condition true? I don't know
        if not decoName in self.DecorationSequences.keys():
            print("No Decoration sequence selection for decoration {deconame} ! -> See DecoSeq_onSave function") 
            self.DecorationSequences[decoName] = {k[0]:1 for k in seqslist}  # Doesn't work: need to collect the seqID
        return self.DecorationSequences[decoName]

    ##############################################################################################
    # Show/hide elements when starting
    ##########################################################
    def initial_show(self):
       self.show_all()
       self.buttonBNDrevcolmin.hide()
       self.buttonBNDrevcolmax.hide()
       self.buttonBXDrevcolmin.hide()
       self.buttonBXDrevcolmax.hide()

       self.boxBX.hide()

    ##############################################################################################
    # Do nothing
    ##########################################################
    def do_nothing(self, index):
       return 1

#==========================================================================================================================
#
# Figure creation functions +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
#==========================================================================================================================

    ##############################################################################################
    # Construct the graph
    ################################################################
    def createcommand(self, button):

        self.outputfile = self.entryPName.get_text()
        if self.outputfile == '':
            self.on_picture_name()
        if self.outputfile == '':
            return None

        # Prepare the Figure creation by extracting all information from the GUI and putting in standard variables
        # This cannot be done in the migure making thread because GTK3+ doesn't support GUI interaction in alternative threads
        # (Causes a lot of bugs)
        ######################################################################################################################
        self.butRUN.set_sensitive(False)

        # format the figure file name according to its format and name.
        # Figure file name can be entered directly in the figure file bar : 
        # in this case, the output format could be inferred from the extension, or by default the last selected format is chosen
        if self.outputfile[-4:].lower() == '.svg':
            self.outputformat = 'SVG' 
        elif self.outputfile[-4:].lower() == '.png':
            self.outputformat = 'PNG' 
        elif 'SVG' in self.outputformat:
            self.outputfile = self.outputfile + '.svg'
        elif 'PNG' in self.outputformat:
            self.outputfile = self.outputfile + '.png'
        
        self.entryPName.set_text(self.outputfile)

        # collect Seqences, Features, and Decoration information
        self.seqstodraw=self.listSeq.return_seqEntries()
        self.featstodraw=self.listFeat.return_featDictionary()
        self.decotodraw=self.listDeco.return_decoDictionary()

        # convert tblastX ListStores into file lists  
        self.blastNlist=[]
        B = self.listBlastN.get_iter_first()
        while B != None:
          self.blastNlist.append(self.listBlastN.get_value(B, 1))
          B = self.listBlastN.iter_next(B)

        # convert tblastX ListStores into file lists  
        self.blastXlist=[]
        B = self.listBlastX.get_iter_first()
        while B != None:
          self.blastXlist.append(self.listBlastX.get_value(B, 1))
          B = self.listBlastX.iter_next(B)

        # initialise Blast custom windows if they don't exist (not loaded or destroyed after sequence move)
        if self.BNDialog == None:
          self.customB_window("noshow", "BLASTN")
        if self.BXDialog == None:
          self.customB_window("noshow", "TBLASTX")
 
        # collect custom blast selections
        self.BNcustomselection=self.BNDialog.get_checkedMatrix()        
        self.BXcustomselection=self.BXDialog.get_checkedMatrix()        

        # Run the Figure creation in one thread, and the waiting display in a second thread
        #########################
        self.threadcreate = threading.Thread(target=self.makeFigure)
        self.threadcreate.start()
        self.threaddot = threading.Thread(target=self.dotdotdot)
        self.threaddot.start()


    ##############################################################################################
    # Thread for the waiting display
    ################################################################
    def dotdotdot(self):
        dots=""
        while self.threadcreate.is_alive():
            # give a progress information, which requires to be done in the main thread (thanks to idle_add) because it interacts with the GUI
            GLib.idle_add(self.update_status,'status', f"Drawing figure"+str(dots))
            print('Drawing figure'+str(dots))
            dots+='.'        
            time.sleep(0.5)

    ##############################################################################################
    # Idle function to process the end of the figure creation
    ################################################################
    def end_makefigure(self):
        if self.theminblast == "NoSeq":
            self.update_status('status_error', f"ERROR - Please select at least one sequence to print out.")
        elif self.theminblast == 44:
            self.update_status('status_error', f"ERROR - Cannot create output file or directory")
        elif self.theminblast == 45:
            self.update_status('status_warning', f"WARNING - At least one sequence is smaller than the step/windows value(s) of a decoration. Please check the console for more details.")
        elif self.theminblast == 46:
            self.update_status('status_error', f"ERROR - BlastN file not compatible or missing. Make sure to provide a tab-delimited blast output file")
        elif self.theminblast == 47:
            self.update_status('status_error', f"ERROR - TBlastX file not compatible or missing. Make sure to provide a tab-delimited blast output file")
        elif self.theminblast == 101:
            self.update_status('status_ok', f"SUCCESS - Drawing figure complete.")
            print('Drawing figure complete.')
        else:
            self.update_status('status_error', f"ERROR - Drawing figure failed. Please check the console for more information.")
            print(self.theminblast)
        self.running = False
        self.butRUN.set_sensitive(True)



    ##############################################################################################
    # Thread for the figure construction
    ################################################################
    def makeFigure(self):

        self.theminblast = None

        try: 
        # create the output folder if it does not exist
         dirpath = os.path.dirname(self.outputfile)
         if dirpath != '' and not os.path.exists(dirpath):
            os.makedirs(dirpath)
            print("Folder ",dirpath, " created")


         # run the drawing
         self.theminblast = DGeno.drawsvg(self.outputfile, self.seqstodraw, self.featstodraw, self.decotodraw, self.blastNlist, self.blastXlist, self.return_options(), self.BNcustomselection, self.BXcustomselection, self.DecorationSequences)

        except(FileNotFoundError,OSError): # raise this error when cannot create the directory
          self.theminblast = 44
        except: # raise other errors whatever they are
          raise
        finally: GLib.idle_add(self.end_makefigure) # Run when the process is finished: allow post-treatment display which requires to be done in the main thread (thanks to idle_add)




#==========================================================================================================================
#
# Save/Load functions +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
#==========================================================================================================================

    ##############################################################################################
    # Extract the various general information values, and return them in a dictionary
    ################################################################
    def return_options(self):
        options={}


        # set general picture options    
        options['figwidth'] = self.entryPWidth.get_text()
        options['tmargin'] = self.entryPMtop.get_text()
        options['bmargin'] = self.entryPMbottom.get_text()
        options['lmargin'] = self.entryPMleft.get_text()
        options['rmargin'] = self.entryPMright.get_text()
        options['bg'] = self.checkPColor.get_active()
        options['bgcol'] = self.buttonPColor.get_rgba().to_string()
        options['enhanced'] = self.switchPEnhance.get_active()

        # set general sequences options    
        G = self.listSeq.get_iter_first()
        options['fheight'] = self.listSeq.get_value(G, 7)
        options['lwidth'] = self.listSeq.get_value(G, 8)
        options['inspace'] = self.listSeq.get_value(G, 9)
        options['lcol'] = self.listSeq.get_value(G, 12)
        options['spos'] = self.listSeq.get_value(G, 13)
        options['slabsel'] = self.listSeq.get_value(G, 17)
        options['slabid'] = self.listSeq.get_value(G, 18)
        options['slabpos'] = self.listSeq.get_value(G, 19)
        options['slaboffset'] = self.listSeq.get_value(G, 30)
        options['slabcol'] = self.listSeq.get_value(G, 21)
        options['slabsize'] = self.listSeq.get_value(G, 22)
        options['slabbold'] = self.listSeq.get_value(G, 32)
        options['slabital'] = self.listSeq.get_value(G, 33)

        options['flabsel'] = self.listSeq.get_value(G, 23)
        options['flabval'] = self.listSeq.get_value(G, 24)
        options['flabpos'] = self.listSeq.get_value(G, 25)
        options['flabcol'] = self.listSeq.get_value(G, 27)
        options['flabsize'] = self.listSeq.get_value(G, 28)
        options['flabrot'] = self.listSeq.get_value(G, 29)


        # set general features options    
        F = self.listFeat.get_iter_first()
        options['fstrand'] = self.listFeat.get_value(F, 6)
        options['fshape'] = self.listFeat.get_value(F, 7)
        options['fhatching'] = self.listFeat.get_value(F, 8)
        options['fcol'] = self.listFeat.get_value(F, 11)
        options['fstroke'] = self.listFeat.get_value(F, 13)
        options['fstrokecol'] = self.listFeat.get_value(F, 15)
        options['ftheight'] = self.listFeat.get_value(F, 9)
        options['flabbold'] = self.listFeat.get_value(F, 27)
        options['flabital'] = self.listFeat.get_value(F, 28)


        # set option for BlastN views        
        options['bnsel'] = 0
        if self.radioBNAdj.get_active(): options['bnsel'] = 1
        elif self.radioBNAll.get_active(): options['bnsel'] = 2
        elif self.radioBNOne.get_active(): options['bnsel'] = 3
        elif self.radioBNCustom.get_active(): options['bnsel'] = 4

        options['bnminl'] = self.entryBNDlength.get_text()
        options['bnmins'] = self.entryBNDsim.get_text()
        options['bnmaxe'] = self.entryBNDeval.get_text()
        options['bnoffset'] = self.entryBNDdist.get_text()
        options['bnselone'] = self.comboBNOne.get_active_text()
        options['bnseloneID'] = self.comboBNOne.get_active_id()

        options['bncol'] = self.buttonBNDcolmin.get_rgba().to_string()
        options['bncol2'] = self.buttonBNDcolmax.get_rgba().to_string()
        options['bncolinvset'] = self.checkBNDrevcol.get_active()
        options['bncolinv'] = self.buttonBNDrevcolmin.get_rgba().to_string()
        options['bncolinv2'] = self.buttonBNDrevcolmax.get_rgba().to_string()
        options['bnopacity'] = self.entryBNDopa.get_text()
        options['bnoutline'] = self.checkBNDoutline.get_active()

        options['bnlabshow'] = self.checkBNLshow.get_active()
        options['bnlabset'] = self.entryBNLmin.get_text()
        options['bnlabcol'] = self.buttonBNLcol.get_rgba().to_string()
        options['bnlabsize'] = self.entryBNLsize.get_text()
        options['bnlabdec'] = self.entryBNLdec.get_text()

        # set option for BlastX views        
        options['bxsel'] = 0
        if self.radioBXAdj.get_active(): options['bxsel'] = 1
        elif self.radioBXAll.get_active(): options['bxsel'] = 2
        elif self.radioBXOne.get_active(): options['bxsel'] = 3
        elif self.radioBXCustom.get_active(): options['bxsel'] = 4

        options['bxminl'] = self.entryBXDlength.get_text()
        options['bxmins'] = self.entryBXDsim.get_text()
        options['bxmaxe'] = self.entryBXDeval.get_text()
        options['bxoffset'] = self.entryBXDdist.get_text()
        options['bxselone'] = self.comboBXOne.get_active_text()
        options['bxseloneID'] = self.comboBXOne.get_active_id()

        options['bxcol'] = self.buttonBXDcolmin.get_rgba().to_string()
        options['bxcol2'] = self.buttonBXDcolmax.get_rgba().to_string()
        options['bxcolinvset'] = self.checkBXDrevcol.get_active()
        options['bxcolinv'] = self.buttonBXDrevcolmin.get_rgba().to_string()
        options['bxcolinv2'] = self.buttonBXDrevcolmax.get_rgba().to_string()
        options['bxopacity'] = self.entryBXDopa.get_text()
        options['bxoutline'] = self.checkBXDoutline.get_active()
        options['bxmerge'] = self.checkBXDmerge.get_active()

        options['bxlabshow'] = self.checkBXLshow.get_active()
        options['bxlabset'] = self.entryBXLmin.get_text()
        options['bxlabcol'] = self.buttonBXLcol.get_rgba().to_string()
        options['bxlabsize'] = self.entryBXLsize.get_text()
        options['bxlabdec'] = self.entryBXLdec.get_text()

        # set deneral options for decoration
        D = self.listDeco.get_iter_first()
        options['dselect'] = self.listDeco.get_value(D, 5)
        options['dtype'] = self.listDeco.get_value(D, 6)
        options['dstep'] = self.listDeco.get_value(D, 7)
        options['dwindow'] = self.listDeco.get_value(D, 8)
        options['dpos'] = self.listDeco.get_value(D, 9)
        options['dheightR'] = self.listDeco.get_value(D, 10)
        options['dlinewidth'] = self.listDeco.get_value(D, 11)
        options['dlinecolor'] = self.listDeco.get_value(D, 13)
        options['dreverse'] = self.listDeco.get_value(D, 14)
        options['dlabelprint'] = self.listDeco.get_value(D, 15)
        options['dlabelcolor'] = self.listDeco.get_value(D, 17)
        options['dlabelsize'] = self.listDeco.get_value(D, 18)
        options['dlabelpos'] = self.listDeco.get_value(D, 19)
        options['dlabelgbkpos'] = self.listDeco.get_value(D, 21)

        # set option for legends
        options['legFdisp'] = self.dispFeat_button.get_active()
        options['legFscale'] = self.scaleFeat.get_text()
        options['legFHpos'] = self.hposFeat_combo.get_active_text()
        options['legFVpos'] = self.vposFeat_combo.get_active_text()
        options['legFfsize'] = self.fsizeFeat.get_text()
        options['legFfcol'] = self.fcolorFeat.get_rgba().to_string()
        options['legFnb'] = self.nbcolsFeat.get_text()

        options['legBdisp'] = self.dispBlast_button.get_active()
        options['legBXdisp'] = self.dispBlastX_button.get_active()
        options['legBscale'] = self.scaleBlast.get_text()
        options['legBXscale'] = self.scaleBlastX.get_text()
        options['legBHpos'] = self.hposBlast_combo.get_active_text()
        options['legBXHpos'] = self.hposBlastX_combo.get_active_text()
        options['legBVpos'] = self.vposBlast_combo.get_active_text()
        options['legBXVpos'] = self.vposBlastX_combo.get_active_text()
        options['legBfsize'] = self.fsizeBlast.get_text()
        options['legBXfsize'] = self.fsizeBlastX.get_text()
        options['legBfcol'] = self.fcolorBlast.get_rgba().to_string()
        options['legBXfcol'] = self.fcolorBlastX.get_rgba().to_string()

        options['legBlastlen'] = len(self.listBlastN)
        options['legBlastXlen'] = len(self.listBlastX)

        options['legSdisp'] = self.dispScale_button.get_active()
        options['legSscale'] = self.scaleScale.get_text()
        options['legSHpos'] = self.hposScale_combo.get_active_text()
        options['legSVpos'] = self.vposScale_combo.get_active_text()
        options['legSfsize'] = self.fsizeScale.get_text()
        options['legSfcol'] = self.fcolorScale.get_rgba().to_string()

        return(options)


    ##################################################################
    # Reload the various general information values from a dictionary 
    ##################################################################
    def reload_options(self, options):
        # for backward compatibility
        if not 'slaboffset' in options: options['slaboffset']='40'
        if not 'slabbold' in options: options['slabbold']='False'
        if not 'slabital' in options: options['slabital']='False'
        if not 'flabbold' in options: options['flabbold']='False'
        if not 'flabital' in options: options['flabital']='False'
        if not 'dlabelgbkpos' in options: options['dlabelgbkpos']='False'

        # reload general picture options    
        self.entryPWidth.set_text(str(options['figwidth']))
        self.entryPMtop.set_text(str(options['tmargin']))
        self.entryPMbottom.set_text(str(options['bmargin']))
        self.entryPMleft.set_text(str(options['lmargin']))
        self.entryPMright.set_text(str(options['rmargin']))
        self.checkPColor.set_active(options['bg'] == 'True')
        tmpcolor=Gdk.RGBA()
        tmpcolor.parse(options['bgcol'])
        self.buttonPColor.set_rgba(tmpcolor)
        self.switchPEnhance.set_active(options['enhanced'] == 'True')

        # reload general sequences options    
        G = self.listSeq.get_iter_first()
        self.listSeq.set_value(G, 7, str(options['fheight']))
        self.listSeq.set_value(G, 8, str(options['lwidth']))
        self.listSeq.set_value(G, 9, str(options['inspace']))
        self.listSeq.set_value(G, 12, str(options['lcol']))
        self.listSeq.set_value(G, 13, str(options['spos']))

        self.listSeq.set_value(G, 17, options['slabsel'] == 'True')
        self.listSeq.set_value(G, 18, str(options['slabid']))
        self.listSeq.set_value(G, 19, str(options['slabpos']))
        self.listSeq.set_value(G, 30, str(options['slaboffset']))
        self.listSeq.set_value(G, 21, str(options['slabcol']))
        self.listSeq.set_value(G, 22, str(options['slabsize']))
        self.listSeq.set_value(G, 32, options['slabbold'] == 'True')
        self.listSeq.set_value(G, 33, options['slabital'] == 'True')

        self.listSeq.set_value(G, 23, options['flabsel'] == 'True')
        self.listSeq.set_value(G, 24, str(options['flabval']))
        self.listSeq.set_value(G, 25, str(options['flabpos']))
        self.listSeq.set_value(G, 27, str(options['flabcol']))
        self.listSeq.set_value(G, 28, str(options['flabsize']))
        self.listSeq.set_value(G, 29, str(options['flabrot']))

        # reload general features options    
        F = self.listFeat.get_iter_first()
        self.listFeat.set_value(F, 6, str(options['fstrand']))
        self.listFeat.set_value(F, 7, str(options['fshape']))
        self.listFeat.set_value(F, 8, str(options['fhatching']))
        self.listFeat.set_value(F, 9, str(options['ftheight']))
        self.listFeat.set_value(F, 11, str(options['fcol']))
        self.listFeat.set_value(F, 13, str(options['fstroke']))
        self.listFeat.set_value(F, 15, str(options['fstrokecol']))
        self.listFeat.set_value(F, 27, options['flabbold'] == 'True')
        self.listFeat.set_value(F, 28, options['flabital'] == 'True')

        # reload option for BlastN views        
        if options['bnsel'] == '1': self.radioBNAdj.set_active(True) 
        elif options['bnsel'] == '2': self.radioBNAll.set_active(True)
        elif options['bnsel'] == '3': self.radioBNOne.set_active(True)
        elif options['bnsel'] == '4': self.radioBNCustom.set_active(True)

        self.entryBNDlength.set_text(str(options['bnminl']))
        self.entryBNDsim.set_text(str(options['bnmins']))
        self.entryBNDeval.set_text(str(options['bnmaxe']))
        self.entryBNDdist.set_text(str(options['bnoffset']))
        tmpcolor.parse(options['bncol'])
        self.buttonBNDcolmin.set_rgba(tmpcolor)
        tmpcolor.parse(options['bncol2'])
        self.buttonBNDcolmax.set_rgba(tmpcolor)
        self.checkBNDrevcol.set_active(options['bncolinvset'] == 'True')

        tmpcolor.parse(options['bncolinv'])
        self.buttonBNDrevcolmin.set_rgba(tmpcolor)
        tmpcolor.parse(options['bncolinv2'])
        self.buttonBNDrevcolmax.set_rgba(tmpcolor)
        self.entryBNDopa.set_text(str(options['bnopacity']))
        self.checkBNDoutline.set_active(options['bnoutline'] == 'True')

        self.checkBNLshow.set_active(options['bnlabshow'] == 'True')
        self.entryBNLmin.set_text(str(options['bnlabset']))
        tmpcolor.parse(options['bnlabcol'])
        self.buttonBNLcol.set_rgba(tmpcolor)
        self.entryBNLsize.set_text(str(options['bnlabsize']))
        self.entryBNLdec.set_text(str(options.get('bnlabdec',self.entryBNLdec.get_text())))

        # reload option for TBlastX views        
        if options['bxsel'] == '1': self.radioBXAdj.set_active(True) 
        elif options['bxsel'] == '2': self.radioBXAll.set_active(True)
        elif options['bxsel'] == '3': self.radioBXOne.set_active(True)
        elif options['bxsel'] == '4': self.radioBXCustom.set_active(True)

        self.entryBXDlength.set_text(str(options['bxminl']))
        self.entryBXDsim.set_text(str(options['bxmins']))
        self.entryBXDeval.set_text(str(options['bxmaxe']))
        self.entryBXDdist.set_text(str(options['bxoffset']))
        tmpcolor.parse(options['bxcol'])
        self.buttonBXDcolmin.set_rgba(tmpcolor)
        tmpcolor.parse(options['bxcol2'])
        self.buttonBXDcolmax.set_rgba(tmpcolor)
        self.checkBXDrevcol.set_active(options['bxcolinvset'] == 'True')

        tmpcolor.parse(options['bxcolinv'])
        self.buttonBXDrevcolmin.set_rgba(tmpcolor)
        tmpcolor.parse(options['bxcolinv2'])
        self.buttonBXDrevcolmax.set_rgba(tmpcolor)
        self.entryBXDopa.set_text(str(options['bxopacity']))
        self.checkBXDoutline.set_active(options['bxoutline'] == 'True')
        self.checkBXDmerge.set_active(options.get('bxmerge','True') == 'True')

        self.checkBXLshow.set_active(options['bxlabshow'] == 'True')
        self.entryBXLmin.set_text(str(options['bxlabset']))
        tmpcolor.parse(options['bxlabcol'])
        self.buttonBXLcol.set_rgba(tmpcolor)
        self.entryBXLsize.set_text(str(options['bxlabsize']))
        self.entryBXLdec.set_text(str(options.get('bxlabdec',self.entryBXLdec.get_text())))


        # Reload legend options

        posHIndex = ["Left", "Middle", "Right"]
        posVIndex = ["Top", "Middle", "Bottom"]

        if options['legFdisp'] == "True":
            legFdisp = True
        else:
            legFdisp = False

        self.dispFeat_button.set_active(legFdisp)
        self.scaleFeat.set_text(options.get('legFscale',self.scaleFeat.get_text()))
        self.hposFeat_combo.set_active(posHIndex.index(options.get('legFHpos',self.hposFeat_combo.get_active_text())))
        self.vposFeat_combo.set_active(posVIndex.index(options.get('legFVpos',self.vposFeat_combo.get_active_text())))
        self.fsizeFeat.set_text(options.get('legFfsize',self.fsizeFeat.get_text()))
        tmpcolor.parse(options.get('legFfcol',self.fcolorFeat.get_rgba().to_string()))
        self.fcolorFeat.set_rgba(tmpcolor)
        self.nbcolsFeat.set_text(options.get('legFnb'))

        
        if options['legBdisp'] == "True":
            legBdisp = True
        else:
            legBdisp = False

        self.dispBlast_button.set_active(legBdisp)
        self.scaleBlast.set_text(options.get('legBscale',self.scaleBlast.get_text()))
        self.hposBlast_combo.set_active(posHIndex.index(options.get('legBHpos',self.hposBlast_combo.get_active_text())))
        self.vposBlast_combo.set_active(posVIndex.index(options.get('legBVpos',self.vposBlast_combo.get_active_text())))
        self.fsizeBlast.set_text(options.get('legBfsize',self.fsizeBlast.get_text()))
        tmpcolor.parse(options.get('legBfcol',self.fcolorBlast.get_rgba().to_string()))
        self.fcolorBlast.set_rgba(tmpcolor)


        if options['legBXdisp'] == "True":
            legBXdisp = True
        else:
            legBXdisp = False

        self.dispBlastX_button.set_active(legBXdisp)
        self.scaleBlastX.set_text(options.get('legBXscale',self.scaleBlastX.get_text()))
        self.hposBlastX_combo.set_active(posHIndex.index(options.get('legBXHpos',self.hposBlastX_combo.get_active_text())))
        self.vposBlastX_combo.set_active(posVIndex.index(options.get('legBXVpos',self.vposBlastX_combo.get_active_text())))
        self.fsizeBlastX.set_text(options.get('legBXfsize',self.fsizeBlastX.get_text()))
        tmpcolor.parse(options.get('legBXfcol',self.fcolorBlastX.get_rgba().to_string()))
        self.fcolorBlastX.set_rgba(tmpcolor)
        
        if options['legSdisp'] == "True":
            legSdisp = True
        else:
            legSdisp = False
        self.dispScale_button.set_active(legSdisp)
        self.scaleScale.set_text(options.get('legSscale',self.scaleScale.get_text()))
        self.hposScale_combo.set_active(posHIndex.index(options.get('legSHpos',self.hposScale_combo.get_active_text())))
        self.vposScale_combo.set_active(posVIndex.index(options.get('legSVpos',self.vposScale_combo.get_active_text())))
        self.fsizeScale.set_text(options.get('legSfsize',self.fsizeScale.get_text()))
        tmpcolor.parse(options.get('legSfcol',self.fcolorScale.get_rgba().to_string()))
        self.fcolorScale.set_rgba(tmpcolor)
        
        # reload general decoration options    
        D = self.listDeco.get_iter_first()
        self.listDeco.set_value(D, 5, options['dselect'] == 'True')
        self.listDeco.set_value(D, 6, str(options['dtype']))
        self.listDeco.set_value(D, 7, str(options['dstep']))
        self.listDeco.set_value(D, 8, str(options['dwindow']))
        self.listDeco.set_value(D, 9, str(options['dpos']))
        self.listDeco.set_value(D, 10, str(options['dheightR']))
        self.listDeco.set_value(D, 11, str(options['dlinewidth']))
        self.listDeco.set_value(D, 13, str(options['dlinecolor']))
        self.listDeco.set_value(D, 14, options['dreverse'] == 'True')
        self.listDeco.set_value(D, 15, options['dlabelprint'] == 'True')
        self.listDeco.set_value(D, 17, str(options['dlabelcolor']))
        self.listDeco.set_value(D, 18, str(options['dlabelsize']))
        self.listDeco.set_value(D, 19, str(options['dlabelpos']))
        self.listDeco.set_value(D, 21, options['dlabelgbkpos'] == 'True')


    ##################################################################
    # Reload the feature information values from a dictionary 
    ##################################################################
    def reload_feat(self, F, entry):
            # for backward compatibility
            if not 'flabbold' in entry: entry['flabbold']='False'
            if not 'flabital' in entry: entry['flabital']='False'

            self.listFeat.set_value(F, 3, str(entry['id']))
            self.listFeat.set_value(F, 4, int(entry['sel']))
            self.listFeat.set_value(F, 26, int(entry.get('flegsel',0)))

            self.listFeat.set_value(F, 5, str(entry['feat']))
            self.listFeat.set_value(F, 6, str(entry['strand']))
            self.listFeat.set_value(F, 7, str(entry['shape']))
            self.listFeat.set_value(F, 8, str(entry['hatching']))
            self.listFeat.set_value(F, 9, str(entry['height']))

            if (str(entry['col'])) != 'None':
             self.listFeat.set_value(F, 10, "")
             self.listFeat.set_value(F, 11, str(entry['col']))
            self.listFeat.set_value(F, 12, int(entry['fill']))
            self.listFeat.set_value(F, 13, str(entry['stroke']))

            if (str(entry['strokecol'])) != 'None':
             self.listFeat.set_value(F, 14, "")
             self.listFeat.set_value(F, 15, str(entry['strokecol']))
            self.listFeat.set_value(F, 16, str(entry['filter']))
            self.listFeat.set_value(F, 17, str(entry['field']))

            self.listFeat.set_value(F, 19, entry['flabsel'] == 'True')
            self.listFeat.set_value(F, 20, str(entry['flabval']))
            self.listFeat.set_value(F, 21, str(entry['flabpos']))
            self.listFeat.set_value(F, 27, entry['flabbold'] == 'True')
            self.listFeat.set_value(F, 28, entry['flabital'] == 'True')

            if (str(entry['flabcol'])) != "None":
             self.listFeat.set_value(F, 22, "")
             self.listFeat.set_value(F, 23, str(entry['flabcol']))
            self.listFeat.set_value(F, 24, str(entry['flabsize']))
            self.listFeat.set_value(F, 25, str(entry['flabrot']))


    ##################################################################
    # Reload the feature information values from a dictionary 
    ##################################################################
    def reload_seq(self, S, entry):

            # for backward compatibility
            if not 'slaboffset' in entry: entry['slaboffset']='-'
            if not 'slabbold' in entry: entry['slabbold']='False'
            if not 'slabital' in entry: entry['slabital']='False'

            self.listSeq.set_value(S, 6, int(entry['sel']))

            self.listSeq.set_value(S, 7, str(entry['height']))
            self.listSeq.set_value(S, 8, str(entry['lwidth']))
            self.listSeq.set_value(S, 9, str(entry['inspace']))
            if (str(entry['lcol'])) != 'None':
              self.listSeq.set_value(S, 10, "")
              self.listSeq.set_value(S, 12, str(entry['lcol']))

            self.listSeq.set_value(S, 13, str(entry['spos']))
            self.listSeq.set_value(S, 11, str(entry['sxpos']))
            self.listSeq.set_value(S, 14, str(entry['min']))
            self.listSeq.set_value(S, 15, str(entry['max']))
            self.listSeq.set_value(S, 16, entry['rev'] == 'True')

            self.listSeq.set_value(S, 17, entry['slabsel'] == 'True')
            self.listSeq.set_value(S, 18, str(entry['slabid']))
            self.listSeq.set_value(S, 19, str(entry['slabpos']))
            self.listSeq.set_value(S, 30, str(entry['slaboffset']))
            if (str(entry['slabcol'])) != 'None':
              self.listSeq.set_value(S, 20, "")
              self.listSeq.set_value(S, 21, str(entry['slabcol']))
            self.listSeq.set_value(S, 22, str(entry['slabsize']))
            self.listSeq.set_value(S, 32, entry['slabbold'] == 'True')
            self.listSeq.set_value(S, 33, entry['slabital'] == 'True')


            self.listSeq.set_value(S, 23, entry['flabsel'] == 'True')
            self.listSeq.set_value(S, 24, str(entry['flabval']))
            self.listSeq.set_value(S, 25, str(entry['flabpos']))
            if (str(entry['flabcol'])) != 'None':
              self.listSeq.set_value(S, 26, "")
              self.listSeq.set_value(S, 27, str(entry['flabcol']))
            self.listSeq.set_value(S, 28, str(entry['flabsize']))
            self.listSeq.set_value(S, 29, str(entry['flabrot']))
            self.listSeq.set_value(S, 31, int(entry['ID']))

            if self.seqID < int(entry['ID']): self.seqID=int(entry['ID'])+1 # Needed in case of larger ID value compared to the number of sequences (for instance after saving a project in which sequences were removed 


    ##################################################################
    # Reload the decoration information values from a dictionary 
    ##################################################################
    def reload_deco(self, D, entry):

            # for backward compatibility
            if not 'labelgbkpos' in entry: entry['labelgbkpos']='False'

            self.listDeco.set_value(D, 4, str(entry['id']))
            self.listDeco.set_value(D, 5, int(entry['select']))
            self.listDeco.set_value(D, 6, str(entry['type']))
            if str(entry['type']) == 'Scale':
             self.listDeco.set_value(D, 22, 1)
            else:
             self.listDeco.set_value(D, 22, 0)
            
            self.listDeco.set_value(D, 7, str(entry['step']))
            self.listDeco.set_value(D, 8, str(entry['window']))

            self.listDeco.set_value(D, 9, str(entry['pos']))
            self.listDeco.set_value(D, 10, str(entry['heightR']))
            self.listDeco.set_value(D, 11, str(entry['linewidth']))
            if (str(entry['linecolor'])) != 'None':
                self.listDeco.set_value(D, 12, "")
                self.listDeco.set_value(D, 13, str(entry['linecolor']))

            self.listDeco.set_value(D, 14, entry['reverse'] == 'True')
            self.listDeco.set_value(D, 15, entry['labelprint'] == 'True')

            if (str(entry['labelcolor'])) != 'None':
                self.listDeco.set_value(D, 16, "")
                self.listDeco.set_value(D, 17, str(entry['labelcolor']))

            self.listDeco.set_value(D, 18, str(entry['labelsize']))
            self.listDeco.set_value(D, 19, str(entry['labelpos']))
            self.listDeco.set_value(D, 21, entry['labelgbkpos'] == 'True')


    ##############################################################################################
    # Save the general configuration settings and features to a file - do not ask for filename, just erase current file
    ##############################################################################################
    def on_Save(self, action, param, saveType):
        if saveType != "saveC":
            if self.isCurrentSavePath != 0: # other condition not necessary -> and self.current_working_filename != 'new.genofig':
                self.Save(f"{self.saveDirectoryPath}/{self.current_working_filename}", 'saveA')
            else:
               self.on_SaveAs(action, param, saveType)
        else:
            self.on_SaveAs(action, param, saveType)


    ##############################################################################################
    # Save the general configuration settings and features to a file
    ##############################################################################################
    def on_SaveAs(self, action, param, saveType):

        current_file= self.current_working_filename 
        current_title= "Save Project as ..."
        current_folder= self.saveDirectoryPath
        extension = EXT_ALL
        filetype = FTYPE_ALL

        if saveType == "saveC":
          extension = EXT_CONFIG
          filetype = FTYPE_CONFIG
          current_file = self.current_working_configname
          current_folder = self.configDirectoryPath
          current_title = "Save Configuration as ..."
          if current_file != os.path.basename(current_file):
            current_file = os.path.basename(self.current_working_configname)

        elif saveType == "saveA":
          self.isCurrentSavePath = 1

        else:
          print("Erreur in id of saving. should be saveC or saveA\n")
          return 0

        # Open a filechooser dialog box
        self.SaveDialog = Gtk.FileChooserDialog(title=current_title, action=Gtk.FileChooserAction.SAVE)
        self.SaveDialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK)
        filefilter1= Gtk.FileFilter()
        filefilter1.add_pattern("*."+extension)
        filefilter1.set_name(filetype)
        filefilter2= Gtk.FileFilter()
        filefilter2.add_pattern("*")
        filefilter2.set_name("All files")
        self.SaveDialog.add_filter(filefilter1)
        self.SaveDialog.add_filter(filefilter2)
        self.SaveDialog.set_do_overwrite_confirmation(True)
        self.SaveDialog.set_current_name(current_file)
        self.SaveDialog.set_current_folder(current_folder)

        response = self.SaveDialog.run()

        # store the selected filename
        if response == Gtk.ResponseType.OK:
          outfilename=self.SaveDialog.get_filename()
          self.SaveDialog.destroy()
        else:
          self.SaveDialog.destroy()
          return 0


        if outfilename[-8:].lower() != '.'+extension:
            outfilename = outfilename + '.'+extension

        self.Save(outfilename, saveType)

    ##############################################################################################
    # Save the general configuration settings and features to a file
    ##############################################################################################
    def Save(self, outfilename, saveType):
        # write out configuration settings
        outf=open(outfilename, 'w')

        # save version
        outf.write('#GenoFig version %s\n' % VERSION)

        # save general settings (from a dictionary)
        #---------------------------------
        options = self.return_options()
        outf.write('#General options\n')
        for key, value in options.items():
            outf.write('%s:%s\n' % (key, value))

        # save feature settings
        #---------------------------------
        outf.write('#Features\n')
        features=self.listFeat.return_featDictionary()
        for feat in features:
          for key, value in feat.items():
            outf.write('%s:%s\t' % (key, value))    ##### POSSIBLE BUG: the id or the filter contains '\t' -> ##### Need to be managed in the GUI 
          outf.write('END\n')             

        # save decoration settings
        #---------------------------------
        outf.write('#Decorum\n')
        
        for i, deco in enumerate(self.listDeco.return_decoDictionary()):
            decoName = deco['id']
            if decoName != "General":
                for key, value in deco.items():
                   outf.write('%s:%s\t' % (key, value))
                # Saving custom decoration selection for sequences, except if it is a configuration file
                if saveType == "saveC":
                  outf.write('custom:"{}"\t')
                else:
                  outf.write('custom:"%s"\t' % (self.DecoSeq_onsave(decoName)))
                outf.write('END\n')             


        # End of Config saving
        #---------------------------------------
        if saveType == "saveC":
            outf.write('#END\n')
            self.current_working_configname = os.path.basename(outfilename)
            self.configDirectoryPath = os.path.dirname(outfilename)
            self.update_status('status', f"SUCCESS - Configuration saved")
            return 0


        # save sequences settings
        #---------------------------------
        outf.write('#SEQUENCES\n')
        sequences=self.listSeq.return_seqEntries()
        for seq in sequences:
            for key, value in seq.gp.items():
                if key == "file":
                    outf.write('%s:%s\t' % (key, os.path.relpath(value, os.path.dirname(outfilename))))
                else:
                    outf.write('%s:%s\t' % (key, value))
            outf.write('END\n')             


        # save Figure and loaded blast files
        #---------------------------------
        outf.write('#FIGURE OUTPUT\n')
        if self.outputfile != "":
            outf.write('%s\n' % (os.path.relpath(self.outputfile, os.path.dirname(outfilename))))
        outf.write('#BlastN files\n')
        BNF = self.listBlastN.get_iter_first()
        while BNF != None:
            outf.write('%s\n' % (os.path.relpath(self.listBlastN.get_value(BNF, 1), os.path.dirname(outfilename))))
            BNF = self.listBlastN.iter_next(BNF)

        outf.write('#BlastX files\n')
        BXF = self.listBlastX.get_iter_first()
        while BXF != None:
            outf.write('%s\n' % (os.path.relpath(self.listBlastX.get_value(BXF, 1), os.path.dirname(outfilename))))
            BXF = self.listBlastX.iter_next(BXF)

        outf.write('#BlastN custom display\n')
        if self.BNDialog == None:
         outf.write(str(self.BNDchecked)+"\n")
        else:
         outf.write(str(self.BNDialog.get_checked())+"\n")

        outf.write('#BlastX custom display\n')
        if self.BXDialog == None:
         outf.write(str(self.BXDchecked)+"\n")
        else:
         outf.write(str(self.BXDialog.get_checked())+"\n")

        outf.write('#END\n')
       
        self.current_working_filename = os.path.basename(outfilename)
        self.set_title('GenoFig '+VERSION + ' ' + self.current_working_filename)
        self.saveDirectoryPath = os.path.dirname(outfilename)
        if self.seqDirectoryPath == '':				# Update seq directory if not defined yet
              self.seqDirectoryPath = self.saveDirectoryPath
        if self.figDirectoryPath == '':				# Update fig directory if not defined yet
              self.figDirectoryPath = self.saveDirectoryPath
        if self.configDirectoryPath == '':				# Update config directory if not defined yet
              self.configDirectoryPath = self.saveDirectoryPath

        self.update_status('status', f"SUCCESS - Project saved")

    
    ##############################################################################################
    # Reset all fields
    ##############################################################################################
    def reset_all_fields(self, loadType):
        # Clear up current sequences, and recreate the general sequence
        # Clear up loaded blast files
        #--------------------------------------------------------------
        if loadType == "open":        
          self.listSeq.clear()
          self.seqID = 0
          self.addSeq(False, "General", "", "")
          self.listBlastN.clear()
          self.radioBNNone.set_active(True)
          self.radioBNAdj.set_active(False)
          self.radioBNAll.set_active(False)
          self.radioBNOne.set_active(False)
          self.radioBNCustom.set_active(False)
          self.listBlastX.clear()
          self.radioBXNone.set_active(True)
          self.radioBXAdj.set_active(False)
          self.radioBXAll.set_active(False)
          self.radioBXOne.set_active(False)
          self.radioBXCustom.set_active(False)

        # Clear up current features, and recreate the general feature + CDS feature
        #------------------------------------------------------------
        self.listFeat.clear()
        self.addFeat(False, "General")
        self.featInc = 1
        
        # Clear up current decoration, and recreate the general decoration
        #------------------------------------------------------------
        self.listDeco.clear()
        self.DecorationSequences = dict()
        self.addDeco(False, "General")
        self.decoInc = 1

        # Reset Legends values
        #------------------------------------------------------------
        self.set_legends_default()

    ##############################################################################################
    # Set defaults values for LEGENDS FIELDS
    ##############################################################################################
    def set_legends_default(self):
        self.dispScale_button.set_active(True)
        self.dispFeat_button.set_active(False)
        self.dispBlast_button.set_active(False)
        self.dispBlastX_button.set_active(False)
        self.hposFeat_combo.set_active_id('3')
        self.hposBlast_combo.set_active_id('1')
        self.hposBlastX_combo.set_active_id('1')
        self.hposScale_combo.set_active_id('1')
        self.vposFeat_combo.set_active_id('3')
        self.vposBlast_combo.set_active_id('1')
        self.vposBlastX_combo.set_active_id('1')
        self.vposScale_combo.set_active_id('3')
        self.scaleFeat.set_text('0.3')
        self.scaleBlast.set_text('0.3')
        self.scaleBlastX.set_text('0.3')
        self.scaleScale.set_text('0.3')
        self.fsizeFeat.set_text('25')
        self.fsizeBlast.set_text('25')
        self.fsizeBlastX.set_text('25')
        self.fsizeScale.set_text('25')
        self.fcolorFeat_setup.parse("#000000")
        self.fcolorBlast_setup.parse("#000000")
        self.fcolorBlastX_setup.parse("#000000")
        self.fcolorScale_setup.parse("#000000")
        self.fcolorFeat.set_rgba(self.fcolorFeat_setup)
        self.fcolorBlast.set_rgba(self.fcolorBlast_setup)
        self.fcolorBlastX.set_rgba(self.fcolorBlastX_setup)
        self.fcolorScale.set_rgba(self.fcolorScale_setup)
        self.nbcolsFeat.set_text("1")

    
    def Load_default_configuration(self):
        if sys.platform.startswith('linux') and os.path.exists(os.path.expanduser('~/.genofig/default.genocfg')):
            self.Load('loadC', os.path.expanduser('~/.genofig/default.genocfg'))
            self.current_working_configname = os.path.expanduser('~/.genofig/default.genocfg')
        else:
            #Populate the Feature list with the generic CDS feature + hypothetical proteins CDS, then set up the features increment (for ID)
            self.addFeat(True, "CDS")
            self.featInc += 1
            self.addFeat(True, "Hypothetical",filt="hypothetical prot",filtf="product",fillcol="rgb(240,240,240)")
            self.featInc += 1


    def Save_default_configuration(self, action, param):
        if sys.platform.startswith('linux'):
                 dirpath=os.path.expanduser('~/.genofig/')
                 if dirpath != '' and not os.path.exists(dirpath):
                   os.makedirs(dirpath)
                 self.Save(os.path.expanduser('~/.genofig/default.genocfg'),'saveC')


    ##############################################################################################
    # Reset projet from scratch - load default configuration if exists
    ##############################################################################################
    def on_New(self, action, param, loadType):

        d = Gtk.MessageDialog(transient_for=self,
                              modal=True,
                              buttons=Gtk.ButtonsType.OK_CANCEL)
        d.props.text = 'Are you sure you want to create a new project and lost unsaved modifications ?'
        response = d.run()
        d.destroy()

        if response == Gtk.ResponseType.OK:
            self.isCurrentSavePath = 0
            self.reset_all_fields('open')

            self.current_working_filename = "new.genofig"
            self.current_working_configname = "new.genocfg"
            self.set_title('GenoFig '+VERSION+ ' New - unsaved')
          
            self.entryPName.set_text("")

            self.Load_default_configuration()
        self.update_status('status', f"Status OK - Waiting for action")

    ##############################################################################################
    # Open a configuration or project settings file
    ##############################################################################################
    def on_Load(self, action, param, loadType):
        # open configuration
        if loadType == "loadC":
          extension = EXT_CONFIG
          filetype = FTYPE_CONFIG
          open_title="choose a configuration file ..."
          current_folder=self.configDirectoryPath
        # open project
        elif loadType == "open":
          extension = EXT_ALL
          filetype = FTYPE_ALL
          open_title="choose a project file ..."
          current_folder=self.saveDirectoryPath
          self.isCurrentSavePath = 1
        else:
          print("Erreur in id of loading. should be loadC or open\n")
          return 0

        # Open a filechooser dialog box
        self.LoadDialog = Gtk.FileChooserDialog(parent=self, title=open_title, action=Gtk.FileChooserAction.OPEN)
        self.LoadDialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)

        filefilter1= Gtk.FileFilter()
        filefilter1.add_pattern("*."+extension)
        filefilter1.set_name(filetype)
        filefilter2= Gtk.FileFilter()
        filefilter2.add_pattern("*")
        filefilter2.set_name("All files")
        self.LoadDialog.add_filter(filefilter1)
        self.LoadDialog.add_filter(filefilter2)
        self.LoadDialog.set_current_folder(current_folder)

        response = self.LoadDialog.run()

        # store the selected filename
        if response == Gtk.ResponseType.OK:
          cfilename=self.LoadDialog.get_filename()
          self.LoadDialog.destroy()
        else:
          self.LoadDialog.destroy()
          return 0

        self.Load(loadType, cfilename)

    ##############################################################################################
    # Open a configuration or project settings file
    ##############################################################################################
    def Load(self, loadType, cfilename):
        self.reset_all_fields(loadType)

        # Open configuration settings file
        #---------------------------------
        cfile=open(cfilename, 'r')
 
        # pass the first and second fields= GenoFig version and '#General options'
        cfile.readline().rstrip()
        cfile.readline().rstrip()

        # Store the general options in a dictionary, and restore them
        #---------------------------------
        goptions = {}
        lineI=cfile.readline().rstrip()
        while not lineI.startswith('#Feat'):
          val = lineI.split(':')
          goptions[val[0]]=val[1]
          lineI=cfile.readline().rstrip()
        self.reload_options(goptions)

        # Restore Features
        #---------------------------------
        lineI=cfile.readline().rstrip()
        F = self.listFeat.get_iter_first() # point to the first feature, which is the "General"
        while not lineI.startswith('#Decorum'):
          # collect params in a dictionary
          allparams = lineI.replace("\tEND", "").split('\t')
          pDict={}
          featname=''
          for param in allparams:
            val=param.split(':')
            pDict[val[0]]=val[1]
            if val[0] == 'id':
                featName = val[1]

          # create basic feature
          self.addFeat(True, featname)
          self.featInc += 1
          F = self.listFeat.iter_next(F)

          # change values of the newly created feature, and pass to next one
          self.reload_feat(F, pDict)
          lineI=cfile.readline().rstrip()



        # Restore Decoration
        #---------------------------------
        lineI=cfile.readline().rstrip()
        D = self.listDeco.get_iter_first() # point to the first feature, which is the "General"
        while not lineI.startswith('#'):
          # collect params in a dictionary
          allparams = lineI.replace("\tEND", "").split('\t')
          pDict={}
          deconame=''
          for param in allparams:
            val=param.split(':')
            pDict[val[0]]=val[1]
            if val[0] == 'id':
                decoName = val[1]
            if val[0] == 'custom':
                self.DecorationSequences[decoName] = ast.literal_eval(lineI.split('"')[1])

          # create basic decoration
          self.addDeco(True, deconame)
          self.decoInc += 1
          D = self.listDeco.iter_next(D)

          ## change values of the newly created decoration, and pass to next one
          self.reload_deco(D, pDict)
          lineI=cfile.readline().rstrip()

    
        # End of Config loading
        if loadType == "loadC":
            self.current_working_configname = os.path.basename(cfilename)
            self.configDirectoryPath = os.path.dirname(cfilename) # force new config directory path

            self.initDecoSeq()
            self.update_status('status', f"SUCCESS - Configuration loaded")
            return 0


        # Update working directory paths 
        #---------------------------------
        self.saveDirectoryPath = os.path.dirname(cfilename)
        self.seqDirectoryPath = self.saveDirectoryPath
        self.figDirectoryPath = self.saveDirectoryPath
        if self.configDirectoryPath == '':				# Update config directory only if not defined yet
          self.configDirectoryPath = self.saveDirectoryPath


       
        # Restore Sequences
        #---------------------------------

        # change the working directory to the location where the saved project is located. 
        os.chdir(os.path.dirname(cfilename))

        seqfileDict = {}

        lineI=cfile.readline().rstrip()
        while not lineI.startswith('#FIGURE'):
          # collect params in a dictionary
          allparams = lineI.replace("\tEND", "").split('\t')
          pDict={}
          for param in allparams:
            val=param.split(':')
            pDict[val[0]]=val[1]

          # load sequences from file. If the file has already been loaded, do not load it again.
          if pDict['file'] not in seqfileDict:
            seqfileDict[pDict['file']] = CGeno.parseSeqFile(pDict['file'], "D")

          # Create the sequence entry with basic parameters. 
          # The sequence entry (record) is identified by its sequence file (pDict['file']) and its accession (pDict['name'])
          S=self.addSeq(True, pDict['name'], seqfileDict[pDict['file']][pDict['name']], pDict['file'])

          # change values of the newly created sequence entry, and pass to next one
          self.reload_seq(S, pDict)
          lineI=cfile.readline().rstrip()

        # restore blast "All vs. One" combo list
        self.on_Seqnbchanged()
        self.comboBNOne.set_active_id(goptions["bnseloneID"])
        self.comboBXOne.set_active_id(goptions["bxseloneID"])

        # Restore other file names
        #---------------------------------
        lineI=cfile.readline().rstrip()
        if not lineI.startswith('#BlastN files'): # Output figure file
          self.outputfile=str(lineI) # Figure output file
          self.entryPName.set_text(self.formatfile(self.outputfile))
          lineI=cfile.readline().rstrip()

        lineI=cfile.readline().rstrip()
        while not lineI.startswith('#BlastX files'): # BlastN files
           iter = self.listBlastN.append()
           self.listBlastN.set(iter, [0, 1], [self.on_removeBlastN_clicked, lineI])
           lineI=cfile.readline().rstrip()
        lineI=cfile.readline().rstrip()

        while not lineI.startswith('#BlastN custom'): # BlastX files
           iter = self.listBlastX.append()
           self.listBlastX.set(iter, [0, 1], [self.on_removeBlastX_clicked, lineI])
           lineI=cfile.readline().rstrip()
          
        # Restore custom Blast displays
        #---------------------------------
        lineI=cfile.readline().rstrip() # Skip BlastN custom display header
        self.BNDchecked=ast.literal_eval(lineI)
 
        lineI=cfile.readline().rstrip() 
        lineI=cfile.readline().rstrip() # Skip BlastX custom display header
        self.BXDchecked=ast.literal_eval(lineI)

        lineI=cfile.readline().rstrip()
        if (lineI == '#END'):
            print("file "+cfilename+" loaded successfully\n")
            self.current_working_filename = os.path.basename(cfilename)
            self.set_title('GenoFig '+VERSION + ' ' + self.current_working_filename)
        
        self.update_status('status', f"SUCCESS - Project loaded")

    ##############################################################################################
    # Open credits window
    ##############################################################################################
    def on_Credits(self, a, b):
        credits = CGeno.DialogCredits(parent=self)
        
        response = credits.run()
        
        if response == Gtk.ResponseType.CLOSE:
          credits.destroy()
    
    ##############################################################################################
    # Update status frame content and style
    ##############################################################################################
    def update_status(self, className, message=""):
        self.statusLabel.set_label(message)
        context = self.statusLabel.get_style_context()
        
        try: context.remove_class('status') 
        except: pass
        try: context.remove_class('status_ok') 
        except: pass
        try: context.remove_class('status_warning') 
        except: pass
        try: context.remove_class('status_error') 
        except: pass

        context.add_class(className)

    def update_status_blast(self, btype, message):
        if btype == 'blastn':
            self.statusLabelBlastn.set_label(message)
        elif btype == 'tblastx':
            self.statusLabelBlastx.set_label(message)
        else:
            self.statusLabelBlastx.set_label('')
            self.statusLabelBlastn.set_label('')

        # We check if both are empty. If, then we re-allow create figure button
        if self.statusLabelBlastx.get_label() == '' and self.statusLabelBlastn.get_label() == '':
            self.butRUN.set_sensitive(True)

        if self.statusLabelBlastn.get_label() == '':
            self.buttonBNrun.set_sensitive(True)
        
        if self.statusLabelBlastx.get_label() == '':
            self.buttonBXrun.set_sensitive(True)

    ##############################################################################################
    # Open README window
    ##############################################################################################
    def on_ReadMe(self, a, b):
        Gtk.show_uri_on_window(None, "https://forgemia.inra.fr/public-pgba/genofig#interface", Gdk.CURRENT_TIME)


#==========================================================================================================================
#
# Application context +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#
#==========================================================================================================================

MENU_XML="""
<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <menu id="app-menu">
    <section>
      <item>
        <attribute name="action">win.new</attribute>
        <attribute name="label" translatable="yes">_New</attribute>
    </item>
      <item>
        <attribute name="action">win.open</attribute>
        <attribute name="label" translatable="yes">_Open</attribute>
        <attribute name="accel">&lt;Primary&gt;o</attribute>
    </item>
      <item>
        <attribute name="action">win.saveA</attribute>
        <attribute name="label" translatable="yes">_Save</attribute>
        <attribute name="accel">&lt;Primary&gt;s</attribute>
    </item>
      <item>
        <attribute name="action">win.saveAs</attribute>
        <attribute name="label" translatable="yes">_Save as ...</attribute>
    </item>
    </section>
    <section>
      <item>
        <attribute name="action">win.loadC</attribute>
        <attribute name="label" translatable="yes">_Load Configuration</attribute>
    </item>
      <item>
        <attribute name="action">win.saveC</attribute>
        <attribute name="label" translatable="yes">_Save Configuration</attribute>
    </item>
      <item>
        <attribute name="action">win.saveDC</attribute>
        <attribute name="label" translatable="yes">_Save Default Configuration</attribute>
    </item>
    </section>
    <section>
      <item>
        <attribute name="action">win.readme</attribute>
        <attribute name="label" translatable="yes">_Help</attribute>
        <attribute name="accel">&lt;Primary&gt;h</attribute>
    </item>
      <item>
        <attribute name="action">win.credits</attribute>
        <attribute name="label" translatable="yes">_Credits</attribute>
    </item>
    </section>
    <section>
      <item>
        <attribute name="action">app.quit</attribute>
        <attribute name="label" translatable="yes">_Quit</attribute>
        <attribute name="accel">&lt;Primary&gt;q</attribute>
    </item>
    </section>
  </menu>
</interface>
"""


class Application(Gtk.Application):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="genofig_v" + VERSION,
                         **kwargs)
        self.window = None

    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        # Build the menu
        builder = Gtk.Builder.new_from_string(MENU_XML, -1)
        self.set_app_menu(builder.get_object("app-menu"))
   

    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if not self.window:
         # Windows are associated with the application
         # when the last one is closed the application shuts down
         self.window = AppWindow(application=self, title='GenoFig v' + VERSION)
         self.window.connect('delete-event', self.on_quit)

         self.window.show_all()

        self.window.buttonBNDrevcolmin.hide()
        self.window.buttonBNDrevcolmax.hide()
        self.window.buttonBXDrevcolmin.hide()
        self.window.buttonBXDrevcolmax.hide()
        self.window.on_minmaxBlastframe(self.window.butMinMaxBX, self.window.boxBX)
        self.window.present()
        
#        print("13")
#        self.quit()
        


    def on_quit(self, action, param):
        d = Gtk.MessageDialog(modal=True,
                              buttons=Gtk.ButtonsType.OK_CANCEL)
        d.props.text = 'Are you sure you want to quit?'
        response = d.run()
        d.destroy()

        if response == Gtk.ResponseType.OK:
            self.quit()
        return 1


if __name__ == "__main__":

    app = Application()
    app.run(sys.argv)


