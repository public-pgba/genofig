'''
Copyright 2023 INRAE, Université de Tours
requests at: sebastien.leclercq@inrae.fr

This file is part of GenoFig.

GenoFig is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Genofig is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

A copy of the GNU General Public License is available in the COPYING file. If not, see <https://www.gnu.org/licenses/>

This script uses some code adapted from Easyfig v2.1.1 written by Mitchell Sullivan and licensed under GNU GPLv3 (http://mjsull.github.io/Easyfig/files.html - Legacy versions)
'''

# Interface GTk3+
from gi.repository import Gtk,Gdk,GObject

VERSION="1.1.1"


# Local modules
import Genomodules.ClassesGenofig as CGeno

import Bio
from Bio import SeqRecord
from Bio.SeqFeature import FeatureLocation,SeqFeature
from Bio.SeqUtils import GC, GC_skew

# Other modules
import re
import math
import copy
import statistics

# Conversions format
from cairosvg import svg2png

#################################################################################################################################
#################################################################################################################################
##                   The SVG Class
#################################################################################################################################
#################################################################################################################################
class scalableVectorGraphics:


# Write the SVG headers ###################################
    def __init__(self, height, width, enhanced):
        self.height = height
        self.width = width
        self.enhanced = int(enhanced)
        self.withmargins = 0
        self.out = '<?xml version="1.0"?>\n'
        self.out += '<svg height="%d" width="%d" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n <g style="fill-opacity:1.0; stroke:black; stroke-width:1; font-family:Sans">\n' % (self.height, self.width)

        if self.enhanced:
          self.out += '<filter id="FencyFilter">\n<feGaussianBlur in="SourceAlpha" stdDeviation="2" result="blur"/>\n<feOffset in="blur" dx="4" dy="4" result="offsetBlur"/>\n'
          self.out += '<feSpecularLighting in="blur" surfaceScale="5" specularConstant="1" specularExponent="40" lighting-color="white" result="specOut">\n<fePointLight x="-5000" y="-10000" z="20000"/>\n</feSpecularLighting>\n'
          self.out += '<feComposite in="specOut" in2="SourceAlpha" operator="in" result="specOut"/>\n<feComposite in="SourceGraphic" in2="specOut" operator="arithmetic" k1="0" k2="1" k3="1" k4="0" result="litPaint"/>\n<feMerge>\n<feMergeNode in="offsetBlur"/>\n<feMergeNode in="litPaint"/>\n</feMerge>\n</filter>\n'

# Define hachtings which will be in use #############################

    def sethatching(self,foptions):
       self.out += '<defs>\n'

       for i in range(len(foptions)):
         # Get the hatching stroke size
         swidth="1"
         if foptions[i]['stroke'] != "": swidth=foptions[i]['stroke']

         # Horizontal Bar hatching
         if foptions[i]['hatching'] == 'hbar':
          self.out += ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="10" height="10">\n'
          self.out +=  '  <line x1="0" y1="5" x2="10" y2="5" stroke-width="%s" stroke="%s" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Horizontal Bar hatching
         if foptions[i]['hatching'] == 'hbar2':
          self.out += ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="10" height="15">\n'
          self.out +=  '  <line x1="0" y1="5" x2="10" y2="5" stroke-width="%s" stroke="%s" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Open circle hatching
         if foptions[i]['hatching'] == 'circle':
          self.out += ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="50" height="50">\n'
          self.out +=  '  <circle cx="0" cy="0" r="12" stroke-width="%s" stroke="%s" fill="none" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '  <circle cx="0" cy="50" r="12" stroke-width="%s" stroke="%s" fill="none" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '  <circle cx="50" cy="50" r="12" stroke-width="%s" stroke="%s" fill="none" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '  <circle cx="50" cy="0" r="12" stroke-width="%s" stroke="%s" fill="none" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '  <circle cx="25" cy="25" r="12" stroke-width="%s" stroke="%s" fill="none" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Crossed hatching
         if foptions[i]['hatching'] == 'crossed':
          self.out += ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">\n'
          self.out +=  '<line x1="0" y1="0" x2="20" y2="20" stroke-width="%s" stroke="%s" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<line x1="0" y1="20" x2="20" y2="0" stroke-width="%s" stroke="%s" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Crossed hatching large
         if foptions[i]['hatching'] == 'crossed2':
          self.out += ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="40" height="40">\n'
          self.out +=  '<line x1="0" y1="0" x2="40" y2="40" stroke-width="%s" stroke="%s" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<line x1="0" y1="40" x2="40" y2="0" stroke-width="%s" stroke="%s" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Rain hatching
         if foptions[i]['hatching'] == 'rain':
          self.out +=  ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">\n'
          self.out +=  '<line x1="5" y1="15" x2="15" y2="5" stroke-width="%s" stroke="%s" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Left Diagonal bars
         if foptions[i]['hatching'] == 'diagonal':
          self.out +=  ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">\n'
          self.out +=  '<line x1="0" y1="-10" x2="30" y2="20" stroke-width="%s" stroke="%s" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<line x1="-10" y1="0" x2="20" y2="30" stroke-width="%s" stroke="%s" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Right Diagonal bars
         if foptions[i]['hatching'] == 'Rdiagonal':
          self.out +=  ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">\n'
          self.out +=  '<line x1="20" y1="-10" x2="-10" y2="20" stroke-width="%s" stroke="%s" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<line x1="30" y1="0" x2="0" y2="30" stroke-width="%s" stroke="%s" />\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Waves
         if foptions[i]['hatching'] == 'waved':
          self.out +=  ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="20" height="15">\n'
          self.out +=  '<polyline points="-5,0  5,10  10,5 15,10 25,0" style="stroke-width:%s; stroke:%s; fill:none;"/>\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 

         # Dots
         if foptions[i]['hatching'] == 'dotted':
          self.out +=  ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="10" height="10">\n'
          self.out +=  '<polyline points="4,4  4,6 6,6 6,4" style="stroke-width:%s; stroke:%s; fill:%s;"/>\n </pattern>\n' % (swidth,foptions[i]['strokecol'],foptions[i]['strokecol']) 

         # Small crosses
         if foptions[i]['hatching'] == 'crosses':

          self.out +=  ' <pattern id="Feat'+str(i)+'"  patternUnits="userSpaceOnUse" x="0" y="0" width="30" height="30">\n'
          self.out +=  '<line x1="10" y1="15" x2="20" y2="15" stroke-width="%s" stroke="%s" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<line x1="15" y1="10" x2="15" y2="20" stroke-width="%s" stroke="%s" />\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<polyline points="0,5  0,0  5,0" style="stroke-width:%s; stroke:%s; fill:none;"/>\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<polyline points="0,25  0,30  5,30" style="stroke-width:%s; stroke:%s; fill:none;"/>\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<polyline points="25,0 30,0 30,5" style="stroke-width:%s; stroke:%s; fill:none;"/>\n' % (swidth,foptions[i]['strokecol']) 
          self.out +=  '<polyline points="25,30 30,30 30,25" style="stroke-width:%s; stroke:%s; fill:none;"/>\n </pattern>\n' % (swidth,foptions[i]['strokecol']) 



       self.out += '</defs>\n'

# Add Graphical enhancement ########################################

    def startImproveG(self):
        if self.enhanced: self.out += '<g filter="url(#FencyFilter)" >\n' 

    def endImproveG(self):
        if self.enhanced: self.out += '</g>\n'


# Add a background color ############################################
    def setbackground(self,bgcolor):
        self.out += '<rect x="0" y="0" height="%d" width="%d" fill="%s" stroke="none"/>\n' % (self.height, self.width,bgcolor)

# Put margins ########################################
    def setmargins(self, tmargin=0, lmargin=0):
        if tmargin or lmargin:
            self.withmargins = 1
            self.out += '  <g transform="translate(%d,%d)">\n' % (lmargin,tmargin)

# Draw the sequence baseline ########################################
    def drawLine(self, x, y, size, lwidth='1', lcol='#000000'):
        x2 = x + size
        self.out += '  <line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%s" stroke="%s" />\n' % (x, y, x2, y, lwidth, lcol)

# Ends the SVG and write it in a file ###############################
    def writesvg(self, filename, format='svg'):
        if self.withmargins:
          self.out += '  </g>'
          self.withmargins = 0

        self.out += ' </g>\n</svg>'

        if format == 'svg':
            outfile = open(filename, 'w')
            outfile.write(self.out)
            outfile.close()
        else:
            svg2png(bytestring=self.out,write_to=filename)


# Return table of GC skew/% using step and window ###############################
    def calculate_GC(self, method, seq, step, window):
        table = []

        for i in range(0, len(seq.record.seq), step):

            # Determine start/stop of region using step and window
            ## If start is less than 0, then use 0
            ## If end is more than seq len, use seq len
            start = int(i - window/2)
            if start < 0:
                start = 0
            end = int(i + window/2)
            if end > len(seq.record.seq):
                end = len(seq.record.seq)

            meanpos = min(i+(step/2),len(seq.record.seq))
            seq_region = seq.record.seq[start : end]

            if method == "skew":
                g = seq_region.count("G") + seq_region.count("g")
                c = seq_region.count("C") + seq_region.count("c")
                try:
                    value = round((g - c) / float(g + c), 2)
                except ZeroDivisionError:
                    value = 0.0

            if method == "percent":
                value = GC(seq_region)
                
            table.append([meanpos, value])
        return table


# Draw GC % and GC skew graphs ###############################
    def drawGCtype(self, x, y, scale, length, step, table, height, doption, deco, seqsize, reverse, GCtype, opacity):
        zeroerror=False
        values = [i[1] for i in table]
        middleval = min(values)+(max(values)-min(values))/2
        yscale = (height, (max(values)-min(values))/2)
    
        x1 = y1 = ""
        polygon_values = f"{x},{y} "
        
        for pos, value in table:
            try:  
               scaled_value = yscale[0] * (middleval-value)/yscale[1]
           # Capture a division by zero when there is only one value calculated (yscale[1]=0). It occurs usually when the step/window is larger than the sequence length
            except ZeroDivisionError:
               scaled_value = 0
               zeroerror=True
            
            x1 = x + pos * scale
            if reverse:
                y1 = y - scaled_value
            else:
                y1 = y + scaled_value

            polygon_values += f"{x1},{y1} "

        polygon_values += f"{x + seqsize * scale},{y}"
        
        # Draw as a polyline, and add a thin straight middle line
        self.out += f'<polyline points="{polygon_values}" stroke="{doption["linecolor"]}" stroke-width="{doption["linewidth"]}" fill="{doption["linecolor"]}" fill-opacity="{opacity}"/>'
        self.out += f'<line x1="{x}" y1="{y}" x2="{x + seqsize * scale}" y2="{y}" stroke="{doption["linecolor"]}" stroke-width="1"/>'   
        
        # prepare label position according to user's choice
        if doption["labelprint"]:
            if reverse:
                ymin = y - int(height)
                ymax = y + int(height)
            else:
                ymin = y + int(height)
                ymax = y - int(height)

            if doption["labelpos"] == "left":
                meanlab =  GCtype + "   " + str(round(middleval,2))
                xlabel = x - 25
                xtick  =  x - 10
                anch = "end"
            else:
                meanlab= str(round(middleval,2)) + "   " + GCtype
                xlabel = x + seqsize * scale + 25
                xtick = x + seqsize * scale + 10
                anch = "start"

            # print label ticks
            self.out += '<line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%d" stroke="%s" />\n' % (xtick-5, y, xtick+5, y, int(doption["linewidth"]), doption["labelcolor"])
            self.out += '<line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%d" stroke="%s" />\n' % (xtick-5, ymax, xtick+5, ymax, int(doption["linewidth"]), doption["labelcolor"])
            self.out += '<line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%d" stroke="%s" />\n' % (xtick-5, ymin, xtick+5, ymin, int(doption["linewidth"]), doption["labelcolor"])
            
            # print label values
            self.drawElementLabel(xlabel, y+(int(doption['labelsize'])*0.4), meanlab, doption['labelcolor'], doption['labelsize'], 0, anchor=anch)
            self.drawElementLabel(xlabel, ymax+(int(doption['labelsize'])*0.4), str(round(max(values),2)), doption['labelcolor'], doption['labelsize'], 0, anchor=anch)
            self.drawElementLabel(xlabel, ymin+(int(doption['labelsize'])*0.4), str(round(min(values),2)), doption['labelcolor'], doption['labelsize'], 0, anchor=anch)

        # Capture and raise division by zero error without stopping the calculation
        if zeroerror: raise ZeroDivisionError()

   
# Draw Sequence-specific scales ###############################
    def drawDecoScale(self, x, y, scale, height, doption, seqsize, reverse, slabel, elabel, seqrevfactor):
        x1 = x
        y1 = y2 = y
        x2 = x + seqsize * scale
        justify = "middle"
        if doption["labelpos"] == "left": justify = "end"
        if doption["labelpos"] == "right": justify = "start"
     
        # variables used to correctly print position labels when the sacle is reversed
        revfactor=1
        laboffset=0
        if reverse: 
          revfactor=-1
          laboffset=int(doption['labelsize'])

        # Main Line
        self.out += '  <line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%s" stroke="%s" />\n' % (x1, y1, x2, y2, doption["linewidth"], doption["linecolor"])

        # Left end
        self.out += '  <line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%s" stroke="%s" />\n' % (x1, y - height/2, x1, y + height/2, doption["linewidth"], doption["linecolor"])
        
        # Right end
        self.out += '  <line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%s" stroke="%s" />\n' % (x2, y - height/2, x2, y + height/2, doption["linewidth"], doption["linecolor"])

        if doption["labelprint"]:
            self.drawElementLabel(x1, y - (height/1.5)*revfactor + laboffset, str(slabel), doption['labelcolor'], doption['labelsize'], 0, anchor=justify)
            self.drawElementLabel(x2, y - (height/1.5)*revfactor + laboffset, str(elabel), doption['labelcolor'], doption['labelsize'], 0, anchor=justify)

        # draw minor ticks
        for i in range(0, seqsize, doption["step"]):
            if i == 0:
                continue
            y2 = y - (height/4)*revfactor
            x1 = x2 = x + i * scale
            self.out += '  <line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%s" stroke="%s" />\n' % (x1, y1, x2, y2, doption["linewidth"], doption["linecolor"])

        # draw major ticks
        for i in range(0, seqsize, doption["window"]):
            if i == 0:
                continue
            y2 = y - (height/2)*revfactor
            x1 = x2 = x + i * scale
            self.out += '  <line x1="%d" y1="%d" x2="%d" y2="%d" stroke-width="%s" stroke="%s" />\n' % (x1, y1, x2, y2, doption["linewidth"], doption["linecolor"])
            # print label on each major tick 
            if doption["labelprint"]:
               wlabel = slabel + (i*seqrevfactor)
               self.drawElementLabel(x2, y - (height/1.5)*revfactor + laboffset, str(wlabel), doption['labelcolor'], doption['labelsize'], 0, anchor=justify)
            


# Main function for selecting the type of drawing for Genbank features
######################################################################
    def drawElement(self, x, y, location, height, feat, foptions,featnb): 
        # define whether the element have to be filled or not
        if foptions['fill'] == 1: 
            fill = foptions['col']
        else: 
            fill = 'none'

        # define the hatching to use if any
        if foptions['hatching'] != 'none': 
            hatch = "url(#Feat"+str(featnb)+")"

        if foptions['shape'] == 'rectangle':
            self.drawOutRect(x+location.nofuzzy_start, y, len(location), height, fill, foptions['stroke'], foptions['strokecol'])
            if foptions['hatching'] != 'none':
             self.drawOutRect(x+location.nofuzzy_start, y, len(location), height, hatch, foptions['stroke'], foptions['strokecol'])
        elif foptions['shape'] == 'arrow':
            self.drawArrow(x+location.nofuzzy_start, y, len(location), height, feat.strand, fill, foptions['stroke'], foptions['strokecol'])
            if foptions['hatching'] != 'none':
             self.drawArrow(x+location.nofuzzy_start, y, len(location), height, feat.strand, hatch, foptions['stroke'], foptions['strokecol'])
        elif foptions['shape'] == 'signal':
            self.drawSignal(x+location.nofuzzy_start, y, len(location), height, feat.strand, fill, foptions['stroke'], foptions['strokecol'])
            if foptions['hatching'] != 'none':
                self.drawSignal(x+location.nofuzzy_start, y, len(location), height, feat.strand, hatch, foptions['stroke'], foptions['strokecol'])
        elif foptions['shape'] == 'frame':
            self.drawFrame(x+location.nofuzzy_start, y, len(location), height, feat.qualifiers['frame'], feat.strand, fill, foptions['stroke'], foptions['strokecol'])
            if foptions['hatching'] != 'none':
             self.drawFrame(x+location.nofuzzy_start, y, len(location), height, feat.qualifiers['frame'], feat.strand, hatch, foptions['stroke'], foptions['strokecol'])
        elif foptions['shape'] == 'range':
            self.drawRange(x+location.nofuzzy_start, y, len(location), height, foptions['stroke'], foptions['strokecol'])
        elif foptions['shape'] == 'rangeL':
            self.drawRange(x+location.nofuzzy_start, y, len(location), height, foptions['stroke'], foptions['strokecol'], down=1)
        else:
            self.drawOutRect(x+location.nofuzzy_start, y, len(location), height, fill, foptions['stroke'], foptions['strokecol'])
            if foptions['hatching'] != 'none':
             self.drawOutRect(x+location.nofuzzy_start, y, len(location), height, hatch, foptions['stroke'], foptions['strokecol'])

# Draw an element as a rectangle
##########################################################
    def drawOutRect(self, x1, y1, wid, hei, col, lt='1',oc='#000000'):
        if lt == '': lt='1'
        self.out += '  <rect fill="%s" stroke="%s" stroke-width="%s"\n' % (col, oc, lt)
        self.out += '        x="%d" y="%d" width="%d" height="%d" />\n' % (x1, y1, wid, hei)

# Draw an element as an arrow
##########################################################
    def drawArrow(self, x, y, wid, ht, orient, col, lt='1',oc='#000000'):
        if lt == '': lt='1'

        x1 = x + wid
        y1 = y + ht/2
        x2 = x + wid - ht / 2
        ht -= 1

        if orient == -1:
            self.out += ' <g transform="rotate(180,%d,%d)">\n' % (x+wid/2, y+ht/2)
  
        if wid > ht/2:
            self.out += '  <polygon fill="%s" stroke="%s" stroke-width="%s"\n' % (col, oc, lt)
            self.out += '           points="%d,%d %d,%d %d,%d %d,%d %d,%d %d,%d %d,%d" />\n' % (x, y+ht/4, x2, y+ht/4,
                                                                                                x2, y, x1, y1, x2, y+ht,
                                                                                                x2, y+3*ht/4, x, y+3*ht/4)
        else:
            self.out += '  <polygon fill="%s" stroke="%s" stroke-width="%s"\n' % (col, oc, lt)
            self.out += '           points="%d,%d %d,%d %d,%d" />\n' % (x, y, x, y+ht, x + wid, y1)

        if orient == -1:
            self.out += ' </g>\n'


# Draw an element as a signal arrow
##########################################################
    def drawSignal(self, x, y, wid, ht, orient, col, lt='1',oc='#000000'):
        if lt == '': lt='1'

        x1 = x + wid
        x2 = x + wid - ht / 2
        y1 = y + ht/2
        y2 = y + ht
    
        if orient == -1:
            self.out += ' <g transform="rotate(180,%d,%d)">\n' % (x+wid/2, y+ht/2)

        if wid > ht/2:
            self.out += '  <polygon fill="%s" stroke="%s" stroke-width="%s"\n' % (col, oc, lt)
            self.out += '           points="%d,%d %d,%d %d,%d %d,%d %d,%d" />\n' % (x, y, x2, y, x1, y1, x2, y2, x, y2)
        else:
            self.out += '  <polygon fill="%s" stroke="%s" stroke-width="%s"\n' % (col, oc, lt)
            self.out += '           points="%d,%d %d,%d %d,%d" />\n' % (x, y, x, y+ht, x + wid, y1)

        if orient == -1:
            self.out += ' </g>\n'

# Draw an element as a frame
##########################################################
    def drawFrame(self, x, y, wid, ht, frame, orient, col, lt='1',oc ='#000000'):
        if lt == '': lt='1'     

        if frame == 1:
            y1 = y + ht * 1/2
            y2 = y + ht * 3/8
            y3 = y + ht * 1/4
        elif frame == 2:
            y1 = y + ht * 3/8
            y2 = y + ht * 1/4
            y3 = y + ht * 1/8
        elif frame == 0:
            y1 = y + ht * 1/4
            y2 = y + ht * 1/8
            y3 = y + 1
        x1 = x
        x2 = x + wid - ht/8
        x3 = x + wid

        if orient == -1:
            self.out += ' <g transform="rotate(180,%d,%d)">\n' % (x+wid/2, y+ht/2)

        if wid > ht/8:
            self.out += '  <polygon fill="%s" stroke="%s" stroke-width="%s"\n' % (col, oc, lt)
            self.out += '           points="%d,%d %d,%d %d,%d %d,%d %d,%d" />\n' % (x1, y1, x2, y1, x3, y2, x2, y3, x1, y3)
        else:
            self.out += '  <polygon fill="%s" stroke="%s" stroke-width="%s"\n' % (col, oc, lt)
            self.out += '           points="%d,%d %d,%d %d,%d" />\n' % (x1, y1, x3, y2, x1, y3)

        if orient == -1:
            self.out += ' </g>\n'
 

# Draw an element as a upper or lower range
##########################################################
    def drawRange(self,x, y, wid, ht, lt='1',oc ='#000000',down=0):

        if lt == '': lt='1'     
        x2 = x+wid
        o = int(ht/10)

        if down == 1: 
            y  = y+ht
            o = -o
        y2 = y+o

        self.out += '  <polyline points="%d,%d %d,%d %d,%d %d,%d" style="stroke-width:%s; stroke:%s; fill:none;"/>\n' % (x,y2,x,y,x2,y,x2,y2,lt,oc)

# Draw the Sequence Label
##########################################################
    def drawSeqLabel(self,ident,x,y,lj,col,fontsize, fstyle='normal', fweight='normal'):

        # Start the text formating
        self.out += '  <text xml:space="preserve" x="%s" y="%s" ' % (x,y) 
        self.out += 'style="stroke:none;font-size:%spx;font-weight:%s;font-style:%s;;fill:%s" text-anchor="%s"' % (fontsize,fstyle,fweight,col,lj)
        self.out += '>' + str(ident) + '</text>\n'


# Draw the Sequence Size label
##########################################################
    def drawSizeMarker(self, valstr, x, y, l, lw, col, fsize):
        
        # Select the offset to not draw on the size label
        ofs = (l/2) + 1.1*len(valstr)*int(fsize)/2
        labofs = int(fsize)*0.75/2

        # Draw the left part of the line 
        self.out += '  <polyline points="%d,%d %d,%d %d,%d %d,%d" style="stroke-width:%s; stroke:%s; fill:none;"/>\n' % (x,y-labofs,x,y+labofs,x,y,x+l-ofs,y,lw,col)

        # Draw the right part of the line 
        self.out += '  <polyline points="%d,%d %d,%d %d,%d %d,%d" style="stroke-width:%s; stroke:%s; fill:none;"/>\n' % (x+l,y-labofs,x+l,y+labofs,x+l,y,x+ofs,y,lw,col)

        # Draw the size label in the middle
        self.out += '  <text x="%s" y="%s" ' % (x+l/2,y + labofs) 
        self.out += 'style="stroke:none;font-size:%spx;font-weight:normal;fill:%s" text-anchor="middle"' % (fsize,col)
        self.out += '>' + valstr + '</text>\n'


# Main function for drawing labels
######################################################################
    def drawElementLabel(self, x, y, label, col, fontsize, rot=0, rotdir=0,anchor="middle", fweight='normal', fstyle='normal'):
        
        # Start the text formating
        self.out += '  <text  xml:space="preserve" x="%s" y="%s" ' % (x,y) 
        self.out += '    style="stroke:none;font-size:%spx;font-weight:%s;font-style:%s;fill:%s" ' % (fontsize,fweight,fstyle,col)


        # Rotation formating or center if no rotation
        if rot != 0:
          rot = rot % 360 # to manage negative and > 360 deg angles
          if rot > 180: # to avoid overlap the sequence display
            rot = 360 - rot          
          if rot > 90:  # to always write toward the element
            rot = rot - 180
            self.out += 'text-anchor="end"  '
          self.out += 'transform="rotate(%d,%s,%s)"' % (rot*rotdir,x,y) 
        else:
          self.out += 'text-anchor="'+ anchor + '"'

        self.out += '>' + label + '</text>\n'


# Draw a Blast Hit
##########################################################
    def drawBlastHit(self, x1, y1, x2, y2, x3, y3, x4, y4, fill="#aaaaaa", op="1", lt=0):
        self.out += '  <polygon fill="%s" stroke="%s" stroke-width="%d" opacity="%s"\n' % (fill, "#000000",lt,op)
        self.out += '           points="%d,%d %d,%d %d,%d %d,%d" />\n' % (x1, y1, x2, y2, x4, y4, x3, y3)



# End figure translation
##########################################################
    def endMargins(self):
        if self.withmargins:
          self.out += '  </g>'
          self.withmargins = 0



# Draw Scale Legend
##########################################################
    def drawScaleLegend(self,x,y,length,label,fsize,fcol):
        # start position translation
        self.out += '  <g transform="translate(%d,%d)">' % (x,y)

        # Draw the label
        self.drawSeqLabel(label,length/2,fsize,'middle',fcol,fsize,'bold','normal')          


        # Draw the left part of the line 
        fsize2= int(fsize)*2
        labofs = int(fsize)*0.75/2
        self.out += '  <polyline points="%d,%d %d,%d %d,%d %d,%d %d,%d %d,%d" style="stroke-width:%s; stroke:%s; fill:none;"/>\n' % (0,fsize2-labofs, 0,fsize2+labofs, 0,fsize2, length,fsize2, length,fsize2-labofs, length,fsize2+labofs  ,fsize2/20,fcol)


        # end of position translation
        self.out += '  </g>'


# Define Blast Legend
##########################################################
    def drawBlastLegend(self,x,y,hgrad,wgrad,colmin,colmax,invset,col2min,col2max,minident,maxident,fsize,fcol,op,btype):
        # start position translation
        self.out += '  <g transform="translate(%d,%d)">' % (x,y)

        # draw gradients
        self.drawGradient(0, 0, wgrad, hgrad, colmax, colmin, op, btype)
        if invset:  
          btype = f"{btype}R"
          self.drawGradient(wgrad, 0, wgrad, hgrad, col2max, col2min, op, btype)

        # put blast values
        self.drawSeqLabel(maxident,-5,fsize,'end',fcol,fsize)
        self.drawSeqLabel(minident,-5,hgrad,'end',fcol,fsize)

        # end of position translation
        self.out += '  </g>'

# Draw a Blast gradient
##########################################################
    def drawGradient(self, x, y, wid, hei, minc, maxc, op=1, btype="N"):
        self.out += '  <defs>\n    <linearGradient id="BlastGradient%s" x1="0%%" y1="0%%" x2="0%%" y2="100%%">\n' % btype
        self.out += '      <stop offset="0%%" stop-color="%s" />\n' % minc
        self.out += '      <stop offset="100%%" stop-color="%s" />\n' % maxc
        self.out += '    </linearGradient>\n  </defs>\n'
        self.out += '  <rect fill="url(#BlastGradient%s)" stroke-width="1" opacity="%s" \n' % (btype, op)
        self.out += '        x="%d" y="%d" width="%d" height="%d"/>\n' % (x, y, wid, hei)
        if float(op) < 1:
         self.out += '  <rect fill="none" stroke-width="1" \n'
         self.out += '        x="%d" y="%d" width="%d" height="%d"/>\n' % (x, y, wid, hei)
  


# Decompose a long text line into several lines ###################################
def addNewLines(string,lmax):
  nblines = 1
  words  = string.split()
 
  if len(words) < 2:
   return (nblines,string)

  result = words[0]
  size=len(words[0])

  # Adding word after word
  for i in range(1,len(words)):
    # Add a newline instead of a space if the size limit is overcomed by the new word
    if size+len(words[i]) > lmax:
      nblines += 1
      result += '\n'
      size = len(words[i])
    else:
      result += ' '
      size += 1 + len(words[i])
    result += words[i]

  return (nblines,result)




#################################################################################################################################
#################################################################################################################################
##                   The drawing command
#################################################################################################################################
#################################################################################################################################
def drawsvg(outfile,sequences,foptions,doptions,blastNfiles,blastXfiles,options,customblastsN,customblastsX,customDecoSeq):
    RETURNVALUE = 101

    figwidth  = int(options['figwidth'])
    tmargin   = int(options['tmargin'])
    bmargin   = int(options['bmargin'])
    lmargin   = int(options['lmargin'])
    rmargin   = int(options['rmargin'])

    minBident = 100
    maxBident = 0
    minBXident = 100
    maxBXident = 0

    # Copy the sequence information because they will be modified in this function
    figwidth -= lmargin+rmargin

    figheight = 500

    # Basic Sequence pre-treatments, done in reverse order to remove safely unselected sequences from the custom blast tables
    #---------------------------------------------------------------------
    sizemax = 0
    i = len(sequences)-1
    while i >= 0:

        # collect strain information, which can be lost in the process of sequence transformation
        if len(sequences[i].record.features) > 0 and "strain" in sequences[i].record.features[0].qualifiers.keys(): 
            sequences[i].gp['strain'] = " " + sequences[i].record.features[0].qualifiers["strain"][0]
        else:
            sequences[i].gp['strain'] = " unknown strain"
        

        # don't display not selected sequences and remove them from the custom blast tables and decoration custom (lines and columns)
        if sequences[i].gp['sel'] == 0:
          del sequences[i]
          for j in range(i+1,len(customblastsN)):
             del customblastsN[j][i]
             del customblastsX[j][i]
          del customblastsN[i]
          del customblastsX[i]
          i=i-1
          continue
      
        # set the values for the parameters as default
        if sequences[i].gp['height'] == '-': sequences[i].gp['height'] = options['fheight']
        if sequences[i].gp['inspace'] == '-': sequences[i].gp['inspace'] = options['inspace']
        if sequences[i].gp['spos'] == '-': sequences[i].gp['spos'] = options['spos']

        # transform strings into values 
        if sequences[i].gp['max'] == 'max' : sequences[i].gp['max'] = sequences[i].length
        else: sequences[i].gp['max'] = int(sequences[i].gp['max'])
        if sequences[i].gp['min'] == '1' : sequences[i].gp['min'] = 0
        else: sequences[i].gp['min'] = int(sequences[i].gp['min'])


        # divide feature appearing at the beginning and end of the sequence in circular replicons (join keyword)
        #----------------------------------------------------------------------------------------------------------------------
        for f in sequences[i].record.features: 
         # only for features overlaping the sequence start
         if f.location.start > 0: break
         # only for joined features
         if f.location_operator == "join":
           # copy the orginal feature with compound locations, then separate it in two features with simple locations.
           tmpfeat=copy.deepcopy(f)
           tmpfeat.location=f.location.parts[1]
           f.location=f.location.parts[0]
           sequences[i].record.features.append(tmpfeat)

        # apply sequence transformation if necessary (subsequence and reverse)
        #----------------------------------------------------------------------------------------------------------------------
        if (sequences[i].gp['min'] > 0 or sequences[i].gp['max'] < sequences[i].length or sequences[i].gp['rev'] != 0):

          maxtemp = sequences[i].gp['max']
          mintemp = sequences[i].gp['min']

          # apply transformation on sequence length
          sequences[i].length=maxtemp-mintemp+1

          # apply transformation on features
          tmprec=sequences[i].record[int(mintemp):maxtemp]

          # add incomplete features (cut by the subsequence selection)
          for f in sequences[i].record.features:
             # located at the beginning 
             if mintemp in f.location:
               tmprec.features.insert(0,f._shift(-mintemp)) 
               try:    tmprec.features[0].location = FeatureLocation(0, tmprec.features[0].location.end,f.strand)
               except: tmprec.features[0].location = FeatureLocation(0, tmprec.features[0].location.end) # previous version of Biopython


             # located at the end 
             if maxtemp in f.location:
               tmprec.features.append(f._shift(-mintemp))
               try:    tmprec.features[-1].location = FeatureLocation(tmprec.features[-1].location.start, maxtemp-mintemp,f.strand)
               except: tmprec.features[-1].location = FeatureLocation(tmprec.features[-1].location.start, maxtemp-mintemp) # previous version of Biopython

          # Apply reverse complement
          if sequences[i].gp['rev'] != 0:
           tmprec=tmprec.reverse_complement()

          # save the general annotations, description and name, and discard the original sequence
          tmprec.annotations = sequences[i].record.annotations.copy()
          tmprec.description = sequences[i].record.description
          tmprec.name = sequences[i].record.name
          sequences[i].record=tmprec


       
        # define the longest displayed sequence
        if sequences[i].length > sizemax : sizemax = sequences[i].length


        # decrement the while function
        i=i-1

    # save the sequence position for each sequence name (used for blast homology display)
    # done when all not-selected sequences are removed
    # One sequence can be at several positions in the figure (for self comparisons homologies) 
    seqh = {}
    for i in range(len(sequences)): 
      if sequences[i].bname not in seqh.keys(): seqh[sequences[i].bname]=[i]
      else: seqh[sequences[i].bname].append(i)

    if len(sequences) < 1:
        return("NoSeq")
    
    # BlastN hits pre-treatments if required
    #---------------------------------------------------------------------
    if options['bnsel'] and len(blastNfiles) > 0:
     BNtable = CGeno.BlastTable(len(sequences),seqh,options['bnminl'],options['bnmaxe'],options['bnmins'])

     # read blast hits
     try:
      for bfile in blastNfiles:
        # open and parse the file
        BNtable.readBlast(bfile)
     except:
        return 46

     # correct non-symmetrical blast files
     BNtable.mirroring()

     # remove and reverse hits according to subsequences selection, reversing, and blast display
     for i in range(len(sequences)-1):
      for j in range(i+1,len(sequences)):

       # Select blast hits which have to be printed
       toprint=0
       # Blast hits between adjacent sequences
       if options['bnsel'] == 1 and j == i+1: toprint=1
       # All blast hits
       if options['bnsel'] == 2: toprint=1
       # Blast hits between the selected sequence and all the other ones; bnseloneID needs to be decreased by one, 
       # because the index also includes the 'general' line from the GUI Sequence list Seqlist, not present in the current sequences list
       if options['bnsel'] == 3 and (int(options['bnseloneID'])-1 == i or int(options['bnseloneID'])-1 == j): toprint=1
       # Custom blast hits -> get transposed values
       if options['bnsel'] == 4 and customblastsN[j][i]: toprint=1

       if (toprint == 1):
         BNtable.filterHits(i,j,sequences[i].gp['min'],sequences[i].gp['max'],sequences[j].gp['min'],sequences[j].gp['max'])
         BNtable.reverseHits(i,j,sequences[i].gp['min'],sequences[i].gp['max'],sequences[i].gp['rev'],sequences[j].gp['min'],sequences[j].gp['max'],sequences[j].gp['rev'])

       # Else remove blast hits for this pair
       else:
         BNtable.removehits(i,j)

    # tBlastX hits pre-treatments if required
    #---------------------------------------------------------------------
    if options['bxsel'] and len(blastXfiles) > 0:
     BXtable = CGeno.BlastTable(len(sequences),seqh,int(options['bxminl'])/3,options['bxmaxe'],options['bxmins'])

     # read blast hits
     try:
      for bfile in blastXfiles:
        # open and parse the file
        BXtable.readBlast(bfile)
     except:
         return 47

     # correct non-symmetrical blast files
     BXtable.mirroring()

     # remove and reverse hits according to subsequences selection, reversing, and blast display
     for i in range(len(sequences)-1):
      for j in range(i+1,len(sequences)):

       # Select blast hits which have to be printed
       toprint=0
       # Blast hits between adjacent sequences
       if options['bxsel'] == 1 and j == i+1: toprint=1
       # All blast hits
       if options['bxsel'] == 2: toprint=1
       # Blast hits between the selected sequence and all the other ones; bnseloneID needs to be decreased by one, 
       # because the index also includes the 'general' line from the GUI Sequence list Seqlist, not present in the current sequences list
       if options['bxsel'] == 3 and (int(options['bxseloneID'])-1 == i or int(options['bxseloneID'])-1 == j): toprint=1
       # Custom blast hits -> get transposed values
       if options['bxsel'] == 4 and customblastsX[j][i]: toprint=1

       if (toprint == 1):
         BXtable.filterHits(i,j,sequences[i].gp['min'],sequences[i].gp['max'],sequences[j].gp['min'],sequences[j].gp['max'])

         # WARNING ! this mergeBXhit method disrupt the integrity of hits. Especially the mismatches and gap counts are not reliable anymore, and the identity is only an approximation.
         if (options['bxmerge']): BXtable.mergeBXHits(i,j)

         BXtable.reverseHits(i,j,sequences[i].gp['min'],sequences[i].gp['max'],sequences[i].gp['rev'],sequences[j].gp['min'],sequences[j].gp['max'],sequences[j].gp['rev'])


       # Else remove blast hits for this pair
       else:
         BXtable.removehits(i,j)


    # Features values pre-treatment
    #---------------------------------------------------------------------
    i = 0
    while i < len(foptions):
       # remove not selected features
       if foptions[i]['sel'] == 0:
          del foptions[i]
          continue

       if foptions[i]['height'] == '-': foptions[i]['height'] = options['ftheight']
       if foptions[i]['strand'] == '-': foptions[i]['strand'] = options['fstrand']
       if foptions[i]['shape'] == '-': foptions[i]['shape'] = options['fshape']
       if foptions[i]['hatching'] == '-': foptions[i]['hatching'] = options['fhatching']
       if foptions[i]['col'] == None: foptions[i]['col'] = options['fcol']
       if foptions[i]['stroke'] == '-': foptions[i]['stroke'] = options['fstroke']
       if foptions[i]['strokecol'] == None: foptions[i]['strokecol'] = options['fstrokecol']

       # general selection for bold and italic style for feature labels
       if options['flabbold']: foptions[i]['flabbold']=True
       if options['flabital']: foptions[i]['flabital']=True

       # increment the while function
       i=i+1

    # Decoration values pre-treatment
    #---------------------------------------------------------------------
    i = 0
    while i < len(doptions):
       # remove not selected decoration
       if doptions[i]['select'] == 0:
          del doptions[i]
          continue

       # set the values for the parameters as default
       if doptions[i]['step'] == '-': doptions[i]['step'] = options['dstep']
       if doptions[i]['window'] == '-': doptions[i]['window'] = options['dwindow']
       if doptions[i]['pos'] == '-': doptions[i]['pos'] = options['dpos']
       if doptions[i]['heightR'] == '-': doptions[i]['heightR'] = options['dheightR']
       if doptions[i]['linewidth'] == '-': doptions[i]['linewidth'] = options['dlinewidth']
       if doptions[i]['linecolor'] == None: doptions[i]['linecolor'] = options['dlinecolor']
       if doptions[i]['labelcolor'] == None: doptions[i]['labelcolor'] = options['dlabelcolor']
       if doptions[i]['labelsize'] == "-": doptions[i]['labelsize'] = options['dlabelsize']
       if doptions[i]['labelpos'] == "-": doptions[i]['labelpos'] = options['dlabelpos']

       # increment the while function
       i=i+1


    #get the scale convertion ratio depending on the larger sequence
    #------------------------------------------------------------------
    scale = float(figwidth) / sizemax


    # Compute the image height (including all elements), and refine ypos for each sequence  
    #-------------------------------------------------------------------------------------
    figheight = tmargin + bmargin

    ypos= int(sequences[0].gp['height'])/2
    for i in range(len(sequences)-1):
       figheight += int(sequences[i].gp['inspace'])

       sequences[i].ypos=ypos
       ypos+=int(sequences[i].gp['inspace'])

    # Do not include the spacer for the last sequence
    figheight += int(sequences[-1].gp['height'])
    sequences[-1].ypos=ypos


    # transform nucleotide size into pixel size using the scale ratio
    #------------------------------------------------------------------
    for i in range(len(sequences)):
        # for each feature in the sequence first
        for feat in sequences[i].record.features:

          ls = int(feat.location.nofuzzy_start * scale)
          le = int(feat.location.nofuzzy_end * scale)
          if ls == le and feat.strand == 1: le+=1 
          if ls == le and feat.strand == -1: ls-=1 

          #save the frame first
          if feat.strand == 1: feat.qualifiers['frame'] = feat.location.nofuzzy_start % 3
          else : feat.qualifiers['frame'] = (sequences[i].length-feat.location.nofuzzy_end) % 3

          strand = feat.strand
          try:    feat.location=FeatureLocation(ls,le,feat.strand) 
          except: feat.location=FeatureLocation(ls,le)    # previous version of Biopython
          feat.strand=strand

        # set the sequence length and position, but save the original length
        sequences[i].lengthO = sequences[i].length
        sequences[i].length = int(sequences[i].length * scale)
        sequences[i].xpos= startPos(sequences[i].xpos, sequences[i].gp['spos'], figwidth, sequences[i].length, sequences[i].gp['sxpos'])



    # Transform BlastN entries
    #------------------------------------------------------------------
    if options['bnsel'] and len(blastNfiles) > 0:
     for i in range(len(sequences)):
       for j in range(len(sequences)):
          for h in BNtable.hits(i,j):
                h['qStart']=h['qStart']* scale
                h['qEnd']=h['qEnd']* scale
                h['rStart']=h['rStart']* scale
                h['rEnd']=h['rEnd']* scale

                # Round the percent id to the number of decimal given in the GUI
                options['bnlabdec']=int(options['bnlabdec'])
                tmpident = round(h['ident'],options['bnlabdec'])
                # Small trick to never display a 100% homology when it is < 100%
                if (tmpident == 100 and h['ident'] < 100):
                 tmpident=100-math.pow(10,-1*options['bnlabdec'])
                h['ident']=tmpident

		# pass in integer for diplay purpose if no decimal is wanted
                if (options['bnlabdec'] == 0): h['ident']=int(h['ident'])

                # get the minimum and maximum blast hits
                if h['ident'] < minBident: minBident = h['ident']
                if h['ident'] > maxBident: maxBident = h['ident']


    # Transform tBlastX entries
    #------------------------------------------------------------------
    if options['bxsel'] and len(blastXfiles) > 0:
     for i in range(len(sequences)):
       for j in range(len(sequences)):
          for h in BXtable.hits(i,j):
                h['qStart']=h['qStart']* scale
                h['qEnd']=h['qEnd']* scale
                h['rStart']=h['rStart']* scale
                h['rEnd']=h['rEnd']* scale

                # Round the percent id to the number of decimal given in the GUI
                options['bxlabdec']=int(options['bxlabdec'])
                tmpident = round(h['ident'],options['bxlabdec'])
                # Small trick to never display a 100% homology when it is < 100%
                if (tmpident == 100 and h['ident'] < 100):
                 tmpident=100-math.pow(10,-1*options['bxlabdec'])
                h['ident']=tmpident

		# pass in integer for diplay purpose if no decimal is wanted
                if (options['bxlabdec'] == 0): h['ident']=int(h['ident'])

                # get the minimum and maximum blast hits
                if h['ident'] < minBXident: minBXident = h['ident']
                if h['ident'] > maxBXident: maxBXident = h['ident']


    # adapt x position for sequences with 'best blast' option => merge blastn and blastp hits together
    #-----------------------------------------------------------------------------------------------------

    # 1. create a table of blast hits incling either blastN or blastX hits, or both
    AllBhits=[]
    if (options['bnsel'] and len(blastNfiles) > 0): AllBhits = AllBhits + BNtable.returnhits()
    if (options['bxsel'] and len(blastXfiles) > 0): AllBhits = AllBhits + BXtable.returnhits()

    if (len(AllBhits) > 0):
    
     # 2. Sort all hits by decreasing size
     #    all future displayed blast hits are mixed, independent of the matched sequences
     sortedBhits=sorted(AllBhits,key=lambda seqpair: seqpair[2])
     sortedBhits.reverse()

     # 3. define a structure indicating if the sequence is correctly positionned
     #    all sequences with an option other than 'best blast' are by definition correctly positionned
     positionned = []
     ambiguous = []
     whichlongest = 0
     for i in range(len(sequences)):
      if sequences[i].length == figwidth and sequences[i].gp['spos'] == 'best blast':
        positionned.append(0)
        whichwidest=i
      elif sequences[i].gp['spos'] == 'best blast': positionned.append(0)
      else: positionned.append(1)
      ambiguous.append(list()) # this will store the possible best matches for a given sequence, when its not defined yet 

     # if all sequences have the option 'best blast', position the longest sequence as correctly positioned
     if 1 not in positionned: positionned[whichlongest] = 1
    
    
     # 4. read the best hits by descending order and change position of 
     #    unpositionned position if possible 
     for sh in sortedBhits:
    
      # first, test that all sequences are not already positionned
      if 0 not in positionned: break

      # pass if both sequences of the current hit are positioned    
      if positionned[sh[0]] and positionned[sh[1]]: continue
    
      # next, correct for reversed matches
      qS = sh[3]['qStart']; rS = sh[3]['rStart']
      if sh[3]['qStart'] < sh[3]['qEnd']: qS = sh[3]['qEnd']
      if sh[3]['rStart'] < sh[3]['rEnd']: rS = sh[3]['rEnd']
       
      # finally, do the positionning
      if positionned[sh[0]] and not positionned[sh[1]]:
          sequences[sh[1]].xpos = sequences[sh[0]].xpos+qS-rS
          positionned[sh[1]] = 1
          resolveambiguous(sh[1],ambiguous,positionned,sequences)
          
      if not positionned[sh[0]] and positionned[sh[1]]:
          sequences[sh[0]].xpos = sequences[sh[1]].xpos+rS-qS
          positionned[sh[0]] = 1
          resolveambiguous(sh[0],ambiguous,positionned,sequences)

      else:
         ambiguous[sh[0]].append(sh)  ## NEED TO IMPLEMENT THIS
         ambiguous[sh[1]].append(sh)     
         

#####################################################################################################
# Start the SVG writing #############################################################################
#####################################################################################################

    # Initialize the svg graphics
    #------------------------------------------------------------------------------------
    svg = scalableVectorGraphics(figheight, int(options['figwidth']),options['enhanced'])
    svg.sethatching(foptions)

    if options['bg']:
      svg.setbackground(options['bgcol'])
    svg.setmargins(tmargin,lmargin)  


    # Draw BlastN hits if selected
    #------------------------------------------------------------------
    if options['bnsel'] and len(blastNfiles) > 0:
     print("Drawing blastn homologies...")
     for i in range(len(sequences)-1):
      for j in range(i+1,len(sequences)):

        # pass to the next sequence pair if no hit are found for this pair (or are not displayed)
        if len(BNtable.hits(i,j)) == 0: continue

        xp1=sequences[i].xpos
        xp2=sequences[j].xpos

        # define hit vertical position
        if options['bnoffset'] == '-': 
            yp1 = sequences[i].ypos + int(sequences[i].gp['height'])
            yp2 = sequences[j].ypos - int(sequences[j].gp['height'])
        else : 
            yp1 = sequences[i].ypos + int(options['bnoffset'])
            yp2 = sequences[j].ypos - int(options['bnoffset'])

        for hit in BNtable.hits(i,j):
         # define hit color according to gradient and orientation
         if options['bncolinvset'] == 1 and (hit['qEnd']-hit['qStart']) * (hit['rEnd']-hit['rStart']) < 0:
            ccol = blastCol(hit['ident'],options['bncolinv'],options['bncolinv2'],minBident,maxBident)
         else:
            ccol = blastCol(hit['ident'],options['bncol'],options['bncol2'],minBident,maxBident)

         # draw the hit
         svg.drawBlastHit(hit['qStart']+xp1,yp1, hit['qEnd']+xp1, yp1, hit['rStart']+xp2, yp2, hit['rEnd']+xp2, yp2, ccol, options['bnopacity'], options['bnoutline']) 

         # print hit label
         if options['bnlabshow'] and options['bnlabset'] not in [""," "] and hit['length'] >= int(options['bnlabset']):
          svg.drawElementLabel((min(hit['qStart'],hit['qEnd'])+xp1+max(hit['rStart'],hit['rEnd'])+xp2)/2, (yp1+yp2)/2, str(hit['ident'])+'%', options['bnlabcol'], options['bnlabsize'])


    # Draw tBlastX hits if selected
    #------------------------------------------------------------------
    if options['bxsel'] and len(blastXfiles) > 0:
     print("Drawing tblastx homologies...")
     for i in range(len(sequences)-1):
      for j in range(i+1,len(sequences)):

        # pass to the next sequence pair if no hit are found for this pair (or are not displayed)
        if len(BXtable.hits(i,j)) == 0: continue

        xp1=sequences[i].xpos
        xp2=sequences[j].xpos

        # define hit vertical position
        if options['bxoffset'] == '-': 
            yp1 = sequences[i].ypos + int(sequences[i].gp['height'])
            yp2 = sequences[j].ypos - int(sequences[j].gp['height'])
        else : 
            yp1 = sequences[i].ypos + int(options['bxoffset'])
            yp2 = sequences[j].ypos - int(options['bxoffset'])

        for hit in BXtable.hits(i,j):
         # define hit color according to gradient and orientation
         if options['bxcolinvset'] == 1 and (hit['qEnd']-hit['qStart']) * (hit['rEnd']-hit['rStart']) < 0:
            ccol = blastCol(hit['ident'],options['bxcolinv'],options['bxcolinv2'],minBXident,maxBXident)
         else:
            ccol = blastCol(hit['ident'],options['bxcol'],options['bxcol2'],minBXident,maxBXident)

         # draw the hit
         svg.drawBlastHit(hit['qStart']+xp1,yp1, hit['qEnd']+xp1, yp1, hit['rStart']+xp2, yp2, hit['rEnd']+xp2, yp2, ccol, options['bxopacity'], options['bxoutline']) 

         # print hit label
         if options['bxlabshow'] and options['bxlabset'] not in [""," "] and hit['length'] >= int(options['bxlabset']):
          svg.drawElementLabel((min(hit['qStart'],hit['qEnd'])+xp1+max(hit['rStart'],hit['rEnd'])+xp2)/2, (yp1+yp2)/2, str(hit['ident'])+'%', options['bxlabcol'], options['bxlabsize'])




    # Draw sequences line
    #------------------------------------------------------------------
    print("Drawing sequences...")
    for s in sequences:
      width =  gsoption(options,'lwidth',s)
      color =  gsoption(options,'lcol',s)
      svg.drawLine(s.xpos, s.ypos, s.length, width, color)

    # Draw sequences decoration
    #------------------------------------------------------------------
    print("Drawing decorations...")
    for s in sequences:

        # Set up values for avoid superposed decorations while above/below (superposition is allowed on sequence)
        ##############
        decoration_top = 0
        decoration_bottom = 0
        for d, deco in enumerate(doptions):

            # Checking if decoration is selected for this sequence using customDecoSeq
            if customDecoSeq[deco['id']][s.gp['ID']] == 1:
                d_step = int(doptions[d]['step'])
                d_window = int(doptions[d]['window'])
                seqsize = len(s.record.seq)
                reverse = doptions[d]['reverse']
                
                # use the sequence height by default, and apply the decoration-specific ratio 
                dheight=sheight=int(s.gp['height'])
                if doptions[d]['heightR'] != '-': dheight *= float(doptions[d]['heightR'])            


                # calcultate position, and avoid decoration superimposition
                if doptions[d]['pos'] == "above":
                    if decoration_top == 0:
                        y = s.ypos - 1.3 * (sheight + dheight)/2
                    else:
                        y = decoration_top - 30 - dheight/2 * 1.2
                    decoration_top = y - dheight/2
                     
                elif doptions[d]['pos'] == "below":
                    if decoration_bottom == 0:
                        y = s.ypos + 1.3 * (sheight + dheight)/2
                    else:
                        y = decoration_bottom + 30 + dheight/2 * 1.2
                    decoration_bottom = y + dheight/2
                else:
                    y = s.ypos

                dheight=dheight/2
                    
                # draw decorum according to its type
                try:
                 if doptions[d]['type'] == "GCskew":
                    skew_table = svg.calculate_GC("skew", s, d_step, d_window)
                    svg.drawGCtype(s.xpos, y, scale, s.length, d_step, skew_table, dheight, doptions[d], d, seqsize, reverse,"GC skew",0)
                
                 if doptions[d]['type'] == "GCskew_fill":
                    skew_table = svg.calculate_GC("skew", s, d_step, d_window)
                    svg.drawGCtype(s.xpos, y, scale, s.length, d_step, skew_table, dheight, doptions[d], d, seqsize, reverse,"GC skew",0.5)
                
                 if doptions[d]['type'] == "GC%":
                    GC_table = svg.calculate_GC("percent", s, d_step, d_window)
                    svg.drawGCtype(s.xpos, y, scale, s.length, d_step, GC_table, dheight, doptions[d], d, seqsize, reverse,"GC %",0)
                
                 if doptions[d]['type'] == "GC%_fill":
                    GC_table = svg.calculate_GC("percent", s, d_step, d_window)
                    svg.drawGCtype(s.xpos, y, scale, s.length, d_step, GC_table, dheight, doptions[d], d, seqsize, reverse,"GC %",0.5)

                # Catch the division by zero error which could occur when GC calculation return only one value 
                # (usually when window and/or step are too large compared to the sequence length)
                # the RETURNVALUE to 45 will raise a specific warning message in the console
                except ZeroDivisionError:
                    print(f"\n\nWARNING ! {doptions[d]['type']} step/window sizes {d_step}/{d_window} too large for sequence {s.gp['name']} of length {seqsize}\n")
                    RETURNVALUE = 45
 
                if doptions[d]['type'] == "Scale":

                    # Compute min and max absolute positions according to the selected min/max positions 
                    startlabel=s.gp['min']
                    endlabel=s.gp['max']
                    seqrevfactor=1
      
                    # change the min and max mositions according to the REGION accession attribute (for GenBank files) if requested
                    if doptions[d]['labelgbkpos'] and 'accessions' in s.record.annotations.keys() and 'REGION:' in s.record.annotations['accessions']:
                          R = s.record.annotations['accessions'][2].split(".")
                          if 'c' in R[0]:
                           R[2]=R[2].replace(")","")
                           seqrevfactor=-1
                           startlabel=int(R[2])-startlabel
                           endlabel=int(R[2])-endlabel
                          else:            
                           startlabel+=int(R[0])
                           endlabel+=int(R[0])

                    # Scale needs to be printed from end to beginning if the sequence is reversed
                    if s.gp['rev'] != 0:
                         tmp=startlabel 
                         startlabel=endlabel
                         endlabel=tmp
                         seqrevfactor*=-1


                    svg.drawDecoScale(s.xpos, y, scale, dheight, doptions[d], seqsize, reverse, startlabel, endlabel, seqrevfactor)


    # Draw Features
    #------------------------------------------------------------------
    print("Drawing features...")
    for s in sequences:
       lwidth =  int(gsoption(options,'lwidth',s))
       svg.startImproveG()
      
       for feat in s.record.features:
         whichfeat = -1

         # select the LAST feature defined by the user which match with this feature 
         for f in range(len(foptions)):
           if foptions[f]['feat'] == feat.type:  # matching
               ## No filter
               if foptions[f]['filter'] == '': whichfeat = f
               ## With filter
               elif passfilter(feat,foptions[f]['filter'],foptions[f]['field']):
                 whichfeat = f
           
         # print the feature if found in the list of feats to display
         if whichfeat > -1:

            # change the height accordingly
            fheight=int(s.gp['height'])
            if foptions[whichfeat]['height'] != '-': fheight *= float(foptions[whichfeat]['height'])            

            # define the position according to the strand option (center/leading up/lagging up)
            if foptions[whichfeat]['strand'] == 'none':
              yposfeat=s.ypos-fheight/2
            elif (foptions[whichfeat]['strand'] == 'lead up' and feat.strand == 1) or (foptions[whichfeat]['strand'] == 'lag up' and feat.strand == -1):
              yposfeat=s.ypos-fheight-lwidth
            else:              
              yposfeat=s.ypos+lwidth

            svg.drawElement(s.xpos, yposfeat, feat.location, fheight, feat, foptions[whichfeat], whichfeat)

       svg.endImproveG()


    # Draw Sequence label
    #------------------------------------------------------------------
    print("Writing sequence labels...")
    for s in sequences:

       # Sequence label information
       if options['slabsel'] or s.gp['slabsel']:

           # get general or sequence-specific parameters
           identifier =  gsoption(options,'slabid',s)
           offset =  int(gsoption(options,'slaboffset',s))
           position =  gsoption(options,'slabpos',s)
           color =  gsoption(options,'slabcol',s)
           size =  gsoption(options,'slabsize',s)

           # select bold or italic options 
           lweight = 'normal'
           if options['slabbold'] or s.gp['slabbold']:lweight = 'bold'
           lstyle = 'normal'
           if options['slabital'] or s.gp['slabital']:lstyle = 'italic'


           #define organism in italic by default. 
           #WARNING: Not implemented because of inconsistent display in PNG format !!!
           if 'organism' in s.record.annotations.keys():
             #organismIT='<tspan style="font-style:italic;">'+s.record.annotations['organism']+'</tspan>'
             organismIT=s.record.annotations['organism']
           else:
             organismIT=''

           #test for accession existence. Doesn't exist in all fasta files
           if 'accessions' in s.record.annotations.keys():
             accessionval=" ".join(s.record.annotations['accessions'])
           else:
             accessionval=s.gp['name']


           # set the identifier
           if identifier == 'locus': identifier=s.gp['name']
           if identifier == 'organism': identifier=organismIT
           if identifier == 'strain': identifier= s.gp['strain']
           if identifier == 'organism+strain': identifier=organismIT + s.gp['strain']
           if identifier == 'locus+organism': identifier=s.gp['name'] + ": " + organismIT
           if identifier == 'locus+organism+strain': identifier=s.gp['name'] + ": " + organismIT + s.gp['strain']
           if identifier == 'locus+size': identifier=s.gp['name'] + ": " + str(s.lengthO) + " bp"
           if identifier == 'description': identifier=s.record.description
           if identifier == 'accession': identifier= accessionval
           if identifier == 'accession+organism': identifier= accessionval + ", " + organismIT
           if identifier == 'accession+organism+strain': identifier= accessionval + ", " + organismIT + s.gp['strain']
           if identifier == 'locus+description': identifier=s.gp['name'] + ": " + s.record.description
           if identifier == 'accession+description': identifier= accessionval + ", " + s.record.description

           # set the position
           lposy=0
           lposx=0
           ljustify=''
           #offset = int(s.gp['height']) - (int(s.gp['height'])%10)*2
           if position == 'left' : 
               lposy=s.ypos+int(size)/2
               lposx=s.xpos-10
               ljustify='end'
           if position == 'right' : 
               lposy=s.ypos+int(size)/2
               lposx=s.xpos+s.length+10
               ljustify='start'
           if position == 'top' : 
               lposy=s.ypos-offset
               lposx=s.xpos+(s.length/2)
               ljustify='middle'
           if position == 'bottom' : 
               lposy=s.ypos+offset+int(size)
               lposx=s.xpos+(s.length/2)
               ljustify='middle'
           if position == 'top/left' : 
               lposy=s.ypos-offset
               lposx=s.xpos
               ljustify='start'
           if position == 'top/right' : 
               lposy=s.ypos-offset
               lposx=s.xpos+s.length
               ljustify='end'
           if position == 'bottom/left' : 
               lposy=s.ypos+offset+int(size)
               lposx=s.xpos
               ljustify='start'
           if position == 'bottom/right' : 
               lposy=s.ypos+offset+int(size)
               lposx=s.xpos+s.length
               ljustify='end'

           # draw the label
           svg.drawSeqLabel(identifier,lposx,lposy,ljustify,color,size,lweight,lstyle)




    # Draw Feature label
    #------------------------------------------------------------------
    print("Writing feature labels...")
    for s in sequences:

       # Set the label offset according to the sequence feature height
       hoffset = int(int(gsoption(options,'height',s))/5) 
       lwidth = int(gsoption(options,'lwidth',s))
       
       # If the features label is selected
       if options['flabsel'] or s.gp['flabsel']:

        for feat in s.record.features:
         whichfeat = -1

         # select the LAST feature defined by the user which match with this feature 
         for f in range(len(foptions)):
          if foptions[f]['feat'] == feat.type:  # matching
               ## No filter
               if foptions[f]['filter'] == '': whichfeat = f
               ## With filter
               elif passfilter(feat,foptions[f]['filter'],foptions[f]['field']):
                 whichfeat = f
           
         # print the label if the feature was found in the list of feats to display
         if whichfeat > -1 and foptions[whichfeat]['flabsel']: 
           rotdir = -1
           
           # get general, sequence-specific, or feature-specific parameters
           labval =  gsoption(options,'flabval',s)
           position =  gsoption(options,'flabpos',s)
           color =  gsoption(options,'flabcol',s)
           size =  gsoption(options,'flabsize',s)
           rot =  int(gsoption(options,'flabrot',s))
           fweight = 'normal'
           fstyle = 'normal'

           if foptions[whichfeat]['flabval'] != '-': labval = foptions[whichfeat]['flabval']
           if foptions[whichfeat]['flabcol'] != None: color = foptions[whichfeat]['flabcol']
           if foptions[whichfeat]['flabsize'] != '-': size = foptions[whichfeat]['flabsize']
           if foptions[whichfeat]['flabrot'] != '-': rot = int(foptions[whichfeat]['flabrot'])
           if foptions[whichfeat]['flabbold']: fweight = 'bold'
           if foptions[whichfeat]['flabital']: fstyle = 'italic'

           # get label value if exists
           if labval in feat.qualifiers.keys(): label = str(feat.qualifiers[labval][0])
           else: continue
           
           # Need to invert the position in the case of Opposite option
           if foptions[whichfeat]['flabpos'] == 'Opposite':
               if position == 'Top': position = 'Bottom'
               elif position == 'Bottom': position = 'Top'               
           elif foptions[whichfeat]['flabpos'] != '-': position = foptions[whichfeat]['flabpos']
           
           # Select the height of the feature, to correctly print the legend above or below
           fheight=int(s.gp['height'])
           if foptions[whichfeat]['height'] != '-': fheight *= float(foptions[whichfeat]['height'])            

           # eventiually add new lines for long labels
           (nblines,label) = addNewLines(label,23)


           # define the position according to the strand option (center/leading up/lagging up)
           if foptions[whichfeat]['strand'] == 'none':
             yposfeat=s.ypos-fheight/2
           elif (foptions[whichfeat]['strand'] == 'lead up' and feat.strand == 1) or (foptions[whichfeat]['strand'] == 'lag up' and feat.strand == -1):
             yposfeat=s.ypos-fheight
             if position == 'Bottom': position = 'Top'
           else:              
             yposfeat=s.ypos+lwidth
             if position == 'Top': position = 'Bottom'


           # Change Y pos depending on the label position (according to feature/sequence/general choice)
           if position == 'Top':
             if nblines > 1: y= yposfeat  - int(size)*0.75*nblines - hoffset
             else          : y= yposfeat  - hoffset
           elif position == 'Middle':
             if nblines > 1: y=yposfeat + fheight/2 + int(size)*(1-nblines)*0.75/2
             else          : y=yposfeat + fheight/2 + int(size)*0.75/2
           elif position == 'Bottom':
             y=yposfeat + fheight + int(size) + hoffset
             rotdir=1
                      
           # draw the label
           svg.drawElementLabel(s.xpos+feat.location.nofuzzy_start+len(feat.location)/2, y, label, color, size, rot, rotdir,'middle',fweight,fstyle)


    # End translation caused by margins
    #------------------------------------------------------------------
    svg.endMargins()

    # Draw Blast Legend
    #------------------------------------------------------------------
    if options['legBdisp'] and options['legBlastlen']:
        print("Drawing blastn scale...")
        hgrad=svg.height*float(options['legBscale'])
        wgrad=hgrad/4
        (x,y) = (0,0)

        # compute translation
        if options['legBHpos'] == 'Left': x=5*int(options['legBfsize'])
        elif options['legBHpos'] == 'Right': x=svg.width - (10 + wgrad*(1+options['bncolinvset']))
        elif options['legBHpos'] == 'Middle': x=svg.width/2 - (10 + wgrad*(1+options['bncolinvset']))/2

        if options['legBVpos'] == 'Top': y=10
        elif options['legBVpos'] == 'Bottom': y=svg.height - hgrad - 20
        elif options['legBVpos'] == 'Middle': y=svg.height/2 - hgrad/2

        svg.drawBlastLegend(x,y,hgrad,wgrad,options['bncol'],options['bncol2'],options['bncolinvset'],options['bncolinv'],options['bncolinv2'],minBident,maxBident,options['legBfsize'],options['legBfcol'], options['bnopacity'], "N")

    # Draw BlastX Legend
    #------------------------------------------------------------------
    if options['legBXdisp'] and options['legBlastXlen']:
        print("Drawing tblastx scale...")
        hgrad=svg.height*float(options['legBXscale'])
        wgrad=hgrad/4
        (x,y) = (0,0)

        # compute translation
        if options['legBXHpos'] == 'Left': x=5*int(options['legBXfsize'])
        elif options['legBXHpos'] == 'Right': x=svg.width - (10 + wgrad*(1+options['bxcolinvset']))
        elif options['legBXHpos'] == 'Middle': x=svg.width/2 - (10 + wgrad*(1+options['bxcolinvset']))/2

        if options['legBXVpos'] == 'Top': y=10
        elif options['legBXVpos'] == 'Bottom': y=svg.height - hgrad - 20
        elif options['legBXVpos'] == 'Middle': y=svg.height/2 - hgrad/2

        svg.drawBlastLegend(x,y,hgrad,wgrad,options['bxcol'],options['bxcol2'],options['bxcolinvset'],options['bxcolinv'],options['bxcolinv2'],minBXident,maxBXident,options['legBXfsize'],options['legBXfcol'], options['bxopacity'], "X")

    # Draw Scale Legend
    #------------------------------------------------------------------
    if options['legSdisp']:
        print("Drawing size scale...")

        # scale setting (best fitting integer number)
        aftersc=int(sizemax*float(options['legSscale']))

        if aftersc > 1000000: # Megabase scale
            i = 2
            while aftersc/1000000 < 10**i: i=i-1
            aftersc = int(aftersc/(1000000*(10**i)))
            scsize = aftersc*scale*1000000*(10**i)
            scstr = str(aftersc*(10**i)) + " Mbp"
        elif aftersc > 1000:  # Kilobase scale
            i = 2
            while aftersc/1000 < 10**i: i=i-1
            aftersc = int(aftersc/(1000*(10**i)))
            scsize = aftersc*scale*1000*(10**i)
            scstr = str(aftersc*(10**i)) + " Kbp"
        else:                 # base scale
            i = 2
            while aftersc < 10**i: i=i-1
            aftersc = int(aftersc/(10**i))
            scsize = aftersc*scale*(10**i)
            scstr = str(aftersc*(10**i)) + " bp"


        (x,y) = (0,0)

        # compute translation
        if options['legSHpos'] == 'Left': x=10
        elif options['legSHpos'] == 'Right': x=svg.width - scsize - 10
        elif options['legSHpos'] == 'Middle': x=svg.width/2 - scsize/2

        if options['legSVpos'] == 'Top': y=10
        elif options['legSVpos'] == 'Bottom': y=svg.height - 2*float(options['legSfsize']) - 20
        elif options['legSVpos'] == 'Middle': y=svg.height/2 - float(options['legSfsize'])

        svg.drawScaleLegend(x,y,scsize,scstr,options['legSfsize'],options['legSfcol'])



    # Draw Feature Legend
    #------------------------------------------------------------------
    if options['legFdisp']:
        print("Drawing features legend...")
        
        maxflegl=0
        nbdisplayed=0
        flegend = []
        
        # First get feature legend and find the longest one (in case of multicolumn display)
        for f in range(len(foptions)):
          # Get the proper legend: the filter or the feature if a specific legend is not provided
          if foptions[f]['id'] == "" and foptions[f]['filter'] == "": flegend.append(foptions[f]['feat'])
          elif foptions[f]['id'] == "": flegend.append(foptions[f]['filter'])
          else: flegend.append(foptions[f]['id'])
        
          # Get the size of the longest displayed feature legend
          if foptions[f]['flegsel'] == 1 and len(flegend[-1]) > maxflegl: maxflegl = len(flegend[-1])
        
          # Get the number of displayed feature legend
          if foptions[f]['flegsel'] == 1: nbdisplayed = nbdisplayed+1
        
        # set variables
        fght = int(options['fheight'])*1.2*float(options['legFscale'])
        flsize = int(options['legFfsize'])
        try:
         colnb = int(options['legFnb'])
        except: 
         print("ERROR - Legend Features Cols Number - Used default = 1")
         colnb = 1
        xoffset = fght*2 + maxflegl*flsize*0.65 + fght
        yoffset = fght + fght/2
        
        if nbdisplayed >= colnb: legwidth = xoffset*colnb
        else: legwidth = xoffset*nbdisplayed
        
        if nbdisplayed <= colnb: legheight = yoffset
        else: legheight = yoffset*math.ceil(float(nbdisplayed)/colnb)
        
        
        (fx,fy) = (0,0)
        
        # compute translation  #### COrrected by removing margins translation = works prefectly at home
        if options['legFHpos'] == 'Left': fx=10
        elif options['legFHpos'] == 'Right': fx=svg.width - 10 - legwidth
        elif options['legFHpos'] == 'Middle': fx=svg.width/2 - legwidth/2
        fx0 = fx # we need it to go back to original coordinate when new line
        
        if options['legFVpos'] == 'Top': fy=10
        elif options['legFVpos'] == 'Bottom': fy=svg.height - legheight - 20
        elif options['legFVpos'] == 'Middle': fy=svg.height/2 - legheight/2
        fy0 = fy

        # determine how many features to put in each column
        nbcol = colnb # Number of colums to display features legend
        nbdisp = nbdisplayed # Number of features to display

        colsItems = []
        y = 0
        while nbcol > 0:
            nbfeat = math.ceil(nbdisp / nbcol)
            counter = 0
            for f, feat in enumerate(reversed(foptions)):
                if not feat['sel'] == 0 and not feat['flegsel'] == 0: 
                    if counter == nbfeat:
                        break
                    else:
                        colsItems.append(y)
                    counter += 1
            nbcol -= 1
            nbdisp -= nbfeat
            y += 1

        # Feature by feature legend print # in reverse order to print the more general at the end !
        feature_disp_counter = 0
        for f, feat in enumerate(reversed(foptions)):
            if feat['sel'] == 0 or feat['flegsel'] == 0: continue
             
            try:    loc= FeatureLocation(0, int(round(fght*1.2,0)), strand=1 )
            except: loc= FeatureLocation(0, int(round(fght*1.2,0)))     # previous version of Biopython
            
            # print the feature display and its legend according to the Biopython version
            # We recalculate fID because for is in inverted order
            featureID = len(foptions)-1 - f
            svg.drawElement(fx, fy, loc, fght, SeqFeature(loc, qualifiers={'frame':0}), feat, featureID)
            svg.drawElementLabel(fx+fght*3/2, fy+fght/2+(flsize*0.75/2), feat['id'], options['legFfcol'], flsize, rot=0.005)
            
            # update the position for the next feature legend
            try:
                if colsItems[feature_disp_counter] != colsItems[feature_disp_counter+1]:
                    fx = fx + xoffset
                    fy = fy0 - yoffset
            except: continue

            fy = fy + yoffset
            feature_disp_counter+=1

    # End the svg graphics

    #------------------------------------------------------------------
    svg.writesvg(outfile, outfile[-3:].lower())
    
    return RETURNVALUE




#####################################################################################################
# Other functions       #############################################################################
#####################################################################################################

# Select between general or sequence specific option ########################
def gsoption(opt,key,s):

    if s.gp[key] != '-' and s.gp[key] != None: return s.gp[key]
    else: return opt[key]


# Select the starting position of the sequence ###################################################
def startPos(pos, aln, pictwidth, length,sxpos):
    if aln == 'center':
        return pictwidth/2 - length/2
    elif aln == 'right':
        return pictwidth-length
    elif aln.startswith('x ='): 
        return int(sxpos)        
    else:
        return pos


# Filtering featutre test ###################################################
def passfilter(feat,filtr,field):
    if 'translation' in feat.qualifiers.keys(): del feat.qualifiers["translation"]

    # when the filter is on a specific field
    if field != 'any' and field in feat.qualifiers.keys() and re.search(filtr,str(feat.qualifiers[field]),re.I) != None: return True
    # when the filter apply on all fields (except translation)
    if field == 'any' and re.search(filtr,str(feat.qualifiers.values()),re.I) != None: return True

    return False


# Compute the blast color according to gradient ###################################################
def blastCol(ident, mincol, maxcol, minid, maxid):

    if maxid == minid: ratio = 1
    else: ratio = round((ident - minid) / (maxid - minid), 2)

    mincolRGB = mincol[4:-1].split(",")
    maxcolRGB = maxcol[4:-1].split(",")

    r = int(float(mincolRGB[0]) * (1 - ratio) + float(maxcolRGB[0]) * ratio)
    g = int(float(mincolRGB[1]) * (1 - ratio) + float(maxcolRGB[1]) * ratio)
    b = int(float(mincolRGB[2]) * (1 - ratio) + float(maxcolRGB[2]) * ratio)

    return "#%02x%02x%02x" % (r,g,b)

  
# Recursive function which remove position ambiguation for all sequences connected to the input sequence i ###
def resolveambiguous(i,ambiguous,positionned,sequences):
    while len(ambiguous[i])>0:
      sh=ambiguous[i].pop(0)

      # pass if both sequences of the previously ambiguous hit are positioned    
      if positionned[sh[0]] and positionned[sh[1]]: continue

      # next, correct for reversed matches
      qS = sh[3]['qStart']; rS = sh[3]['rStart']
      if sh[3]['qStart'] < sh[3]['qEnd']: qS = sh[3]['qEnd']
      if sh[3]['rStart'] < sh[3]['rEnd']: rS = sh[3]['rEnd']

      # otherwise, the first or the second sequence have to be the disambiguated
      if i == sh[0]:
          sequences[sh[1]].xpos = sequences[sh[0]].xpos+qS-rS
          positionned[sh[1]] = 1
          resolveambiguous(sh[1],ambiguous,positionned,sequences)
      else:   
          sequences[sh[0]].xpos = sequences[sh[1]].xpos+rS-qS
          positionned[sh[0]] = 1
          resolveambiguous(sh[0],ambiguous,positionned,sequences)

