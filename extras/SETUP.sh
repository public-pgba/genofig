#! /bin/bash

CONDASH=`conda init | grep -o "/.*/conda.sh"`

echo '#! /bin/bash' > Genofig
echo 'source' $CONDASH >> Genofig
echo 'conda activate genofig' >> Genofig
echo 'python' "$(pwd)/Genofig.py&" >> Genofig
chmod +x Genofig
